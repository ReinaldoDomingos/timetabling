var express = require('express');

module.exports = function () {
    var app = express();

    app.use(express.static('./public'));
    app.use('/static', express.static(__dirname + '/public'));

    app.get('/env/host', function (req, res) {
        let json = {host: process.env.HOST_API, port: process.env.port ? process.env.port : 8080};
        res.send(JSON.stringify(json));
    });

    return app;
};