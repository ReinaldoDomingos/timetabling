Vue.use(VueRouter);

async function inicializar() {
    let Home = await getHomeView();
    let Turma = await getTurmaView();
    let Professor = await getProfessorView();
    let Disciplina = await getDisciplinaView();
    let GradeHoraria = await getGradeHorariaView();

    const routes = [
        {path: '/', component: Home},
        {name: 'disciplina', path: '/disciplina', component: Disciplina},
        {name: 'disciplina.editar', path: '/disciplina/:id/edit', component: Disciplina},
        {name: 'disciplina.visualizar', path: '/disciplina/:id/view', component: Disciplina},
        {name: 'professor', path: '/professor', component: Professor},
        {name: 'professor.editar', path: '/professor/:id/edit', component: Professor},
        {name: 'professor.visualizar', path: '/professor/:id/view', component: Professor},
        {name: 'turma', path: '/turma', component: Turma},
        {name: 'turma.editar', path: '/turma/:id/edit', component: Turma},
        {name: 'turma.visualizar', path: '/turma/:id/view', component: Turma},
        {name: 'gradeHoraria', path: '/gradeHoraria', component: GradeHoraria},
        {name: 'gradeHoraria.editar', path: '/gradeHoraria/:id/edit', component: GradeHoraria},
        {name: 'gradeHoraria.visualizar', path: '/gradeHoraria/:id/view', component: GradeHoraria}
    ]

    const router = new VueRouter({
        routes
    })

    const app = new Vue({
        router
    }).$mount('#app');
}

inicializar().finally(() => console.log("App inicializado"));