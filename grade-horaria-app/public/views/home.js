async function getHomeView() {
    return {
        template: await importar('./views/home.html'),
        data() {
            return {
                turmas: [],
                disciplinas: [],
                professores: [],
                nomePagina: 'home',
                gradesHorarias: [],
                abaSelecionada: 'disciplinas',
                alertaOptions: criarAlertaOptions(),
                abasMenu: constantes.ESQUEMAS.ABAS_MENU,
                colunasTurmas: constantes.ESQUEMAS.COLUNAS_TURMAS,
                colunasDisciplinas: constantes.ESQUEMAS.COLUNAS_DISCIPLINAS,
                colunasProfessores: constantes.ESQUEMAS.COLUNAS_PROFESSORES,
                colunasGradeHorarias: constantes.ESQUEMAS.COLUNAS_GRADE_HORARIAS,
                modalImportarOptions: {isModalAberto: false, titulo: 'Importar XLS'},
                resultados: {
                    carga: 0,
                    ch_ufms: 3000,
                    carga_horaria_optativa: 238
                }
            };
        },
        mounted() {
            let estadoAtualPagina = CacheEstadoAtual.getEstadoAtualPagina(this.nomePagina);

            if (estadoAtualPagina) {
                this.selecionarAba(estadoAtualPagina.abaSelecionada);
            } else {
                this.selecionarAba(this.abasMenu[0].id);
            }

        },
        computed: {
            estiloClassesNav() {
                return function (id) {
                    return {
                        'show active': this.abaSelecionada === id
                    };
                };
            }
        },
        methods: {
            salvarEstadoAtualPagina() {
                CacheEstadoAtual.salvarEstadoAtualPagina(this.nomePagina, {abaSelecionada: this.abaSelecionada});
            },
            selecionarAba(nomeLista) {
                // this.turmas = [];
                // this.professores = [];
                // this.professores = [];
                // this.gradesHorarias = [];
                this.abaSelecionada = nomeLista;

                this.salvarEstadoAtualPagina();

                switch (nomeLista) {
                    case 'disciplinas':
                        this.buscarDisciplinas();
                        break;
                    case 'professores':
                        this.buscarProfessores();
                        break;
                    case 'turmas':
                        this.buscarTurmas();
                        break;
                    case 'gradesHorarias':
                        this.buscarGradeHorarias();
                        break;
                }
            },
            importarXLS() {
                this.modalImportarOptions.abrirModal();
            },
            enviarXls(arquivo) {
                if (arquivo) {
                    let self = this;
                    enviarArquivo(URL_API + '/gradeHoraria/uploadDisciplinasXLS', arquivo)
                        .then(response => self.modalImportarOptions.setXlsOptions(response.data))
                        .catch(response => self.alertaOptions.mensagemAlerta = getErroFormatado(response));
                }
            },
            finalizarImportacao() {
                let self = this;
                let referenciaColunas = self.modalImportarOptions.referenciaColunas;
                salvarRegistro(URL_API + '/gradeHoraria/importacao', referenciaColunas)
                    .then(response => self.acessarGradeHoraria(response.data.id))
                    .catch(response => self.alertaOptions.mensagemAlerta = getErroFormatado(response));
            },
            acessarItem(path, id, visualizar) {
                if (id && visualizar) {
                    this.$router.push({name: path + '.visualizar', params: {id: id, acao: 'visualizar'}});
                } else if (id) {
                    this.$router.push({name: path + '.editar', params: {id: id}});
                } else {
                    this.$router.push(path);
                }
            },
            acessarProfessor(id, visualizar) {
                this.acessarItem('professor', id, visualizar);
            },
            acessarDisciplina(id, visualizar) {
                this.acessarItem('disciplina', id, visualizar);
            },
            acessarGradeHoraria(id, visualizar) {
                this.acessarItem('gradeHoraria', id, visualizar);
            },
            acessarTurma(id, visualizar) {
                this.acessarItem('turma', id, visualizar);
            },
            excluirDisciplina(disciplina) {
                let self = this;
                deletarRegistro(URL_API + '/disciplina', disciplina.id)
                    .catch(erro => {
                        if (erro.response && getErroFormatado(erro)) {
                            self.alertaOptions.mensagemAlerta = getErroFormatado(erro);
                        } else {
                            self.alertaOptions.mensagemAlerta = 'Falha ao excluir Disciplina.';
                        }
                    })
                    .finally(this.buscarDisciplinas);
            },
            excluirProfessor(professor) {
                let self = this;
                deletarRegistro(URL_API + '/professor', professor.id)
                    .catch(response => self.alertaOptions.mensagemAlerta = getErroFormatado(response))
                    .finally(this.buscarProfessores);
            },
            excluirGradeHoraria(gradeHoraria) {
                let self = this;
                deletarRegistro(URL_API + '/gradeHoraria', gradeHoraria.id)
                    .catch(response => self.alertaOptions.mensagemAlerta = getErroFormatado(response))
                    .finally(this.buscarGradeHorarias);
            },
            excluirTurma(turma) {
                let self = this;
                deletarRegistro(URL_API + '/turma', turma.id)
                    .catch(response => self.alertaOptions.mensagemAlerta = getErroFormatado(response))
                    .finally(this.buscarTurmas);
            },
            buscarDisciplinas(page, size, sort) {
                // this.disciplinas = [];
                buscarListagem(URL_API + '/disciplina', page ? page : 0, size ? size : 5, sort ? sort : 'nome')
                    .then(response => this.processarDisciplinas(response.data));
            },
            buscarProfessores(page, size, sort) {
                this.professores = [];
                buscarListagem(URL_API + '/professor', page ? page : 0, size ? size : 5, sort ? sort : 'nome')
                    .then(response => this.professores = response.data);
            },
            buscarGradeHorarias(page, size, sort) {
                buscarListagem(URL_API + '/gradeHoraria', page ? page : 0, size ? size : 5, sort ? sort : 'ano,semestreAno')
                    .then(response => this.processarGradeHorarias(response.data));
            },
            buscarTurmas(page, size, sort) {
                buscarListagem(URL_API + '/turma', page ? page : 0, size ? size : 5, sort ? sort : 'nome')
                    .then(response => this.turmas = response.data);
            },
            processarGradeHorarias(gradeHorarias) {
                this.gradesHorarias = gradeHorarias;
                this.gradesHorarias.content.forEach(gradeHoraria => this.setSemestreAnoGradeHoraria(gradeHoraria));
            },
            setSemestreAnoGradeHoraria(gradeHoraria) {
                gradeHoraria.semestreAno = filtroEnumValor(gradeHoraria.semestreAno, constantes.ENUMS.SEMESTRE)
            },
            processarDisciplinas(disciplinas) {
                this.disciplinas = disciplinas;

                this.resultados.quantidade = this.disciplinas.totalElements;

                let self = this;
                this.disciplinas.content.forEach(disciplina => {
                    self.resultados.carga += disciplina.cargaHoraria;
                })

                self.resultados.porcentagem = parseFloat(self.resultados.carga) / parseFloat(self.resultados.ch_ufms) * 100;
            }
        },
        filters: {}
    };
}