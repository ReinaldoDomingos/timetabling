async function getGradeHorariaView() {
    return {
        template: await importar('./views/gradeHoraria.html'),
        data() {
            return {
                disciplinas: [],
                professores: [],
                visualizando: false,
                isModalAberto: false,
                nomePagina: 'gradeHoraria',
                filters: this.$route.params,
                colunasSelecionaveis: [],
                colunasDiasMarcadados: [{
                    coluna: 'dias',
                    colunaCheckbox: 'chave',
                    titulo: 'Dias Indisponíveis',
                    classes: ['col-8']
                }],
                disciplinaSelecionada: null,
                gradeHoraria: {isTeste: false},
                semestresDS: constantes.ENUMS.SEMESTRE,
                abaSelecionada: 'disciplinasGradeHoraria',
                alertaOptions: criarAlertaOptions('AVISO'),
                restricoesProfessores: gerarListaPaginadaVazia(),
                disciplinasGradeHoraria: gerarListaPaginadaVazia(),
                abasMenu: constantes.ESQUEMAS.ABAS_MENU_GRADE_HORARIA,
                alertaDisciplinasOptions: {tipo: 'ERRO', mensagemAlerta: null},
                alertaRestricoesOptions: {tipo: 'ERRO', mensagemAlerta: null},
                modalOptions: {isModalAberto: false, titulo: 'Gerar Grade Horária'},
                restricoesCheckbox: [{
                    titulo: 'Manter disciplina em dias alternados',
                    valor: 'DIAS_NAO_CONSECUTIVOS',
                    checked: true
                }],
                colunasDisciplinasGradeHoraria: constantes.ESQUEMAS.COLUNAS_DISCIPLINAS_GRADE_HORARIA,
                colunasRestricoesProfessores: constantes.ESQUEMAS.COLUNAS_RESTRICOES_PROFESSORES_GRADE_HORARIA
            };
        },
        async mounted() {
            if (this.filters.id) {
                this.gradeHoraria.id = this.filters.id;
                this.buscarGradeHoraria();
                this.buscarDisciplinas();
                this.buscarProfessores();
                this.visualizando = this.filters.acao === 'visualizar';

                let estadoAtualPagina = CacheEstadoAtual.getEstadoAtualPagina(this.nomePagina);

                if (estadoAtualPagina) {
                    this.selecionarAba(estadoAtualPagina.abaSelecionada);
                } else {
                    this.selecionarAba(this.abasMenu[0].id);
                }
            }
        },
        computed: {
            estiloClassesNav() {
                return function (id) {
                    return {
                        'show active': this.abaSelecionada === id
                    };
                };
            }
        },
        methods: {
            salvarRestricao(restricoesProfessor) {
                let self = this;

                return function () {
                    let professor = restricoesProfessor.professor;
                    let diasComRestricao = restricoesProfessor.dias.filter(restricao => restricao.checked).map(restricao => restricao.chave);
                    salvarRegistro(URL_API + '/gradeHoraria/' + self.gradeHoraria.id + '/restricao/professor/' + professor.id, diasComRestricao)
                        .catch(() => {
                            self.alertaRestricoesOptions.mensagemAlerta = 'Falha ao salvar os dias em que o professor está indisponível.';
                        })
                }
            },
            salvarEstadoAtualPagina() {
                CacheEstadoAtual.salvarEstadoAtualPagina(this.nomePagina, {abaSelecionada: this.abaSelecionada});
            },
            selecionarAba(nomeLista) {
                this.abaSelecionada = nomeLista;

                this.restricoesProfessores = gerarListaPaginadaVazia();

                this.salvarEstadoAtualPagina();

                switch (nomeLista) {
                    case 'disciplinasGradeHoraria':
                        this.buscarDisciplinas();
                        this.buscarDisciplinasGradeHoraria();
                        break;
                    case 'restricoesProfessores':
                        this.buscarRestricoesProfessores();
                        break;
                }
            },
            buscarGradeHoraria() {
                let self = this;
                buscarRegistro(URL_API + '/gradeHoraria', this.filters.id)
                    .then((response) => self.gradeHoraria = response.data);
            },
            abrirModalGerarGradeHoraria() {
                this.modalOptions.abrirModal();
            },
            voltar() {
                this.$router.push('/');
            },
            excluirDisciplinaGradeHoraria(disciplinaGradeHoraria) {
                deletarRegistro(URL_API + '/disciplinaGradeHoraria', disciplinaGradeHoraria.idDisciplinaGradeHoraria)
                    .finally(() => this.buscarDisciplinasGradeHoraria(this.disciplinasGradeHoraria.number, this.disciplinasGradeHoraria.size));
            },
            salvarDisciplinaGradeHoraria(disciplinaGradeHoraria) {
                let self = this;
                return function () {
                    salvarRegistro(URL_API + '/disciplinaGradeHoraria', disciplinaGradeHoraria, 'idDisciplinaGradeHoraria')
                        .catch(response => {
                            self.alertaDisciplinasOptions.mensagemAlerta = getErroFormatado(response);
                        })
                        .finally(() => self.buscarDisciplinasGradeHoraria(self.disciplinasGradeHoraria.number, self.disciplinasGradeHoraria.size));
                }
            },
            buscarDisciplinas(page, size, sort) {
                // this.disciplinas = [];
                buscarListagem(URL_API + '/disciplina/todas')
                    .then(response => this.disciplinas = response.data.map(disciplina => gerarDataListOptions(disciplina, 'nome')))
            },
            adicionarColunaSelecionavel(colunaSelecionavel) {
                this.colunasSelecionaveis.push(colunaSelecionavel);
            },
            buscarProfessores() {
                let self = this;
                buscarListagem(URL_API + '/professor/todos')
                    .then(response => self.professores = response.data)
                    .then(() => self.adicionarColunaSelecionavel({
                        coluna: 'nome',
                        classes: ['col-3'],
                        titulo: 'Professor',
                        chaveObjeto: 'professor',
                        lista: self.professores
                    }))
                    .finally(self.buscarTurmas);
            },
            criarRestricaoProfessor(restricaoProfessor) {
                let dias = constantes.ENUMS.DIAS_DA_SEMANA.map(diaDaSemana => {
                    let restricoesFiltradas = restricaoProfessor.restricoes.filter(restricao => restricao.diaDaSemana === diaDaSemana.chave);
                    diaDaSemana.checked = restricoesFiltradas.length > 0;
                    return {chave: diaDaSemana.chave, valor: diaDaSemana.valor, checked: diaDaSemana.checked};
                });

                return {professor: restricaoProfessor.professor, dias: dias};
            },
            buscarRestricoesProfessores(page, size, sort, direction) {
                let self = this;

                buscarListagem(URL_API + '/gradeHoraria/' + self.gradeHoraria.id + '/restricao/porProfessor', page, size, sort, direction)
                    .then(response => {
                        let page = response.data;
                        var restricoes = page.content.map(restricaoProfessor => {
                            return self.criarRestricaoProfessor(restricaoProfessor);
                        });
                        self.restricoesProfessores = {
                            page: page.page,
                            size: page.size,
                            number: page.number,
                            content: restricoes,
                            totalPages: page.totalPages,
                            totalElements: page.totalElements,
                            numberOfElements: page.numberOfElements
                        };
                    });
            },
            buscarTurmas() {
                let self = this;
                buscarListagem(URL_API + '/turma/todas')
                    .then(response => self.turmas = response.data)
                    .then(() => self.adicionarColunaSelecionavel({
                        coluna: 'nome',
                        titulo: 'Turma',
                        classes: ['col-3'],
                        chaveObjeto: 'turma',
                        lista: self.turmas
                    }));
            },
            salvarGradeHoraria() {
                let camposObrigatorios = [
                    {atributo: 'ano', titulo: 'ano'},
                    {atributo: 'semestreAno', titulo: 'semestre'},
                    {atributo: 'quantidadeAulasPorTurno', titulo: 'Quantidade aulas por período'}
                ];

                let self = this;
                if (self.isValidoFormulario(camposObrigatorios)) {
                    salvarRegistro(URL_API + '/gradeHoraria', this.gradeHoraria)
                        .then(function (response) {

                            if (!self.filters.id) {
                                self.gradeHoraria = response.data;
                                self.$router.push({name: 'gradeHoraria.editar', params: {id: self.gradeHoraria.id}});
                                self.selecionarAba('disciplinasGradeHoraria');
                            }
                        })
                        .catch((erro) => {
                            if (erro.response && getErroFormatado(erro)) {
                                self.alertaOptions.mensagemAlerta = getErroFormatado(erro);
                            } else {
                                self.alertaOptions.mensagemAlerta = 'Falha ao salvar Grade Horária.';
                            }
                        });
                }
            },
            adicionarDisciplina() {
                if (this.disciplinaSelecionada && this.disciplinaSelecionada.id) {
                    let self = this;
                    let disciplinaSelecionada = {id: this.disciplinaSelecionada.id};
                    axios.post(`${URL_API}/gradeHoraria/${self.gradeHoraria.id}/adicionarDisciplina`, disciplinaSelecionada)
                        .then(() => self.buscarDisciplinasGradeHoraria(self.disciplinasGradeHoraria.number, self.disciplinasGradeHoraria.size))
                        .catch(response => self.alertaDisciplinasOptions.mensagemAlerta = getErroFormatado(response));
                }
            },
            buscarDisciplinasGradeHoraria(page, size, sort, direction) {
                buscarListagem(URL_API + '/gradeHoraria/' + this.gradeHoraria.id + '/disciplinas', page ? page : 0, size ? size : 5)
                    .then(response => this.disciplinasGradeHoraria = response.data);
            },
            gerarXLS() {
                let self = this;
                let restricoesAtivas = self.restricoesCheckbox.filter(restricao => restricao.checked).map(restricao => restricao.valor);
                buscar(URL_API + '/gradeHoraria/gradeHorariaCompleta/' + self.gradeHoraria.id + '?restricoes=' + restricoesAtivas)
                    .then(response => exportarXls('Grade Horária', response.data))
                    .catch(response => self.alertaDisciplinasOptions.mensagemAlerta = getErroFormatado(response))
                // .finally(self.modalOptions.fecharModal);
            },
            isValidoFormulario(campos) {

                for (let indice = 0; indice < campos.length; indice++) {
                    let atributo = campos[indice].atributo;
                    if (!this.gradeHoraria[atributo]) {
                        this.alertaOptions.mensagemAlerta = 'Preencha o campo ' + campos[indice].titulo + '.';
                        return false;
                    }
                }

                return true;
            }
        }
    };
}
