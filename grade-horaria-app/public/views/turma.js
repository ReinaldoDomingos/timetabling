async function getTurmaView() {
    return {
        template: await importar('./views/turma.html'),
        data() {
            return {
                id: null,
                turma: {},
                visualizando: false,
                filters: this.$route.params,
                alertaOptions: criarAlertaOptions()
            };
        },
        mounted() {
            if (this.filters.id) {
                this.buscarTurma(this.filters.id)
                this.visualizando = this.filters.acao === 'visualizar';
            }
        },
        methods: {
            voltar() {
                this.$router.push('/');
            },
            buscarTurma(id) {
                buscarRegistro(URL_API + '/turma', id)
                    .then(response => this.turma = response.data);
            },
            salvarTurma() {
                let self = this;
                salvarRegistro(URL_API + '/turma', self.turma)
                    .then((response) => {
                        if (!self.filters.id) {
                            this.$router.push({name: 'turma.editar', params: {id: response.data.id}});
                        }
                    })
                    .catch(response => self.alertaOptions.mensagemAlerta = getErroFormatado(response));
            }
        }
    };
}