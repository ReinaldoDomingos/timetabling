Vue.component('modal-importar-grade-horaria', {
    props: ['options', 'funcao', 'funcaoUpload', 'acoesCheckbox'],
    data() {
        return {
            cabecalhos: [],
            mostrarBotaoSelecionarArquivo: true,
            alertaOptions: criarAlertaOptions('AVISO'),
            referenciaColunas: this.getReferenciaColunasDefault()
        }
    },
    template: `
    <div v-show="options.isModalAberto" class="modal-importar-grade-horaria naoSelecionavel">
        <div class="modal fade show modal-aberto" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{options.titulo}}</h5>
                        <button @click="fecharModal" type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <caixa-de-alerta :alerta-options="alertaOptions"></caixa-de-alerta>
                        <botao-selecionar-arquivo v-if="mostrarBotaoSelecionarArquivo"
                         :funcao="funcaoUpload" texto-botao="Selecionar Arquivo" 
                         placeholder="Selecione um arquivo (Ex.: arquivo.xlsx)">                       
                        </botao-selecionar-arquivo>
   
                        <div v-if="cabecalhos.length" class="grid formulario">
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <botao-radio label="Dados de teste:" :valor="referenciaColunas" campo="isTeste"></botao-radio>
                                </div>        
                                <div class="col">
                                    <caixa-de-numero label="Quantidade aulas por período" :valor="referenciaColunas"
                                     campo="quantidadeAulasPorTurno"></caixa-de-numero>
                                </div>        
                            </div>
                            
                            <div class="row">
                                <div class="col">
                                    <caixa-de-selecao label="Coluna Professor" :lista="cabecalhos" 
                                        chave-combo="valor" campo-combo="valor" campo="colunaProfessor"
                                        :valor="referenciaColunas">                          
                                    </caixa-de-selecao>
                                </div>
                                <div class="col">
                                    <caixa-de-selecao label="Coluna Turma" :lista="cabecalhos" 
                                        chave-combo="valor" campo-combo="valor" campo="colunaTurma"
                                        :valor="referenciaColunas">                          
                                    </caixa-de-selecao>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col">
                                    <caixa-de-selecao label="Coluna Disciplina" :lista="cabecalhos" 
                                        chave-combo="valor" campo-combo="valor" campo="colunaDisciplina"
                                        :valor="referenciaColunas">                          
                                    </caixa-de-selecao>
                                </div>
                                <div class="col">
                                    <caixa-de-selecao label="Coluna Carga Horária Semanal" :lista="cabecalhos" 
                                        chave-combo="valor" campo-combo="valor" campo="colunaCargaHorariaSemanal"
                                        :valor="referenciaColunas">                          
                                    </caixa-de-selecao>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <botao-cancelar :funcao="fecharModal"></botao-cancelar>
                        <botao-salvar v-if="funcao" titulo="Salvar" :funcao="funcaoPrincipal"></botao-salvar>
                    </div>
                </div>
            </div>
        </div>
        <div @click="fecharModal" class="modal-backdrop fade show"></div>
    </div>`,
    computed: {
        estiloClasses() {
            return function () {

                return {
                    'modal-aberto': this.options.isModalAberto,
                    'show': this.options.isModalAberto
                }
            }
        }
    },
    mounted() {
        this.options.abrirModal = this.abrirModal;
        this.options.fecharModal = this.fecharModal;
        this.options.setXlsOptions = this.setXlsOptions;
        this.options.referenciaColunas = this.referenciaColunas;
    },
    updated() {
        this.mostrarBotaoSelecionarArquivo = true;
    },
    methods: {
        getReferenciaColunasDefault() {
            return {
                isTeste: true,
                colunaTurma: null,
                nomeArquivo: null,
                colunaProfessor: null,
                colunaDisciplina: null,
                colunaCargaHorariaSemanal: null
            };
        },
        abrirModal() {
            this.options.isModalAberto = true;
        },
        fecharModal() {
            this.options.isModalAberto = false;
            this.mostrarBotaoSelecionarArquivo = false;
            this.referenciaColunas = this.getReferenciaColunasDefault();
            this.options.abrirModal = this.abrirModal;
            this.options.fecharModal = this.fecharModal;
            this.options.setXlsOptions = this.setXlsOptions;
            this.options.referenciaColunas = this.referenciaColunas;
            this.setXlsOptions({});
            window.scrollTo(0, 10);
        },
        funcaoPrincipal() {
            let objeto = this.referenciaColunas;
            let camposObrigatorios = ['colunaTurma', 'colunaProfessor', 'colunaDisciplina',
                'colunaCargaHorariaSemanal', 'quantidadeAulasPorTurno'];

            for (let i = 0; i < camposObrigatorios.length; i++) {
                if (!objeto[camposObrigatorios[i]]) {
                    this.alertaOptions.mensagemAlerta = 'Preencha todos campos obrigatórios!';
                    break;
                } else {
                    this.alertaOptions.mensagemAlerta = '';
                }
            }

            if (!this.alertaOptions.mensagemAlerta) {
                this.funcao();
            }
        },
        gerarCabecalhos: function (xlsOptions) {
            return xlsOptions.colunasCabecalho ? xlsOptions.colunasCabecalho.map(cabecalho => {
                return {valor: cabecalho};
            }) : [];
        },
        formatarParaComparar(valor) {
            return valor.replaceAll(':', '').toLowerCase();
        },
        setXlsOptions(xlsOptions) {
            this.referenciaColunas.nomeArquivo = xlsOptions.nomeArquivo;
            this.cabecalhos = this.gerarCabecalhos(xlsOptions);

            for (let i = 0; i < this.cabecalhos.length; i++) {
                let cabecalho = this.cabecalhos[i];
                let valoresPadraoProfessor = ['professor', 'docente'];
                if (valoresPadraoProfessor.indexOf(this.formatarParaComparar(cabecalho.valor)) !== -1) {
                    this.referenciaColunas.colunaProfessor = cabecalho.valor;
                    continue;
                }
                let valoresPadraoTurma = ['turma', 'semestre'];
                if (valoresPadraoTurma.indexOf(this.formatarParaComparar(cabecalho.valor)) !== -1) {
                    this.referenciaColunas.colunaTurma = cabecalho.valor;
                    continue;
                }
                let valoresPadraoDisciplina = ['disciplina', 'matéria'];
                if (valoresPadraoDisciplina.indexOf(this.formatarParaComparar(cabecalho.valor)) !== -1) {
                    this.referenciaColunas.colunaDisciplina = cabecalho.valor;
                    continue;
                }
                let valoresPadraoCHS = ['chs', 'c.h.s', 'carga horária semanal', 'quantidade de aulas'];
                if (valoresPadraoCHS.indexOf(this.formatarParaComparar(cabecalho.valor)) !== -1) {
                    this.referenciaColunas.colunaCargaHorariaSemanal = cabecalho.valor;
                }
            }
        }
    }
});