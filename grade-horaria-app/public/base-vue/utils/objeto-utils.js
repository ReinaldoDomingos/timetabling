function isNull(valor) {
    return valor === undefined || valor === null;
}

function nonNull(valor) {
    return !isNull(valor);
}

function isEmpty(lista) {
    return isNull(lista) || lista.length === 0;
}

function isNotEmpty(lista) {
    return !isEmpty(lista);
}

function getValorOuValorPadrao(valor, valorPadrao) {
    return nonNull(valor) ? valor : valorPadrao;
}

Array.prototype.groupBy = function (keyFunction) {
    var groups = {};
    this.forEach(function (el) {
        var key = keyFunction(el);
        if (!(key in groups)) {
            groups[key] = [];
        }
        groups[key].push(el);
    });
    return Object.keys(groups).map(function (key) {
        return groups[key];
    });
};