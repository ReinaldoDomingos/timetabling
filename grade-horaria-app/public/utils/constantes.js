const constantes = {
    ENUMS: {
        SEMESTRE: [
            {chave: 'PRIMEIRO_SEMESTRE', valor: '1° Semestre'},
            {chave: 'SEGUNDO_SEMESTRE', valor: '2° Semestre'}
        ],
        TIPO_ALERTA: [
            {chave: 'ERRO', valor: 'Erro'},
            {chave: 'AVISO', valor: 'Aviso'},
            {chave: 'SUCESSO', valor: 'Sucesso'}
        ],
        DIAS_DA_SEMANA: [
            {chave: 'SEGUNDA_FEIRA', valor: 'Segunda-feira'},
            {chave: 'TERCA_FEIRA', valor: 'Terça-feira'},
            {chave: 'QUARTA_FEIRA', valor: 'Quarta-feira'},
            {chave: 'QUINTA_FEIRA', valor: 'Quinta-feira'},
            {chave: 'SEXTA_FEIRA', valor: 'Sexta-feira'},
            {chave: 'SABADO', valor: 'Sábado'},
            {chave: 'DOMINGO', valor: 'Domingo'}
        ]
    },
    ESQUEMAS: {
        COLUNAS_DISCIPLINAS: [
            {
                coluna: 'codigo',
                titulo: 'Código',
                classes: ['col-1'],
            },
            {
                classes: [],
                coluna: 'nome',
                titulo: 'Nome'
            },
            {titulo: 'C.H', coluna: 'cargaHoraria'}
        ],
        COLUNAS_DISCIPLINAS_GRADE_HORARIA: [
            {
                titulo: 'Nome',
                coluna: 'nome',
                classes: []
            },
            {
                titulo: 'C.H',
                classes: ['col-1'],
                coluna: 'cargaHoraria'
            },
            {
                editavel: true,
                titulo: 'C.H.S',
                classes: ['col-1'],
                coluna: 'cargaHorariaSemanal'
            }
        ],
        COLUNAS_RESTRICOES_PROFESSORES_GRADE_HORARIA: [
            {
                titulo: 'Professor',
                coluna: 'professor.nome',
                classes: []
            }
        ],
        COLUNAS_PROFESSORES: [
            {
                coluna: 'id',
                titulo: 'Código',
                classes: ['col-1']
            },
            {
                classes: [],
                coluna: 'nome',
                titulo: 'Nome'
            }
        ],
        COLUNAS_GRADE_HORARIAS: [
            {
                titulo: 'Ano',
                coluna: 'ano',
                classes: ['col-1'],
            },
            {
                classes: [],
                titulo: 'Semestre',
                coluna: 'semestreAno'
            }
        ],
        COLUNAS_TURMAS: [
            {
                coluna: 'codigo',
                titulo: 'Código',
                classes: ['col-1']
            },
            {
                classes: [],
                titulo: 'Nome',
                coluna: 'nome'
            },
            {coluna: 'semestre', titulo: 'Semestre'}
        ],
        ABAS_MENU: [
            {id: 'disciplinas', titulo: 'Disciplinas'},
            {id: 'professores', titulo: 'Professores'},
            {id: 'turmas', titulo: 'Turmas'},
            {id: 'gradesHorarias', titulo: 'Grades Horárias'}
        ],
        ABAS_MENU_GRADE_HORARIA: [
            {id: 'disciplinasGradeHoraria', titulo: 'Lista de Disciplinas'},
            {id: 'restricoesProfessores', titulo: 'Restrições Professores'}
        ]
    }
};