package br.ufms.cpcx.gradehoraria.gradehoraria.repository;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Long> {
    boolean existsProfessorByNome(String nome);

    void deleteAllByGradeHorariaTesteId(Long idGradeHoraria);

    Professor findByNome(String nome);
}
