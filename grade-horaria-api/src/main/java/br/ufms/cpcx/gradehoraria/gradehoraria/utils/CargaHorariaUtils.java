package br.ufms.cpcx.gradehoraria.gradehoraria.utils;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CargaHorariaUtils {
    private CargaHorariaUtils() {
    }

    public static Map<Integer, Map<String, Integer>> getCargaHorariaTurmasPorDia(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosPorDia = CandidatoUtils.getCandidatosPorDia(candidatos);

        HashMap<Integer, Map<String, Integer>> cargaHorariaTumasPorDia = new HashMap<>();

        candidatosPorDia.forEach((dia, candidatosDia) -> cargaHorariaTumasPorDia.put(dia, getCargaHorariaTurmas(candidatosDia)));

        return cargaHorariaTumasPorDia;
    }

    public static Map<Integer, Integer> getCargaHorariaProfessorPorDia(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, String professor) {
        return getCargaHorariaProfessorPorDia(CandidatoUtils.getCandidatosPorDia(candidatos), professor);
    }

    public static Map<Integer, Integer> getCargaHorariaProfessorPorDia(Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosPorDia, String professor) {
        HashMap<Integer, Integer> cargaHorariaProfessorPorDia = new HashMap<>();

        candidatosPorDia.forEach((dia, candidatosDia) -> cargaHorariaProfessorPorDia.put(dia, getCargaHorariaProfessorCandidatos(candidatosDia, professor)));

        return cargaHorariaProfessorPorDia;
    }

    private static Map<String, Integer> getCargaHorariaTurmas(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia) {
        Map<String, List<EntidadeDTO>> entidadesPorSemestre = CandidatoUtils.getEntidadesCandidatosPorSemestre(candidatosDia);

        Map<String, Integer> cargaHorariaPorSemestre = new HashMap<>();

        entidadesPorSemestre.forEach((semestre, entidades) -> cargaHorariaPorSemestre.put(semestre, getCargaHorariaEntidades(entidades)));

        return cargaHorariaPorSemestre;
    }

    public static int getCargaHorariaEntidades(List<EntidadeDTO> entidades) {
        return entidades.stream().mapToInt(EntidadeDTO::getCargaHorariaTotal).sum();
    }

    public static int getCargaHorariaProfessorCandidatos(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, String professor) {
        List<EntidadeDTO> entidades = CandidatoUtils.getEntidadesCanditatos(candidatos);

        return getCargaHorariaProfessorEntidades(professor, entidades);
    }

    public static int getCargaHorariaCandidatos(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        List<EntidadeDTO> entidades = CandidatoUtils.getEntidadesCanditatos(candidatos);

        return getCargaHorariaEntidades(entidades);
    }

    public static int getCargaHorariaProfessorEntidades(String professor, List<EntidadeDTO> entidades) {
        return entidades.stream().mapToInt(entidade -> entidade.getCargaHorariaTotalProfessor(professor)).sum();
    }

    public static int getCargaHorariaCandidato(Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        return getCargaHorariaCandidatos(Collections.singletonList(candidato));
    }
}
