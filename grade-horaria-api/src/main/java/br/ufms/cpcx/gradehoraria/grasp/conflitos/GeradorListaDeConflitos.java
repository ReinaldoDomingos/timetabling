package br.ufms.cpcx.gradehoraria.grasp.conflitos;

import br.ufms.cpcx.gradehoraria.arquivos.utils.txt.ExportarTxtUtils;
import br.ufms.cpcx.gradehoraria.arquivos.utils.xls.LeitorXlS;
import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.utils.StringUtils;
import br.ufms.cpcx.gradehoraria.grasp.conflitos.dto.ConflitoDTO;
import br.ufms.cpcx.gradehoraria.grasp.conflitos.exception.ColunaInexistenteException;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Objects.isNull;

@Data
public class GeradorListaDeConflitos {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeradorListaDeConflitos.class);

    private String nomeArquivo;
    private String colunaSemestre;
    private List<String> cabecalho;
    private String colunaProfessor;
    private String colunaCargaHoraria;
    private String colunaIdentificador;
    private String colunaQtdTurmaPratica;
    private Integer quantidadeAulasPorTurno;
    private final List<EntidadeDTO> entidades;
    private final String colunaNomeEntidade;
    private String colunaCargaHorariaPratica;
    private String colunaCargaHorariaTeorica;
    private final List<String> colunasRestritas;

    public GeradorListaDeConflitos(String colunaNomeEntidade) {
        this.entidades = new ArrayList<>();
        this.cabecalho = new ArrayList<>();
        this.colunasRestritas = new ArrayList<>();
        this.colunaNomeEntidade = colunaNomeEntidade;
    }

    public List<EntidadeDTO> getEntidades() {
        return this.entidades;
    }

    public void setColunaIdentificador(String colunaIdentificador) {
        this.colunaIdentificador = colunaIdentificador;
    }

    public void setColunaCargaHoraria(String colunaCargaHoraria) {
        this.colunaCargaHoraria = colunaCargaHoraria;
    }

    public void setColunaCargaHorariaPratica(String colunaCargaHorariaPratica) {
        this.colunaCargaHorariaPratica = colunaCargaHorariaPratica;
    }

    public void setColunaCargaHorariaTeorica(String colunaCargaHorariaTeorica) {
        this.colunaCargaHorariaTeorica = colunaCargaHorariaTeorica;
    }

    public void setColunaQtdTurmaPratica(String colunaQtdTurmaPratica) {
        this.colunaQtdTurmaPratica = colunaQtdTurmaPratica;
    }

    public String getColunaSemestre() {
        return colunaSemestre;
    }

    public void setColunaSemestre(String colunaSemestre) {
        this.colunaSemestre = colunaSemestre;
    }

    public String getColunaProfessor() {
        return colunaProfessor;
    }

    public void setColunaProfessor(String colunaProfessor) {
        this.colunaProfessor = colunaProfessor;
    }

    public void lerRegistrosTabuladosNoXlS(String nomeArquivo, List<String> colunasProfessor) throws IOException, ColunaInexistenteException {
        LOGGER.info("-------------------------------------------------------");
        setNomeArquivo(nomeArquivo);

        List<String> linhas = LeitorXlS.getLinhas(nomeArquivo);

        lerRegistrosTabulados(linhas, colunasProfessor);

        LOGGER.info("-----------------Leitura arquivo finalizada---------------------");
    }

    private void gerarNovosIdsEntidades() {
        Map<String, String> relacaoIdsAntigosComNovos = new HashMap<>();
        for (int j = 0; j < this.entidades.size(); j++) {
            int idNovo = j + 1;
            relacaoIdsAntigosComNovos.put(String.valueOf(this.entidades.get(j).getId()), String.valueOf(idNovo));
            this.entidades.get(j).setId(idNovo);
        }

        for (int j = 0; j < this.entidades.size(); j++) {
            List<String> idsEntidadesIdenticasAntigos = this.entidades.get(j).getIdsEntidadesIdenticas();
            List<String> idsEntidadesIdenticasNovos = idsEntidadesIdenticasAntigos.stream()
                    .map(relacaoIdsAntigosComNovos::get).collect(Collectors.toList());
            this.entidades.get(j).setIdsEntidadesIdenticas(idsEntidadesIdenticasNovos);
        }
    }

    private void juntarProfessoresDisciplina(EntidadeDTO entidade, List<EntidadeDTO> entidadesParaJuntar) {
        List<String> professores = StringUtils.getListaDoTexto(entidade.getProfessor());
        for (EntidadeDTO entidadeParaJuntar : entidadesParaJuntar) {
            professores.addAll(StringUtils.getListaDoTexto(entidadeParaJuntar.getProfessor()));
        }

        entidade.setProfessor(StringUtils.getTextoDaLista(professores));
    }

    private String gerarJuncaoNomeDisciplina(EntidadeDTO disciplina1, EntidadeDTO disciplina2) {
        return disciplina1.getNome() + " | " + disciplina2.getNome();
//        return disciplina1.getNome() + " (" + disciplina1.getCargaHoraria() + ")" + " | " + disciplina2.getNome() + " (" + disciplina2.getCargaHoraria() + ")";
    }

    private void juntarColunasProfessor(List<String> colunasProfessor) throws ColunaInexistenteException {
        if (colunasProfessor.size() > 1) {
            juntarColunas(this.colunaProfessor, colunasProfessor.get(0), colunasProfessor.get(1));
        }
    }

    public void lerRegistrosTabulados(List<String> linhas, List<String> colunasProfessor) throws ColunaInexistenteException {
        this.cabecalho = gerarListaAtributos(linhas.get(0));

        try {
            validarColuna(this.colunaCargaHoraria);

            int i = 1;
            while (i < linhas.size()) {
                adicionarEntidades(linhas.get(i++));
            }

//            juntarColunasProfessor(colunasProfessor);

            dividirEntidades();

            gerarNovosIdsEntidades();

//            Map<String, List<EntidadeDTO>> aulasPeriodoParcialPorSemestre = this.entidades.stream()
//                    .sorted(Comparator.comparing(EntidadeDTO::getCargaHoraria).reversed())
//                    .collect(Collectors.groupingBy(EntidadeDTO::getSemestre));
//
//            for (Map.Entry<String, List<EntidadeDTO>> entry : aulasPeriodoParcialPorSemestre.entrySet()) {
//                List<EntidadeDTO> entidadesNoSemestre = entry.getValue();
//                entidadesNoSemestre.sort(Comparator.comparing(EntidadeDTO::getNome));
//
//                while (entidadesNoSemestre.size() > 1) {
//                    List<EntidadeDTO> entidadesParaJuntar = new ArrayList<>();
//
//                    EntidadeDTO entidade = entidadesNoSemestre.remove(0);
//                    Integer totalCargaHoraria = entidade.getCargaHoraria();
//                    for (EntidadeDTO value : entidadesNoSemestre) {
//                        if (totalCargaHoraria + value.getCargaHoraria() <= this.quantidadeAulasPorTurno) {
//                            entidadesParaJuntar.add(value);
//                            totalCargaHoraria += value.getCargaHoraria();
//                        }
//                    }
//
//                    if (!entidadesParaJuntar.isEmpty()) {
//                        entidadesParaJuntar.stream().map(entidadeParaJuntar -> AulaDTO.builder()
//                                .disciplina(entidadeParaJuntar.getNome())
//                                .turma(entidadeParaJuntar.getSemestre())
//                                .professor(entidadeParaJuntar.getProfessor())
//                                .cargaHoraria(entidadeParaJuntar.getCargaHoraria())
//                                .build()).forEach(aula -> entidade.getAulas().add(aula));
//
//                        juntarDisciplinas(entidade, entidadesParaJuntar);
//                        juntarProfessoresDisciplina(entidade, entidadesParaJuntar);
////                        entidade.setCargaHoraria(totalCargaHoraria);
//                        this.entidades.removeAll(entidadesParaJuntar);
//                        entidadesNoSemestre.removeAll(entidadesParaJuntar);
//                    } else {
//                        break;
//                    }
//                }
//            }
//
//            gerarNovosIdsEntidades();

        } catch (ColunaInexistenteException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    private void juntarDisciplinas(EntidadeDTO entidade, List<EntidadeDTO> entidadesParaJuntar) {
        for (EntidadeDTO entidadeParaJuntar : entidadesParaJuntar) {
            entidade.setNome(gerarJuncaoNomeDisciplina(entidade, entidadeParaJuntar));
            List<String> idsEntidadesIdenticas = entidade.getIdsEntidadesIdenticas();
            idsEntidadesIdenticas.addAll(entidadeParaJuntar.getIdsEntidadesIdenticas());
            entidade.setIdsEntidadesIdenticas(idsEntidadesIdenticas.stream().distinct().collect(Collectors.toList()));
        }
    }

    private void dividirEntidades() {
        //        List<Entidade> aulasParcial = this.entidades.stream().filter(entidade -> entidade.getCargaHoraria().compareTo(this.quantidadeAulasPorTurno) < 0).collect(Collectors.toList());
        List<EntidadeDTO> aulasParcial = new ArrayList<>(this.entidades);
//        List<Entidade> aulasParcial = this.entidades.stream().filter(entidade -> entidade.getCargaHoraria().compareTo(this.quantidadeAulasPorTurno) < 0).collect(Collectors.toList());

//        if (this.quantidadeAulasPorTurno > 4) {
        switch (this.quantidadeAulasPorTurno) {
            case 6:
                dividirAulasComCargaHorariaIgualA(aulasParcial, 6, asList(3, 3));
                dividirAulasComCargaHorariaIgualA(aulasParcial, 5, asList(3, 2));
                dividirAulasComCargaHorariaIgualA(aulasParcial, 4, asList(3, 1));
                dividirAulasComCargaHorariaIgualA(aulasParcial, 3, asList(2, 1));
                dividirAulasComCargaHorariaIgualA(aulasParcial, 2, asList(1, 1));
                break;
            default:
                dividirAulasComCargaHorariaIgualA(aulasParcial, 5, asList(3, 2));
                dividirAulasComCargaHorariaIgualA(aulasParcial, 4, asList(3, 1));
                dividirAulasComCargaHorariaIgualA(aulasParcial, 3, asList(2, 1));
                dividirAulasComCargaHorariaIgualA(aulasParcial, 2, asList(1, 1));
                break;
        }
//        }

//        return aulasParcial.stream()
//                .sorted(Comparator.comparing(EntidadeDTO::getCargaHoraria).reversed())
//                .collect(Collectors.groupingBy(EntidadeDTO::getSemestre));
    }

    private void dividirAulasComCargaHorariaIgualA(List<EntidadeDTO> aulasParcial, int cargaHorariaRuim, List<Integer> novasCargaHoraria) {
        List<EntidadeDTO> aulasParaDividir = aulasParcial.stream()
                .filter(entidade -> entidade.getCargaHoraria().equals(cargaHorariaRuim))
                .collect(Collectors.toList());

        List<EntidadeDTO> aulasDivididas = new ArrayList<>();

        aulasParaDividir.forEach(entidade -> {
            EntidadeDTO novaEntidade = entidade.clonar();
            novaEntidade.setAulas(new ArrayList<>());

            novaEntidade.put(this.colunaNomeEntidade, entidade.get(this.colunaNomeEntidade));
            novaEntidade.put(this.colunaProfessor, entidade.get(this.colunaProfessor));
            novaEntidade.put(this.colunaSemestre, entidade.get(this.colunaSemestre));
            novaEntidade.put(this.colunaCargaHoraria, entidade.get(this.colunaCargaHoraria));
            novaEntidade.setNome(entidade.get(this.colunaNomeEntidade));
            novaEntidade.setCargaHoraria(novasCargaHoraria.get(0));
            novaEntidade.setProfessor(entidade.getProfessor());
            novaEntidade.setSemestre(entidade.getSemestre());
            novaEntidade.setNumero(entidade.getNumero());

            AulaDTO aula = AulaDTO.builder()
                    .disciplina(novaEntidade.getNome())
                    .turma(novaEntidade.getSemestre())
                    .professor(novaEntidade.getProfessor())
                    .cargaHoraria(novaEntidade.getCargaHoraria())
                    .build();

            novaEntidade.getAulas().add(aula);

            entidade.setCargaHoraria(novasCargaHoraria.get(1));
//            entidade.setAulas(new ArrayList<>(Collections.singletonList(entidade.getAulas().get(0))));
            entidade.getAulas().get(0).setCargaHoraria(novasCargaHoraria.get(1));

            aulasDivididas.add(novaEntidade);
        });

        this.entidades.addAll(aulasDivididas);
        this.entidades.sort(Comparator.comparing(EntidadeDTO::getNome));
        aulasParcial.addAll(aulasDivididas);
        gerarNovosIdsEntidades();
    }

    private void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo.replace(".xlsx", "")
                .replace(".xls", "") + "-lista-conflitos.txt";
    }

    private void validarColuna(String coluna) throws ColunaInexistenteException {
        if (!this.cabecalho.contains(coluna))
            throw new ColunaInexistenteException("Coluna Inexistente: " + coluna);
    }

    private void adicionarEntidades(String linha) {
        String[] valores = linha.split("\t");

        if (valores.length == 0) return;

        String valor = validarCargaHoraria(valores[this.cabecalho.indexOf(this.colunaCargaHoraria)]);

        int cargaHoraria = Integer.parseInt(valor);

        if (isNull(this.colunaCargaHorariaPratica)) {
            adicionarEntidades(valores, cargaHoraria);
        } else {
            int numeroTurmaPratica = Integer.parseInt(valores[this.cabecalho.indexOf(this.colunaQtdTurmaPratica)]);
            int cargaHorariaPratica = Integer.parseInt(valores[this.cabecalho.indexOf(this.colunaCargaHorariaPratica)]);
            int cargaHorariaTeorica = Integer.parseInt(valores[this.cabecalho.indexOf(this.colunaCargaHorariaTeorica)]);
            String professoresPratica = valores[this.cabecalho.indexOf("Professor Prática")];
            int qtdProfessores = professoresPratica.split(",").length;

            String[] valoresEntidade = Arrays.copyOf(valores, valores.length);
            if (qtdProfessores > 0) {
                valoresEntidade[this.cabecalho.indexOf(this.colunaNomeEntidade)] = valoresEntidade[this.cabecalho.indexOf(this.colunaNomeEntidade)] + " Teórica";
            }
            adicionarEntidades(valoresEntidade, cargaHorariaTeorica);

//            StringBuilder numTurmas = new StringBuilder("\n(");
//            for (int i = 0; i < qtdProfessores; i++) {
//                numTurmas.append("P").append(i + 1).append(",");
//            }
//            String adicional = numTurmas.substring(0, numTurmas.length() - 1) + ")";

//            for (int i = 0; i < numeroTurmaPratica / qtdProfessores; i++) {
            for (int i = 0; i < numeroTurmaPratica; i++) {
                valoresEntidade = Arrays.copyOf(valores, valores.length);
//                valoresEntidade[this.cabecalho.indexOf(this.colunaNomeEntidade)] = valoresEntidade[this.cabecalho.indexOf(this.colunaNomeEntidade)] + " " + adicional;
                valoresEntidade[this.cabecalho.indexOf(this.colunaNomeEntidade)] = valoresEntidade[this.cabecalho.indexOf(this.colunaNomeEntidade)] + " Prática";
                adicionarEntidades(valoresEntidade, cargaHorariaPratica);
            }

/*
            numTurmas = new StringBuilder("\n(");
            int cont = qtdProfessores;
            for (int i = 0; i < qtdProfessores; i++) {
                numTurmas.append("P").append(++cont).append(",");
            }
            adicional = numTurmas.substring(0, numTurmas.length() - 1) + ")";
            for (int j = 0; j < numeroTurmaPratica % qtdProfessores; j++) {
                valoresEntidade = Arrays.copyOf(valores, valores.length);
                valoresEntidade[this.cabecalho.indexOf(this.colunaNomeEntidade)] = valoresEntidade[this.cabecalho.indexOf(this.colunaNomeEntidade)] + " " + adicional;
                adicionarEntidades(valoresEntidade, cargaHorariaPratica, ETipoEntidade.PRATICA);
            }
*/
        }
    }

    private String validarCargaHoraria(String valor) {
        if (isNull(valor) || valor.equals("null")) {
            throw new GenericException("Preencha o campo C.H.S (Carga Horária Semanal).");
        }
        return valor;
    }

    private void adicionarEntidades(String[] valores, int cargaHorariaTotal) {
        List<String> idsEntidadesIdenticas = new ArrayList<>();

        while (cargaHorariaTotal > 0) {
            cargaHorariaTotal -= this.quantidadeAulasPorTurno;
            int cargaHoraria = cargaHorariaTotal >= 0 ? this.quantidadeAulasPorTurno : cargaHorariaTotal + this.quantidadeAulasPorTurno;
            adicionarEntidade(valores, idsEntidadesIdenticas, cargaHoraria);
        }

        for (String idEntidadesIdentica : idsEntidadesIdenticas) {
            int id = Integer.parseInt(idEntidadesIdentica) - 1;
            List<String> entidadesIdenticas = idsEntidadesIdenticas.stream()
                    .filter(idEntidade -> !idEntidade.equals(idEntidadesIdentica))
                    .collect(Collectors.toList());

            EntidadeDTO entidade = this.entidades.get(id);
            entidade.setIdsEntidadesIdenticas(entidadesIdenticas);
            entidade.adicionarEntidadesComConflitos(entidadesIdenticas);
        }
    }

    private void adicionarEntidade(String[] valores, List<String> idsEntidadesIdenticas, int cargaHoraria) {
        if (valores.length == this.cabecalho.size()) {
            int id = this.entidades.size() + 1;

            EntidadeDTO entidade = new EntidadeDTO();
            entidade.setId(id);
            entidade.setAtributos(this.cabecalho);
            entidade.setCargaHoraria(cargaHoraria);

            for (int i = 0; i < this.cabecalho.size(); i++) {
                entidade.put(this.cabecalho.get(i), valores[i]);
            }


            entidade.setNumero(Integer.parseInt(entidade.get(this.colunaIdentificador)));
            entidade.setNome(entidade.get(this.colunaNomeEntidade));
            entidade.setSemestre(entidade.get(this.colunaSemestre));
            entidade.setProfessor(entidade.get(this.colunaProfessor));

            AulaDTO aula = AulaDTO.builder()
                    .disciplina(entidade.getNome())
                    .turma(entidade.getSemestre())
                    .professor(entidade.getProfessor())
                    .cargaHoraria(entidade.getCargaHoraria())
                    .build();

            entidade.getAulas().add(aula);


            this.entidades.add(entidade);

            idsEntidadesIdenticas.add(String.valueOf(id));
        }
    }

    public void juntarColunas(String coluna, String coluna1, String coluna2) throws ColunaInexistenteException {
        try {
            validarColuna(coluna1);
            validarColuna(coluna2);

            for (EntidadeDTO entidade : this.entidades) {
                List<String> valores = StringUtils.getListaDoTexto(entidade.get(coluna1));
                valores.addAll(StringUtils.getListaDoTexto(entidade.get(coluna2)));
                entidade.put(coluna, StringUtils.getTextoDaLista(valores));
            }
        } catch (ColunaInexistenteException e) {
            LOGGER.error("Erro ao juntar colunas: " + StringUtils.limparLista(asList(coluna1, coluna2)));
            throw e;
        }
    }

    public void adicionarRestricaoColuna(String coluna) throws ColunaInexistenteException {
        if (!possuiColunaIdentificador()) return;

        validarColuna(coluna);

        this.colunasRestritas.add(coluna);

        List<String> valoresExistentes = getValoresExistentesColuna(coluna);

        List<ConflitoDTO> conflitos = gerarEntidadesConflitoParaCadaValor(valoresExistentes);

        this.entidades.forEach(entidade -> {
            for (ConflitoDTO conflito : conflitos) {
                List<String> valoresConflito = conflito.getValores();
                String valor = entidade.get(coluna);
                List<String> valores = StringUtils.getListaDoTexto(valor);

                for (String valorConflito : valoresConflito) {
                    if (temConflito(entidade, coluna, conflito)) {
                        conflito.adicionarEntidadeComConflito(String.valueOf(entidade.getId()));
                        break;
                    }
                }
            }
        });

        conflitos.forEach(conflito -> {
            List<String> listaDeEntidadesComConflitos = conflito.getListaDeEntidadesComConflitos();
            this.entidades.forEach(entidade -> {
                if (listaDeEntidadesComConflitos.contains(String.valueOf(entidade.getId()))) {
                    entidade.adicionarEntidadesComConflitos(listaDeEntidadesComConflitos);
                }
            });
        });
    }

    private boolean temConflito(EntidadeDTO entidade, String coluna, ConflitoDTO conflito) {
        if (this.colunaProfessor.equals(coluna)) {
            return entidade.getProfessores().contains(conflito.getValor());
        }

        return conflito.getValor().equals(String.valueOf(entidade.get(coluna)));
    }

    public void imprimirListaDeConflitos() {
        if (this.colunasRestritas.isEmpty()) {
            LOGGER.info("Adicione pelo menos uma restrição!");
            return;
        }


        if (!possuiColunaIdentificador()) return;

        LOGGER.info("------------------Lista de conflitos------------------");
        List<String> listaDeConflitos = getListaDeConflitos();


        for (String linhaListaDeConflitos : listaDeConflitos) {
            LOGGER.info(linhaListaDeConflitos);
        }

        LOGGER.info("------------------------------------------------------");

        exportarTxt(listaDeConflitos);
    }

    public List<String> getListaDeConflitos() {
        return this.entidades.stream().sorted(Comparator.comparing(EntidadeDTO::getId))
                .map(EntidadeDTO::getListaDeEntidadesComConflitos).map(this::getLinhaListaDeConflitos).collect(Collectors.toList());
    }

    private List<ConflitoDTO> gerarEntidadesConflitoParaCadaValor(List<String> valoresExistentes) {
        return valoresExistentes.stream().map(this::criarEntidadeConflito).collect(Collectors.toList());
    }

    private List<String> getValoresExistentesColuna(String coluna) {
        if (this.colunaProfessor.equals(coluna)) {
            List<String> valores = new ArrayList<>();

            this.entidades.forEach(entidade -> valores.addAll(entidade.getProfessores()));

            return valores.stream().distinct().collect(Collectors.toList());
        }
        return this.entidades.stream().map(entidade -> entidade.get(coluna)).distinct().collect(Collectors.toList());
    }

    private String getLinhaListaDeConflitos(List<String> listaConflito) {
//        return StringUtils.limparLista(listaConflito.stream().map(conflito -> conflito + "{" + this.entidades.get(Integer.parseInt(conflito) - 1).getNumero() + "}")
        return StringUtils.limparLista(listaConflito.stream().sorted().collect(Collectors.toList()).toString());
    }

    private void exportarTxt(List<String> listaDeConflitos) {
        try {
            if (isNull(this.nomeArquivo)) return;

            ExportarTxtUtils exportarTxtUtils = new ExportarTxtUtils(this.nomeArquivo);

            for (String linhaListaDeConflito : listaDeConflitos) {
                exportarTxtUtils.escreverLinha(linhaListaDeConflito);
            }

            exportarTxtUtils.fecharArquivo();
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("Erro ao exportar txt: ", e);
        }
    }

    private boolean possuiColunaIdentificador() {
        if (isNull(this.colunaIdentificador)) {
            LOGGER.error("Defina a coluna identificador!");
            return false;
        }

        return true;
    }

    private ConflitoDTO criarEntidadeConflito(String valor) {
        return new ConflitoDTO(valor);
    }

    private List<String> gerarListaAtributos(String linha) {
        return Arrays.stream(linha.split("\t")).collect(Collectors.toList());
    }
}
