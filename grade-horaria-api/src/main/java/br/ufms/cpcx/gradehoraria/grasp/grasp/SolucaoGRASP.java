package br.ufms.cpcx.gradehoraria.grasp.grasp;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.GradeHoraria;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.HorarioAulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;
import br.ufms.cpcx.gradehoraria.grasp.grasp.utils.AvaliadorSolucaoUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class SolucaoGRASP<T, E> {
    private T solucaoAtual;
    private List<Integer> cores;
    private List<E> candidatosColoridos;
    private GradeHoraria gradeHorariaAtual;
    private List<Candidato<E, EntidadeDTO>> candidatos;
    private List<Candidato<E, EntidadeDTO>> candidatosRestrito;

    public SolucaoGRASP() {
        this.candidatos = new ArrayList<>();
        inicializar();
    }

    public void inicializar() {
        this.cores = new ArrayList<>();
        this.candidatosRestrito = new ArrayList<>();
        this.candidatosColoridos = new ArrayList<>();
    }

    public List<Integer> getCores() {
        return this.cores;
    }

    public void setCores(List<Integer> cores) {
        this.cores = cores;
    }

    public List<E> getCandidatosColoridos() {
        return this.candidatosColoridos;
    }

    public List<Candidato<E, EntidadeDTO>> getCandidatos() {
        return this.candidatos;
    }

    public List<Candidato<E, EntidadeDTO>> getCandidatosAtivos() {
        return this.candidatos.stream().filter(candidato -> nonNull(candidato.getValor())).collect(Collectors.toList());
    }

    public void setCandidatos(List<Candidato<E, EntidadeDTO>> candidatos) {
        this.candidatos = candidatos;
    }

    public List<Candidato<E, EntidadeDTO>> getCandidatosRestrito() {
        return candidatosRestrito;
    }

    public void setCandidatosRestrito(List<Candidato<E, EntidadeDTO>> candidatosRestrito) {
        this.candidatosRestrito = candidatosRestrito;
    }

    public T getSolucaoAtual() {
        return this.solucaoAtual;
    }

    public void setSolucaoAtual(T solucaoAtual) {
        this.solucaoAtual = solucaoAtual;
    }

    public GradeHoraria getGradeHorariaAtual() {
        return this.gradeHorariaAtual;
    }

    public void setGradeHorariaAtual(GradeHoraria gradeHorariaAtual) {
        this.gradeHorariaAtual = gradeHorariaAtual;
    }

    public List<AulaDTO> getAulasEntidades() {
        List<AulaDTO> aulas = new ArrayList<>();
        getEntidades().stream().forEach(entidade -> aulas.addAll(entidade.getAulas()));
        return aulas;
    }

    public List<EntidadeDTO> getEntidades() {
        return this.candidatos.stream().map(Candidato::getEntidade).collect(Collectors.toList());
    }

    public Integer getProximaMenorCor(List<Integer> coresVizinhos) {
        return this.cores.stream().filter(cor -> !coresVizinhos.contains(cor))
                .mapToInt(cor -> cor).min().orElse(this.cores.size() + 1);
    }

    public int getAvaliacaoDias(String colunaLocal, int qtdLaboratorios) {
        return this.getAvaliacoesDias(colunaLocal, qtdLaboratorios).stream().mapToInt(v -> v).sum();
    }

    public List<Integer> getAvaliacoesDias(String colunaLocal, int qtdLaboratorios) {
        List<Integer> avaliacoesDias = new ArrayList<>();

        this.cores.forEach(cor -> {
            AtomicInteger soma = new AtomicInteger(qtdLaboratorios);
            this.gradeHorariaAtual.getDia(cor - 1).stream()
                    .map(HorarioAulaDTO::getEntidade)
                    .filter(Objects::nonNull).distinct()
                    .forEach(entidade -> {
                        String usaLaboratorio = this.candidatos.get(entidade - 1).getEntidade().get(colunaLocal);
                        if ("sim".equalsIgnoreCase(usaLaboratorio)) {
                            soma.getAndDecrement();
                        }
                    });
            avaliacoesDias.add(soma.get());
        });

        return avaliacoesDias;
    }

    public int getAvaliacaoTurmas() {
        return (int) AvaliadorSolucaoUtils.getQtdAvaliacoesNegativas(this.gradeHorariaAtual.getAvaliacoesTurmas()) * -1;
    }

    public List<Integer> getAvaliacoesTurmas() {
        return this.gradeHorariaAtual.getAvaliacoesTurmas();
    }

    public List<Integer> getCoresCandidatos(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        return candidatos.stream().map(candidato -> candidato.getValor().getCor()).distinct().collect(Collectors.toList());
    }

    public int getQtdCoresNecessaria(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        Map<String, Integer> qtdRepeticoesDeCadaTurmaNoSemestre = new TreeMap<>();
        for (Candidato<Vertice<Integer>, EntidadeDTO> candidato : candidatos) {
            String nomeEntidade = candidato.getEntidade().getNome();
            Integer qtdRepeticoesDisciplinaNoSemestre = qtdRepeticoesDeCadaTurmaNoSemestre.get(nomeEntidade);
            if (isNull(qtdRepeticoesDisciplinaNoSemestre)) {
                qtdRepeticoesDisciplinaNoSemestre = 0;
            }
            qtdRepeticoesDeCadaTurmaNoSemestre.put(nomeEntidade, ++qtdRepeticoesDisciplinaNoSemestre);
        }

        Optional<Integer> minimoDeCores = qtdRepeticoesDeCadaTurmaNoSemestre.values().stream().max(Comparator.comparing(Integer::intValue));
        return minimoDeCores.map(integer -> Integer.parseInt(String.valueOf(integer))).orElse(-1);
    }

    public List<Candidato<E, EntidadeDTO>> getCandidatosDia(int dia) {
        return this.gradeHorariaAtual.getCandidatosDia(dia).stream().map(candidato -> this.candidatos.get(candidato - 1)).collect(Collectors.toList());
    }

    public List<String> getSemestresCandidatos() {
        return getSemestresCandidatos(this.getCandidatos());
    }

    public List<String> getSemestresCandidatos(List<Candidato<E, EntidadeDTO>> candidatos) {
        return candidatos.stream().map(candidato -> candidato.getEntidade().getSemestre()).distinct().collect(Collectors.toList());
    }
}
