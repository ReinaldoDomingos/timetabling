package br.ufms.cpcx.gradehoraria.gradehoraria.entity;

import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.EDiaSemana;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "TB_RESTRICAO_GRADE_HORARIA")
public class RestricaoGradeHoraria {

    @Id
    @Column(name = "RGH_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "RGH_TIPO", nullable = false)
    private ETipoRestricao tipo;

    @Enumerated(EnumType.STRING)
    @Column(name = "RGH_DIA_SEMANA", nullable = false)
    private EDiaSemana diaDaSemana;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GRA_ID", nullable = false)
    private GradeHoraria gradeHoraria;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRO_ID", nullable = false)
    private Professor professor;
}
