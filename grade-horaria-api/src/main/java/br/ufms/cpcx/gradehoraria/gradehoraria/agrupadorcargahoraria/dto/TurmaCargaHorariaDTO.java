package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class TurmaCargaHorariaDTO {
    private String turma;
    private List<DiaCargaHorariaDTO> dias;

    public TurmaCargaHorariaDTO(String turma) {
        this.turma = turma;
        this.dias = new ArrayList<>();
    }

    public void adicionarDia(int dia, int cargaHorariaDisponivelTurma) {
        this.dias.add(new DiaCargaHorariaDTO(dia, cargaHorariaDisponivelTurma));
    }

    public int getCargaHorariaProfessorNoDia(Integer dia, String professor) {
        DiaCargaHorariaDTO diaCargaHorariaDTO = getDia(dia);

        return diaCargaHorariaDTO.getCargaProfessor(professor);
    }

    public DiaCargaHorariaDTO getDia(Integer dia) {
        return this.dias.stream()
                .filter(diaCargaHorariaDTO -> diaCargaHorariaDTO.getDia().equals(dia))
                .collect(Collectors.toList()).get(0);
    }

    public void adicionarDias(int qtdDias, Integer quantidadeAulasPorTurno) {
        for (int i = 0; i < qtdDias; i++) {
            this.adicionarDia(i + 1, quantidadeAulasPorTurno);
        }
    }
}
