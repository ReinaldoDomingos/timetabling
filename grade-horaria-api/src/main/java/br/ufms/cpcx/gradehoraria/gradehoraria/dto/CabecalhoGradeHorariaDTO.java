package br.ufms.cpcx.gradehoraria.gradehoraria.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.enumaration.EPeriodo;
import lombok.Data;

import java.util.List;

import static java.util.Arrays.asList;

@Data
public class CabecalhoGradeHorariaDTO {
    private EPeriodo periodo;
    private int maximoIteracoes;
    private int quantidadeAulasPorTurno;

    private String colunaSemestre;
    private String colunaProfessor;
    private String colunaDisciplina;
    private String colunaLaboratorio;
    private String colunaCargaHoraria;
    private String colunaIdentificador;

    public List<String> getColunas() {
        return asList(
                this.colunaIdentificador,
                this.colunaDisciplina,
                this.colunaProfessor,
                this.colunaSemestre,
                this.colunaCargaHoraria,
                this.colunaLaboratorio
        );
    }

    public List<String> getColunasRestricoesForte() {
        return asList(this.colunaProfessor, this.colunaSemestre);
    }
}
