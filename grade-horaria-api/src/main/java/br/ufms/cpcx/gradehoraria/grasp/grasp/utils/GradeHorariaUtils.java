package br.ufms.cpcx.gradehoraria.grasp.grasp.utils;

import br.ufms.cpcx.gradehoraria.generico.utils.DataUtils;
import br.ufms.cpcx.gradehoraria.generico.utils.StringUtils;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CandidatoUtils;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public class GradeHorariaUtils {
    private GradeHorariaUtils() {
    }

    public static void imprimirGradeHoraria(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, Integer quantidadeAulasPorTurno) {
        List<Integer> dias = CandidatoUtils.getDiasCandidatos(candidatos);
        List<String> diasDaSemana = DataUtils.getDiasDaSemana(dias);

        Map<String, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosPorSemestre = CandidatoUtils.getCandidatosPorSemestre(candidatos);

        List<String> semestres = new ArrayList<>(candidatosPorSemestre.keySet()).stream().sorted().collect(Collectors.toList());

        int qtdCaracteresDisciplina = 20;
        int qtdCaracteresTurmas = 5;

        System.out.println();
        System.out.print(StringUtils.criarStringComEspacos("Turma", qtdCaracteresTurmas) + " | ");
        diasDaSemana.forEach(diaDaSemana -> System.out.print(StringUtils.cortarStringEAdicionarEspacos(diaDaSemana, qtdCaracteresDisciplina) + " | "));
        System.out.println();
        for (String turma : semestres) {
            Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosPorDia = CandidatoUtils.getCandidatosPorDia(candidatosPorSemestre.get(turma));

            for (int i = 0; i < quantidadeAulasPorTurno; i++) {
                System.out.print(StringUtils.criarStringComEspacos(turma, qtdCaracteresTurmas) + " | ");
                for (Integer dia : dias) {
                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia = candidatosPorDia.get(dia);
                    if (nonNull(candidatosDia) && i < candidatosDia.get(0).getEntidade().getAulas().size()) {
                        String nome = StringUtils.cortarStringEAdicionarEspacos(candidatosDia.get(0).getEntidade().getAulas().get(0).getDisciplina(), qtdCaracteresDisciplina);
                        System.out.print(nome + " | ");
                    } else {
                        System.out.print(StringUtils.criarStringComEspacos(qtdCaracteresDisciplina) + " | ");
                    }
                }
                System.out.println();
            }
        }
        System.out.println(dias.size());
    }

    public static void imprimirCargaHorariaPorDia(Map<Integer, Integer> cargaHorariaPorDia) {
        List<Integer> dias = new ArrayList<>(cargaHorariaPorDia.keySet());
//        List<String> diasDaSemana = DataUtils.getDiasDaSemana(dias);
        int qtdCaracteres = 20;

        System.out.println();
        dias.forEach(dia -> System.out.print(StringUtils.criarStringComEspacos(dia.toString(), qtdCaracteres) + "\t"));
        System.out.println();
        dias.forEach(dia -> System.out.print(StringUtils.criarStringComEspacos(String.valueOf(cargaHorariaPorDia.get(dia)), qtdCaracteres) + "\t"));
        System.out.println();
    }
}
