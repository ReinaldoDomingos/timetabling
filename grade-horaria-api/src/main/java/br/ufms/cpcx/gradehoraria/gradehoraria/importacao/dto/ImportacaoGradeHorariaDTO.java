package br.ufms.cpcx.gradehoraria.gradehoraria.importacao.dto;

import lombok.Data;

@Data
public class ImportacaoGradeHorariaDTO {
    private Boolean isTeste;
    private String colunaTurma;
    private String nomeArquivo;
    private String colunaProfessor;
    private String colunaDisciplina;
    private String colunaCargaHorariaSemanal;
    private Integer quantidadeAulasPorTurno;
}
