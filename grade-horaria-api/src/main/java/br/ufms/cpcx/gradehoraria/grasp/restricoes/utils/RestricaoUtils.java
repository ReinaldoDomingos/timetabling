package br.ufms.cpcx.gradehoraria.grasp.restricoes.utils;

import br.ufms.cpcx.gradehoraria.grasp.restricoes.dto.RestricaoEntidade;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RestricaoUtils {
    private RestricaoUtils() {
    }

    public static List<RestricaoEntidade> getListaRestricoes() {
        return Arrays.stream(ETipoRestricao.values()).map(RestricaoEntidade::new).collect(Collectors.toList());
    }
}
