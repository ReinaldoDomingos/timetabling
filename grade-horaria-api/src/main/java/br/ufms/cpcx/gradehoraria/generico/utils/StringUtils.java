package br.ufms.cpcx.gradehoraria.generico.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public class StringUtils {
    private StringUtils() {
    }

    public static String removerColchetes(String texto) {
        return texto.replace("[", "").replace("]", "");
    }

    public static String limparLista(List<String> texto) {
        return limparLista(texto.toString());
    }

    public static String limparLista(String texto) {
        return removerColchetes(texto.replace(", ", ","));
    }

    public static String getTextoDaLista(List<String> valores) {
        return limparLista(valores.stream().filter(valor -> !valor.isEmpty()).collect(Collectors.toList()).toString());
    }

    public static List<String> getListaDoTexto(String texto) {
        return getListaDoTexto(texto, ",");
    }

    public static List<String> getListaDoTexto(String texto, String separador) {
        return Arrays.stream(texto.split(separador)).collect(Collectors.toList());
    }

    public static String criarStringComEspacos(int qtdEspacos) {
        return criarStringComEspacos("", qtdEspacos);
    }

    public static String criarStringComEspacos(String stringInicial, int qtdCaracteres) {
        StringBuilder stringBuilder = new StringBuilder(stringInicial);
        for (int i = stringInicial.length(); i < qtdCaracteres; i++) {
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    public static String cortarStringEAdicionarEspacos(String string, int qtdCaracteres) {
        if (nonNull(string) && string.length() > qtdCaracteres) {
            return string.substring(0, qtdCaracteres);
        }
        return criarStringComEspacos(string, qtdCaracteres);
    }
}
