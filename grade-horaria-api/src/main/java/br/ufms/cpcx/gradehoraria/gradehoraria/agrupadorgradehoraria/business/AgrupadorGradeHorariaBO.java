package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.business;

import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.DiaGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.GradeHorariaNovaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.ProfessorGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.TurmaGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CargaHorariaUtils;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
public class AgrupadorGradeHorariaBO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgrupadorGradeHorariaBO.class);

    public GradeHorariaNovaDTO gerarGradeHoraria(Integer quantidadeAulasPorTurno, List<EntidadeDTO> entidades) {

        GradeHorariaNovaDTO gradeHorariaNovaDTO = new GradeHorariaNovaDTO(quantidadeAulasPorTurno);

        List<String> tumas = entidades.stream().map(EntidadeDTO::getSemestre).distinct().collect(Collectors.toList());

        List<EntidadeDTO> entidadesGradeHoraria = new ArrayList<>(entidades);
        List<EntidadeDTO> entidadesProximosDia = new ArrayList<>();

        int posDiaAnterior;
        int maximoTentativas = 10;
        while (!entidadesGradeHoraria.isEmpty()) {
//            Collections.shuffle(entidadesGradeHoraria);
            DiaGradeHorariaDTO diaGradeHorariaDTO = getMelhorDiaGradeHorariaDTO(gradeHorariaNovaDTO, quantidadeAulasPorTurno);

            for (int tentativas = 0; tentativas < maximoTentativas; tentativas++) {
                Map<String, List<EntidadeDTO>> entidadesPorTurma = entidadesGradeHoraria.stream().collect(Collectors.groupingBy(EntidadeDTO::getSemestre));

                entidadesPorTurma.forEach((turma, entidadesTurma) -> adicionarTurma(entidadesGradeHoraria, diaGradeHorariaDTO, turma, entidadesTurma, gradeHorariaNovaDTO.getQuantidadeAulasPorTurno()));

                int qtdCargaHorariaFaltando = diaGradeHorariaDTO.getCargaHorariaTotal() % quantidadeAulasPorTurno;
                int qtdTurmasFaltando = tumas.size() - diaGradeHorariaDTO.getTurmas().size();

                LOGGER.info("Dia " + diaGradeHorariaDTO.getDia() + " falta " + qtdCargaHorariaFaltando + " aulas e " + qtdTurmasFaltando + " turmas");
                LOGGER.info(diaGradeHorariaDTO.getTurmas().size() + " turmas");
                if (tentativas < maximoTentativas - 1 && qtdCargaHorariaFaltando > 0) {
                    List<TurmaGradeHorariaDTO> turmasIncompletas = diaGradeHorariaDTO.getTurmas().stream()
                            .filter(turmaGradeHorariaDTO -> quantidadeAulasPorTurno - turmaGradeHorariaDTO.getCargaHorariaTotal() > 0)
                            .collect(Collectors.toList());

                    for (TurmaGradeHorariaDTO turmaIncompleta : turmasIncompletas) {
                        entidadesProximosDia.addAll(turmaIncompleta.getEntidades());
                    }

                    diaGradeHorariaDTO.getTurmas().removeAll(turmasIncompletas);
//                } else if (tentativas < maximoTentativas/2 && qtdTurmasFaltando > 0) {
//                    List<Entidade> finalEntidadesProximosDia = entidadesProximosDia;
//                    diaGradeHorariaDTO.getTurmas().forEach(turmaGradeHorariaDTO -> {
//                        ProfessorGradeHorariaDTO professorGradeHorariaDTO = turmaGradeHorariaDTO.getProfessores().get(0);
//                        if (professorGradeHorariaDTO.getEntidades().size() > 0) {
//                            finalEntidadesProximosDia.add(professorGradeHorariaDTO.getEntidades().get(0));
//                            Entidade entidade = professorGradeHorariaDTO.getEntidades().get(0);
//                            professorGradeHorariaDTO.getEntidades().remove(entidade);
//                        }
//                    });
                } else if (qtdCargaHorariaFaltando == 0 && qtdTurmasFaltando == 0) {
                    break;
                }
                LOGGER.info(diaGradeHorariaDTO.getTurmas().size() + " turmas");
            }

            entidadesGradeHoraria.addAll(entidadesProximosDia);
            entidadesProximosDia = new ArrayList<>();
        }

        return gradeHorariaNovaDTO;
    }

    private DiaGradeHorariaDTO getMelhorDiaGradeHorariaDTO(GradeHorariaNovaDTO gradeHoraria, Integer quantidadeAulasPorTurno) {
        return gradeHoraria.adicionarDia(gradeHoraria.getDias().size() + 1);
    }

    private void adicionarTurma(List<EntidadeDTO> entidades, DiaGradeHorariaDTO diaGradeHorariaDTO, String turma, List<EntidadeDTO> entidadesTurma, Integer quantidadeAulasPorTurno) {
        TurmaGradeHorariaDTO turmaGradeHorariaDTO = diaGradeHorariaDTO.getTurma(turma);

        if (isNull(turmaGradeHorariaDTO)) {
            turmaGradeHorariaDTO = diaGradeHorariaDTO.adicionarTurma(turma);
        }

        entidadesTurma.sort(Comparator.comparing(EntidadeDTO::getProfessor));
//        if (turmaGradeHorariaDTO.getCargaHorariaTotal() < quantidadeAulasPorTurno) {
        Map<String, List<EntidadeDTO>> entidadesProfessores = entidadesTurma.stream().collect(Collectors.groupingBy(EntidadeDTO::getProfessor));

        for (Map.Entry<String, List<EntidadeDTO>> entry : entidadesProfessores.entrySet()) {
            String professor = entry.getKey();
            List<EntidadeDTO> entidadesProfessor = entry.getValue();

            Integer cargaHorariaTotalTurma = turmaGradeHorariaDTO.getCargaHorariaTotal();
            Integer cargaHorariaTotalProfesorNaTurma = turmaGradeHorariaDTO.getCargaHorariaTotalProfesor(professor);
            Integer cargaHorariaTotalProfesorNoDia = diaGradeHorariaDTO.getCargaHorariaTotalProfesor(professor);
//            Integer cargaHorariaProfessorNoDia = diaGradeHorariaDTO.getTurmas().stream().mapToInt(turmaGradeHoraria ->
//                    turmaGradeHoraria.getProfessores().stream().filter(professorGradeHorariaDTO1 -> professorGradeHorariaDTO1.getProfessor().equals(professor))
//                            .mapToInt(ProfessorGradeHorariaDTO::getCargaHorariaTotal).sum()).sum();

            if (possuiHorarioDisponivel(cargaHorariaTotalTurma, cargaHorariaTotalProfesorNaTurma, quantidadeAulasPorTurno)
                    && cargaHorariaTotalProfesorNoDia < quantidadeAulasPorTurno) {
//                if (cargaHorariaTotalTurma < quantidadeAulasPorTurno) {
                adicionarProfessor(diaGradeHorariaDTO, entidades, turmaGradeHorariaDTO, professor, entidadesProfessor, quantidadeAulasPorTurno);
            }
        }
//        }
    }

    private boolean possuiHorarioDisponivel(Integer cargaHorariaTotalTurma,
                                            Integer cargaHorariaTotalProfesorNaTurma,
                                            Integer quantidadeAulasPorTurno) {

        return cargaHorariaTotalTurma < quantidadeAulasPorTurno
                && cargaHorariaTotalProfesorNaTurma < quantidadeAulasPorTurno;
    }

    private void adicionarProfessor(DiaGradeHorariaDTO diaGradeHorariaDTO, List<EntidadeDTO> entidades, TurmaGradeHorariaDTO turmaGradeHorariaDTO, String professor, List<EntidadeDTO> entidadesProfessor, Integer quantidadeAulasPorTurno) {
        entidadesProfessor.sort(Comparator.comparing(EntidadeDTO::getCargaHorariaTotal));

        ProfessorGradeHorariaDTO professorGradeHorariaDTO = turmaGradeHorariaDTO.getProfessor(professor);

        if (isNull(professorGradeHorariaDTO)) {
            professorGradeHorariaDTO = turmaGradeHorariaDTO.adicionarProfessor(professor);
        }

        while (!entidadesProfessor.isEmpty()) {
            EntidadeDTO entidade = entidadesProfessor.remove(0);
            if (turmaGradeHorariaDTO.getCargaHorariaTotal() + entidade.getCargaHorariaTotal() <= quantidadeAulasPorTurno) {
                adicionarEntidades(diaGradeHorariaDTO, entidades, professorGradeHorariaDTO, entidade, quantidadeAulasPorTurno);
            }
        }
    }

    private void adicionarEntidades(DiaGradeHorariaDTO diaGradeHorariaDTO, List<EntidadeDTO> entidades, ProfessorGradeHorariaDTO professorGradeHorariaDTO, EntidadeDTO entidadeProfessor, Integer quantidadeAulasPorTurno) {
        Integer dia = diaGradeHorariaDTO.getDia();
        String professor = entidadeProfessor.getProfessor();

        List<EntidadeDTO> entidadesDia = diaGradeHorariaDTO.getEntidades();
        int cargaHorariaProfessorNoDia = CargaHorariaUtils.getCargaHorariaProfessorEntidades(professor, entidadesDia);


//        if (professor.equals("Flora")) {
//            LOGGER.info(dia + " " + professor + " " + cargaHorariaProfessorNoDia);
//        } else if (professor.contains("Flora")) {
//            LOGGER.info(dia + " " + professor + " " + cargaHorariaProfessorNoDia);

//        }
        if (cargaHorariaProfessorNoDia + entidadeProfessor.getCargaHorariaTotal() <= quantidadeAulasPorTurno) {
            entidadeProfessor.setDiaInicial(dia);
            entidadeProfessor.getAulas().forEach(aula -> aula.setDia(String.valueOf(diaGradeHorariaDTO)));
            entidades.remove(entidadeProfessor);
            professorGradeHorariaDTO.adicionarEntidade(entidadeProfessor);
        }
    }
}
