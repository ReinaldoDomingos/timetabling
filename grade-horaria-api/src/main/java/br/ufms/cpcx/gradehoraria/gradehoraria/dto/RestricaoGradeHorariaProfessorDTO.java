package br.ufms.cpcx.gradehoraria.gradehoraria.dto;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import lombok.Data;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class RestricaoGradeHorariaProfessorDTO {
    private ProfessorDTO professor;

    private List<RestricaoGradeHorariaDTO> restricoes;

    public RestricaoGradeHorariaProfessorDTO(ProfessorDTO professor, List<RestricaoGradeHoraria> restricaoGradeHoraria) {
        this.professor = professor;
        this.restricoes = restricaoGradeHoraria.stream().map(RestricaoGradeHorariaDTO::new).collect(Collectors.toList());
    }

    public static RestricaoGradeHoraria toMapRestricaoGradeHoraria(RestricaoGradeHorariaProfessorDTO restricaoGradeHorariaDTO) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(restricaoGradeHorariaDTO, RestricaoGradeHoraria.class);
    }
}
