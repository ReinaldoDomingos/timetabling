package br.ufms.cpcx.gradehoraria.arquivos.dto;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public class GradeHorariaXLS {

    private HSSFWorkbook planilha;
    private final List<HSSFSheet> paginas;
    private HSSFSheet paginaAtual;
    private HSSFCellStyle estiloTextoAulas;

    public GradeHorariaXLS() {
        this.planilha = new HSSFWorkbook();
        this.paginas = new ArrayList<>();

        this.estiloTextoAulas = planilha.createCellStyle();
        this.estiloTextoAulas.setWrapText(true);

        setEstilosPadrao(this.estiloTextoAulas);
        adicionarPagina();
        setPaginaAtual(0);
    }

    public HSSFSheet getPaginaAtual() {
        return paginaAtual;
    }

    public void setPaginaAtual(int pagina) {
        this.paginaAtual = this.paginas.get(pagina);
    }

    public HSSFWorkbook getPlanilha() {
        return planilha;
    }

    public List<HSSFSheet> getPaginas() {
        return paginas;
    }

    public HSSFCellStyle getEstiloTextoAulas() {
        return estiloTextoAulas;
    }

    public void adicionarPagina() {
        this.adicionarPagina("Pagina " + (this.paginas.isEmpty() ? 1 : this.paginas.size() + 1));
    }

    public void adicionarPagina(String nomePagina) {
        HSSFSheet pagina = planilha.createSheet(nomePagina);
        this.paginas.add(pagina);

        pagina.setDefaultColumnWidth(25);
        pagina.setDefaultRowHeight((short) 1500);
    }

    public String gerarBase64() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            planilha.write(baos);
            baos.close();
            return Base64.getEncoder().encodeToString(baos.toByteArray());
        } catch (Exception exception) {
            throw new GenericException("Erro ao gerar Relatório!");
        }
    }

    public Map<String, String> exportarBase64() {
        Map<String, String> resp = new HashMap<>();
        resp.put("relatorio", gerarBase64());
        return resp;
    }

    public void adicionarLinhaCabecalho(int posicao, List<String> valoresColunas) {
        HSSFRow linha = this.paginaAtual.createRow(posicao);

        HSSFCellStyle estiloTextoCabecalho = criarEstiloCabecalho();

        adicionarColunas(linha, valoresColunas, estiloTextoCabecalho);
    }

    public HSSFRow adicionarLinha(int posicao, List<String> valoresColunas, HSSFCellStyle estilo) {
        HSSFRow linhaExistente = this.paginaAtual.getRow(posicao);
        HSSFRow linha = nonNull(linhaExistente) ? linhaExistente : this.paginaAtual.createRow(posicao);
        linha.setHeight((short) 750);

        adicionarColunas(linha, valoresColunas, estilo);

        return linha;
    }

    private void adicionarColunas(HSSFRow linha, List<String> valoresColunas, HSSFCellStyle estilo) {
        for (int i = 0; i < valoresColunas.size(); i++) {
            String valor = valoresColunas.get(i);
            adicionarColuna(i, linha, valor, estilo);
        }
    }

    public void adicionarColuna(int indiceColuna, HSSFRow linha, String valor, HSSFCellStyle estilo) {
        HSSFCell coluna = linha.createCell(indiceColuna);
        coluna.setCellValue(valor);

        if (nonNull(estilo)) {
            setEstilosPadrao(estilo);
            coluna.setCellStyle(estilo);
        }
    }

    private HSSFCellStyle criarEstiloCabecalho() {
        HSSFFont fonteNegrita = criarFonteNegrita();
        HSSFCellStyle estiloTextoCabecalho = criarEstilo();
        estiloTextoCabecalho.setFont(fonteNegrita);
        estiloTextoCabecalho.setFont(fonteNegrita);
        return estiloTextoCabecalho;
    }

    private HSSFFont criarFonteNegrita() {
        HSSFFont fonteNegrita = criarFonte();
        fonteNegrita.setBold(true);
        return fonteNegrita;
    }

    private HSSFCellStyle criarEstilo() {
        return this.planilha.createCellStyle();
    }

    private HSSFFont criarFonte() {
        return this.planilha.createFont();
    }

    private void setEstilosPadrao(CellStyle estilo) {
        estilo.setAlignment(HorizontalAlignment.CENTER);
        estilo.setVerticalAlignment(VerticalAlignment.CENTER);
        estilo.setBorderBottom(BorderStyle.MEDIUM);
        estilo.setBorderLeft(BorderStyle.MEDIUM);
        estilo.setBorderTop(BorderStyle.MEDIUM);
        estilo.setBorderRight(BorderStyle.MEDIUM);
        estilo.setBottomBorderColor(IndexedColors.GREY_80_PERCENT.getIndex());
        estilo.setTopBorderColor(IndexedColors.GREY_80_PERCENT.getIndex());
        estilo.setRightBorderColor(IndexedColors.GREY_80_PERCENT.getIndex());
        estilo.setLeftBorderColor(IndexedColors.GREY_80_PERCENT.getIndex());
    }

    public HSSFCellStyle gerarEstiloColorido(short corCelula) {
        HSSFCellStyle estilo;
        estilo = this.planilha.createCellStyle();
        estilo.setFillForegroundColor(corCelula);
        estilo.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estilo.setWrapText(true);

        return estilo;
    }

    public static List<Short> getCoresTextoPadrao() {
        return Arrays.stream(HSSFColor.HSSFColorPredefined.values())
                .map(HSSFColor.HSSFColorPredefined::getIndex)
                .collect(Collectors.toList());
    }
}
