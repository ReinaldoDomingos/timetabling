package br.ufms.cpcx.gradehoraria.generico.enumaration;

public enum EStatus {
    ATIVO,
    INATIVO
}
