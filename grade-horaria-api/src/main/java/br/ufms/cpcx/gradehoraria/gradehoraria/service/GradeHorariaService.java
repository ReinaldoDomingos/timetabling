package br.ufms.cpcx.gradehoraria.gradehoraria.service;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.filter.GenericFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.GradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.GradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.DisciplinaGradeHorariaRepository;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.GradeHorariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class GradeHorariaService {

    @Autowired
    private GradeHorariaRepository gradeHorariaRepository;

    @Autowired
    private DisciplinaGradeHorariaRepository disciplinaGradeHorariaRepository;

    @Autowired
    private TurmaService turmaService;

    @Autowired
    private ProfessorService professorService;

    @Autowired
    private DisciplinaService disciplinaService;

    @Autowired
    private RestricaoGradeHorariaService restricaoGradeHorariaService;

    @Autowired
    private DisciplinaGradeHorariaService disciplinaGradeHorariaService;

    public Page<GradeHorariaDTO> buscarTodos(GenericFilter filter) {
        return gradeHorariaRepository.findAll(filter.getPageRequest()).map(GradeHorariaDTO::fromGradeHoraria);
    }

    public GradeHorariaDTO buscarPorId(Long id) {
        GradeHoraria gradeHorariaSalva = gradeHorariaRepository.buscarPorId(id);

        int totalCargaHorariasDisciplinasGradeHoraria = disciplinaGradeHorariaService.buscarTotalCargaHorariasDisciplinasPorGradeHorariaId(id);

        gradeHorariaSalva.setCargaHorariaSemanalTotal(totalCargaHorariasDisciplinasGradeHoraria);

        return GradeHorariaDTO.fromGradeHoraria(gradeHorariaSalva);
    }

    public GradeHorariaDTO salvar(GradeHorariaDTO gradeHorariaDTO) {
        if (existeGradeHorariaIgual(GradeHorariaDTO.toMapGradeHoraria(gradeHorariaDTO)))
            throw new GenericException("Grade Horária já existe.");

        GradeHorariaDTO gradeHorariaDTOSalva = salvarGradeHoraria(gradeHorariaDTO);
        gradeHorariaDTOSalva.setCargaHorariaSemanalTotal(0);

        return gradeHorariaDTOSalva;
    }

    public GradeHorariaDTO alterar(Long id, GradeHorariaDTO gradeHorariaDTO) {
        GradeHoraria gradeHoraria = GradeHorariaDTO.toMapGradeHoraria(gradeHorariaDTO);

        if (!id.equals(gradeHoraria.getId())) {
            throw new GenericException("Erro ao atualizar o registro.");
        }

        return salvarGradeHoraria(gradeHorariaDTO);
    }

    private GradeHorariaDTO salvarGradeHoraria(GradeHorariaDTO gradeHoraria) {
        GradeHoraria gradeHorariaSalva = gradeHorariaRepository.save(GradeHorariaDTO.toMapGradeHoraria(gradeHoraria));

        return GradeHorariaDTO.fromGradeHoraria(gradeHorariaSalva);
    }

    @Transactional("transactionManager")
    public void deletar(Long id) {
        Optional<GradeHoraria> gradeHorariaOptional = gradeHorariaRepository.findById(id);

        if (gradeHorariaOptional.isPresent()) {
            GradeHoraria gradeHoraria = gradeHorariaOptional.get();

            disciplinaGradeHorariaRepository.deleteByGradeHorariaId(gradeHoraria.getId());
            restricaoGradeHorariaService.deletarTodosPorGradeHorariaId(gradeHoraria.getId());

            deletarDadosDeTeste(gradeHoraria);
            gradeHorariaRepository.deleteById(gradeHoraria.getId());
        }
    }

    private void deletarDadosDeTeste(GradeHoraria gradeHoraria) {
        if (Boolean.TRUE.equals(gradeHoraria.getIsTeste())) {
            turmaService.deletarTodosPorGradeHorariaId(gradeHoraria.getId());
            professorService.deletarTodosPorGradeHorariaId(gradeHoraria.getId());
            disciplinaService.deletarTodosPorGradeHorariaId(gradeHoraria.getId());
        }
    }

    private boolean existeGradeHorariaIgual(GradeHoraria gradeHoraria) {
        return gradeHorariaRepository.existsGradeHorariaByAnoAndSemestreAno(gradeHoraria.getAno(), gradeHoraria.getSemestreAno());
    }
}
