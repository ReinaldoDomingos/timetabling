package br.ufms.cpcx.gradehoraria.grasp.restricoes.dto;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;

import java.util.List;

public class RestricaoProfessor extends Restricao {
    private List<RestricaoGradeHoraria> restricaoProfessores;

    public RestricaoProfessor(ETipoRestricao tipo) {
        super(tipo);
    }

    public List<RestricaoGradeHoraria> getRestricaoProfessores() {
        return restricaoProfessores;
    }

    public void setRestricaoProfessores(List<RestricaoGradeHoraria> restricoesProfessores) {
        this.restricaoProfessores = restricoesProfessores;
    }
}
