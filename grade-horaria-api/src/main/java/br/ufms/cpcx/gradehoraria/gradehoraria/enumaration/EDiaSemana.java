package br.ufms.cpcx.gradehoraria.gradehoraria.enumaration;

public enum EDiaSemana {
    SEGUNDA_FEIRA("Segunda"),
    TERCA_FEIRA("Terça-feira"),
    QUARTA_FEIRA("Quarta-feira"),
    QUINTA_FEIRA("Quinta-feira"),
    SEXTA_FEIRA("Sexta-feira"),
    SABADO("Sábado"),
    DOMINGO("Domingo");

    private final String valor;

    EDiaSemana(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
