package br.ufms.cpcx.gradehoraria.grasp.restricoes.business;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.EDiaSemana;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CandidatoUtils;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;
import br.ufms.cpcx.gradehoraria.grasp.grasp.impl.GRASPImpl;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.dto.DiaRestricaoDTO;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.dto.GradeHorariaRestricaoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AvaliadorRestricoesBO {
    private static final Logger LOGGER = LoggerFactory.getLogger(AvaliadorRestricoesBO.class);

    public GradeHorariaRestricaoDTO avaliarSolucao(GRASPImpl grasp, Map<Professor, List<RestricaoGradeHoraria>> restricoesAgrupadasPorProfessor) {
        List<Professor> professores = new ArrayList<>(restricoesAgrupadasPorProfessor.keySet());

        List<String> diasDaSemana = Arrays.stream(EDiaSemana.values()).map(EDiaSemana::getValor).collect(Collectors.toList());


        GradeHorariaRestricaoDTO gradeHorariaAvaliacaoRestricaoDTO = new GradeHorariaRestricaoDTO();

        for (Professor professor : professores) {
            List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDoProfessor = CandidatoUtils.getCandidatosDoProfessor(grasp.getSolucaoGRASP().getCandidatos(), professor.getNome());
            List<RestricaoGradeHoraria> restricoesProfessor = restricoesAgrupadasPorProfessor.get(professor);
            List<Integer> diasComRestricao = restricoesProfessor.stream().map(restricaoGradeHoraria -> diasDaSemana.indexOf(restricaoGradeHoraria.getDiaDaSemana().getValor())).collect(Collectors.toList());
            List<Integer> diasCandidatos = CandidatoUtils.getDiasCandidatos(candidatosDoProfessor);
            diasComRestricao.forEach(dia -> {
                if (diasCandidatos.contains(dia)) {
                    DiaRestricaoDTO diaRestricaoDTO = gradeHorariaAvaliacaoRestricaoDTO.getDia(dia);
                    diaRestricaoDTO.setAvaliacaoNegativa(diaRestricaoDTO.getAvaliacaoNegativa() - 1);
                }
            });
        }

        gradeHorariaAvaliacaoRestricaoDTO.getDias().forEach(diaRestricaoDTO -> {
            System.out.println(diaRestricaoDTO.getDia() + " = " + diaRestricaoDTO.getAvaliacaoNegativa());
        });

        return gradeHorariaAvaliacaoRestricaoDTO;
    }
}
