package br.ufms.cpcx.gradehoraria.grasp.grasp;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.enumaration.EPeriodo;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.MelhorSolucaoDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.exception.NenhumaRestricaoFracaAtivaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static java.util.Objects.isNull;

public abstract class GRASP<T, E> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GRASP.class);

    protected MelhorSolucaoDTO<T> melhorSolucao;
    protected EPeriodo periodo;
    protected SolucaoGRASP<T, E> solucaoGRASP;
    protected int tamanhoListaRestritaDeCandidatos;
    protected boolean isPossuiEntradaInicialJaColorida;

    protected GRASP(EPeriodo periodo, int tamanhoListaRestritaDeCandidatos) {
        this.periodo = periodo;
        this.tamanhoListaRestritaDeCandidatos = tamanhoListaRestritaDeCandidatos;
    }

    public SolucaoGRASP<T, E> getSolucaoGRASP() {
        return solucaoGRASP;
    }

    public MelhorSolucaoDTO<T> execute(int maximoIteracoes) throws NenhumaRestricaoFracaAtivaException {
        long tempoIncial = this.logIniciar();

        this.solucaoGRASP = new SolucaoGRASP<>();

        this.lerDados();

        this.logPosicaoAtual(tempoIncial, "Leitura dados finalizada");

        int i = 0;
//        int maximo = 1;
        int maximo = maximoIteracoes;
        while (i < maximo && (isNull(this.melhorSolucao) || this.melhorSolucao.getCores().size() > 5 || i < maximoIteracoes)) {
            this.logPosicaoAtual(tempoIncial, "------------ Iteração " + (i + 1) + " iniciada ------------");
            T solucao = this.construirSolucao();

//            if (!ehValida(solucao)) {
            this.repararSolucao(solucao);
//            }

            solucao = this.buscaLocal(solucao);

            this.atualizarMelhorSolucao(solucao);

            this.logPosicaoAtual(tempoIncial, "------------ Iteração " + (i + 1) + " finalizada. Restam: " + (maximoIteracoes * 2 - (i + 1) + " ------------"));
            i++;
        }

        String menagemLog = "Melhor solução : " + this.melhorSolucao.toString();
        LOGGER.info(menagemLog);

        this.logPosicaoAtual(tempoIncial, "------------ GRASP Finalizado ------------");

        return this.melhorSolucao;
    }

    public abstract T construirSolucaoComGradeHorariaJaColorida();

    public T construirSolucao() {
        T solucao = null;

        if (this.isPossuiEntradaInicialJaColorida) {
            solucao = this.construirSolucaoComGradeHorariaJaColorida();
        } else {
            this.avaliarCandidatos();

            executarRestricoesPreColoracao();

            while (this.solucaoGRASP.getCandidatosColoridos().size() != this.solucaoGRASP.getCandidatos().size()) {

                this.construirLRC();

                Candidato<E, EntidadeDTO> candidato = this.selecionarCandidato();

                solucao = this.adicionarCandidatoNaSolucao(candidato);

                this.atualizarListaCandidatos(candidato);

                this.avaliarCandidatos();
            }

            String mensgemLog = this.solucaoGRASP.getCores().size() + " cores";
            LOGGER.info(mensgemLog);

            executarRestricoesPosColoracao();
        }
        return solucao;
    }

    protected abstract void executarRestricoesPreColoracao();

    protected abstract void executarRestricoesPosColoracao();

    public abstract T buscaLocal(T solucao);

    public abstract void avaliarCandidatos();

    public void atualizarListaCandidatos(Candidato<E, EntidadeDTO> candidato) {
        if (!this.solucaoGRASP.getCandidatosColoridos().contains(candidato.getValor())) {
            this.solucaoGRASP.getCandidatosColoridos().add(candidato.getValor());
        }
    }

    public abstract T adicionarCandidatoNaSolucao(Candidato<E, EntidadeDTO> candidato);

    public abstract Candidato<E, EntidadeDTO> selecionarCandidato();

    public abstract void construirLRC();

    public abstract void atualizarMelhorSolucao(T solucao);

    public abstract void lerDados();

    public abstract void repararSolucao(T solucao);

    public List<Integer> getCores() {
        return this.solucaoGRASP.getCores();
    }

    public void setCores(List<Integer> cores) {
        this.solucaoGRASP.setCores(cores);
    }

    public abstract boolean ehValida(T solucao) throws NenhumaRestricaoFracaAtivaException;

    public abstract void setColunasRestricoesFortes(List<String> colunasRestricoesFortes);

    private long logIniciar() {
        long tempoInicial = System.currentTimeMillis();

        String mensagemLog = "------------ Iniciando GRASP: Inicio -> '" + tempoInicial + "'. ------------";

        LOGGER.info(mensagemLog);
        return tempoInicial;
    }

    public void logPosicaoAtual(Long tempoInicial, String posicaoAtual) {
        long tempoFinal = System.currentTimeMillis();

        String mensagemLog = "Etapa do GRASP finalizada: " +
                "Inicio -> '" + tempoInicial + "'. " +
                "Posição atual -> '" + posicaoAtual + "'. " +
                "Tempo atual ->  " + ((tempoFinal - tempoInicial) / 1000) + "s.";

        LOGGER.info(mensagemLog);
    }

    public void setPossuiEntradaInicialJaColorida(boolean possuiEntradaInicialJaColorida) {
        isPossuiEntradaInicialJaColorida = possuiEntradaInicialJaColorida;
    }
}
