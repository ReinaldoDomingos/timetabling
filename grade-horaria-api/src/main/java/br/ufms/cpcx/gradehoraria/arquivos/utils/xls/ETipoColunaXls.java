package br.ufms.cpcx.gradehoraria.arquivos.utils.xls;

public enum ETipoColunaXls {
    TEXTO,
    NUMERICO
}
