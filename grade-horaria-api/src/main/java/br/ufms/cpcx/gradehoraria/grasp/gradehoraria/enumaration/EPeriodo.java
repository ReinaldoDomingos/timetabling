package br.ufms.cpcx.gradehoraria.grasp.gradehoraria.enumaration;

public enum EPeriodo {
    UNICO,
    INTEGRAL
}
