package br.ufms.cpcx.gradehoraria.gradehoraria.service;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.ProfessorDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.RestricaoGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.RestricaoGradeHorariaProfessorDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.GradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.EDiaSemana;
import br.ufms.cpcx.gradehoraria.gradehoraria.filter.RestricaoGradeHorariaFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.RestricaoGradeHorariaRepository;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
public class RestricaoGradeHorariaService {

    @Autowired
    private RestricaoGradeHorariaRepository restricaoGradeHorariaRepository;

    @Autowired
    private DisciplinaGradeHorariaService disciplinaGradeHorariaService;

    public void salvarRestricoes(Long idGradeHoraria, Long idProfessor, List<EDiaSemana> diaDaSemanas) {
        ETipoRestricao tipo = ETipoRestricao.DIAS_PROFESSOR_INDISPONIVEL;

        List<RestricaoGradeHoraria> restricoesProfessorExistentes = restricaoGradeHorariaRepository.buscarFiltroPorProfessor(idGradeHoraria, tipo, idProfessor);
        List<EDiaSemana> diasExistentes = restricoesProfessorExistentes.stream().map(RestricaoGradeHoraria::getDiaDaSemana).collect(Collectors.toList());
        List<EDiaSemana> copiaExistentes = new ArrayList<>(diasExistentes);

        copiaExistentes.forEach(diaSemana -> {
            if (diaDaSemanas.contains(diaSemana)) {
                diaDaSemanas.remove(diaSemana);
                diasExistentes.remove(diaSemana);
            }
        });

        diaDaSemanas.forEach(diaSemana -> {
            RestricaoGradeHoraria restricaoNova = criarRestricaoGradeHoraria(idGradeHoraria, idProfessor, tipo);
            restricaoNova.setDiaDaSemana(diaSemana);
            salvar(new RestricaoGradeHorariaDTO(restricaoNova));
        });

        restricoesProfessorExistentes.stream()
                .filter(restricaoGradeHoraria -> diasExistentes.contains(restricaoGradeHoraria.getDiaDaSemana()))
                .forEach(restricaoGradeHoraria -> deletar(restricaoGradeHoraria.getId()));
    }

    private RestricaoGradeHoraria criarRestricaoGradeHoraria(Long idGradeHoraria, Long idProfessor, ETipoRestricao tipo) {
        GradeHoraria gradeHoraria = new GradeHoraria();
        gradeHoraria.setId(idGradeHoraria);

        Professor professor = new Professor();
        professor.setId(idProfessor);

        RestricaoGradeHoraria restricaoNova = new RestricaoGradeHoraria();
        restricaoNova.setGradeHoraria(gradeHoraria);
        restricaoNova.setProfessor(professor);
        restricaoNova.setTipo(tipo);
        return restricaoNova;
    }

    public RestricaoGradeHorariaDTO salvar(RestricaoGradeHorariaDTO restricaoGradeHorariaDTO) {
        if (!ehValida(restricaoGradeHorariaDTO)) {
            throw new GenericException("Restrição Grade Horária inválida.");
        }

        if (existeRestricao(restricaoGradeHorariaDTO)) {
            throw new GenericException("Restrição Grade Horária já existe.");
        }

        return salvarRestricaoGradeHoraria(restricaoGradeHorariaDTO);
    }

    private boolean ehValida(RestricaoGradeHorariaDTO restricaoGradeHorariaDTO) {
        return nonNull(restricaoGradeHorariaDTO.getGradeHoraria())
                && nonNull(restricaoGradeHorariaDTO.getProfessor())
                && nonNull(restricaoGradeHorariaDTO.getDiaDaSemana());
    }

    private boolean existeRestricao(RestricaoGradeHorariaDTO restricaoGradeHorariaDTO) {
        Long idGradeHoraria = restricaoGradeHorariaDTO.getGradeHoraria().getId();
        Long idProfessor = restricaoGradeHorariaDTO.getProfessor().getId();
        EDiaSemana diaDaSemana = restricaoGradeHorariaDTO.getDiaDaSemana();

        return restricaoGradeHorariaRepository.existsByGradeHorariaIdAndProfessorIdAndDiaDaSemana(idGradeHoraria, idProfessor, diaDaSemana);
    }

    public List<RestricaoGradeHoraria> buscarTodos() {
        return restricaoGradeHorariaRepository.findAll();
    }

    public Page<RestricaoGradeHorariaProfessorDTO> buscarTodosPorFiltro(Long idGradeHoraria, RestricaoGradeHorariaFilter filter) {
        filter.setTipo(ETipoRestricao.DIAS_PROFESSOR_INDISPONIVEL);

        List<ProfessorDTO> professoresDTOS = disciplinaGradeHorariaService.buscarProfessoresPorGradeHorariaId(idGradeHoraria);

        List<RestricaoGradeHorariaProfessorDTO> restricaoGradeHorariaProfessorDTOS = new ArrayList<>();

        professoresDTOS.forEach(professor -> {
            Long idProfessor = professor.getId();
            List<RestricaoGradeHoraria> restricoes = restricaoGradeHorariaRepository.buscarFiltroPorProfessor(idGradeHoraria, filter.getTipo(), idProfessor);

            restricaoGradeHorariaProfessorDTOS.add(new RestricaoGradeHorariaProfessorDTO(professor, restricoes));
        });

        PagedListHolder<RestricaoGradeHorariaProfessorDTO> page = new PagedListHolder<>(restricaoGradeHorariaProfessorDTOS);
        page.setPageSize(filter.getSize());
        page.setPage(filter.getPage());

        return new PageImpl<>(page.getPageList(), filter.getPageRequest(), restricaoGradeHorariaProfessorDTOS.size());
    }

    public Page<RestricaoGradeHorariaDTO> buscarTodos(Long idGradeHoraria, RestricaoGradeHorariaFilter filter) {
        filter.setTipo(ETipoRestricao.DIAS_PROFESSOR_INDISPONIVEL);
        return restricaoGradeHorariaRepository.buscarFiltro(idGradeHoraria, filter.getTipo(), filter.getPageRequest()).map(RestricaoGradeHorariaDTO::new);
    }

    public RestricaoGradeHorariaDTO buscarPorId(Long id) {
        return restricaoGradeHorariaRepository.findById(id).map(RestricaoGradeHorariaDTO::new).orElse(null);
    }

    public void deletar(Long id) {
        restricaoGradeHorariaRepository.deleteById(id);
    }

    public void deletarTodosPorGradeHorariaId(Long idGradeHoraria) {
        restricaoGradeHorariaRepository.deleteAllByGradeHorariaId(idGradeHoraria);
    }

    public List<RestricaoGradeHoraria> buscarPorGradeHorariaId(Long idGradeHoraria) {
        return restricaoGradeHorariaRepository.buscarPorGradeHorariaId(idGradeHoraria);
    }

    public RestricaoGradeHorariaDTO alterar(Long id, RestricaoGradeHorariaDTO restricaoGradeHorariaDTO) {
        if (!id.equals(restricaoGradeHorariaDTO.getId()))
            throw new GenericException("Erro ao atualizar o registro.");

        return salvarRestricaoGradeHoraria(restricaoGradeHorariaDTO);
    }

    private RestricaoGradeHorariaDTO salvarRestricaoGradeHoraria(RestricaoGradeHorariaDTO restricaoGradeHorariaDTO) {
        RestricaoGradeHoraria restricaoGradeHorariaSalva = restricaoGradeHorariaRepository.save(RestricaoGradeHorariaDTO.toMapRestricaoGradeHoraria(restricaoGradeHorariaDTO));

        return new RestricaoGradeHorariaDTO(restricaoGradeHorariaSalva);
    }
}
