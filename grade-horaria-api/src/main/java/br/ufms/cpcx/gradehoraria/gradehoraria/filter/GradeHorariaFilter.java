package br.ufms.cpcx.gradehoraria.gradehoraria.filter;

import br.ufms.cpcx.gradehoraria.generico.filter.GenericFilter;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GradeHorariaFilter extends GenericFilter {

    private Long gradeHorariaId;

    private List<ETipoRestricao> restricoes;

    public GradeHorariaFilter() {
        this.restricoes = new ArrayList<>();
    }

    public GradeHorariaFilter(Map<String, String> parametros) {
        super(parametros);
    }

    public Long getGradeHorariaId() {
        return gradeHorariaId;
    }

    public void setGradeHorariaId(Long gradeHorariaId) {
        this.gradeHorariaId = gradeHorariaId;
    }

    public List<ETipoRestricao> getRestricoes() {
        return restricoes;
    }

    public void setRestricoes(List<ETipoRestricao> restricoes) {
        this.restricoes = restricoes;
    }

    public static GradeHorariaFilter of(Map<String, String> parametros) {
        return new GradeHorariaFilter(parametros);
    }

}
