package br.ufms.cpcx.gradehoraria.gradehoraria.utils;

import br.ufms.cpcx.gradehoraria.generico.utils.NumericUtils;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;
import br.ufms.cpcx.gradehoraria.grasp.grasp.utils.GradeHorariaUtils;
import br.ufms.cpcx.gradehoraria.grasp.grasp.utils.MoverAulasDiaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public class CandidatoUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CandidatoUtils.class);

    private CandidatoUtils() {
    }

    public static Map<Integer, Map<String, List<Candidato<Vertice<Integer>, EntidadeDTO>>>> getCandidatosTurmasPorDia(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosPorDia = getCandidatosPorDia(candidatos);

        HashMap<Integer, Map<String, List<Candidato<Vertice<Integer>, EntidadeDTO>>>> cargaHorariaTumasPorDia = new HashMap<>();

        candidatosPorDia.forEach((dia, candidatosDia) -> cargaHorariaTumasPorDia.put(dia, getCandidatosPorSemestre(candidatosDia)));

        return cargaHorariaTumasPorDia;
    }

    public static Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> getCandidatosPorDia(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        return candidatos.stream().filter(Candidato::estaAtivo).collect(Collectors.groupingBy(CandidatoUtils::getCorCandidato));
    }

    public static List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosNoDia(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, Integer dia) {
        return candidatos.stream().filter(candidato -> getCorCandidato(candidato).equals(dia)).collect(Collectors.toList());
    }

    public static List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosNosDias(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, List<Integer> dias) {
        return candidatos.stream().filter(candidato -> dias.contains(getCorCandidato(candidato))).collect(Collectors.toList());
    }

    public static List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosTurmaNoDia(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, String semestre, Integer dia) {
        return candidatos.stream().filter(candidato -> getCorCandidato(candidato).equals(dia) && estaNoSemestre(candidato, semestre)).collect(Collectors.toList());
    }

    public static Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> getCandidatosDoProfessorPorDia(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, String professor) {
        return getCandidatosDoProfessor(candidatos, professor).stream().collect(Collectors.groupingBy(CandidatoUtils::getCorCandidato));
    }

    public static List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosDoProfessor(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, String professor) {
        return candidatos.stream().filter(candidato -> getProfessores(candidato).contains(professor)).collect(Collectors.toList());
    }

    public static Map<String, List<EntidadeDTO>> getEntidadesCandidatosPorSemestre(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        List<EntidadeDTO> entidadesCanditatos = getEntidadesCanditatos(candidatos);
        return getEntidadesPorSemestre(entidadesCanditatos);
    }

    public static Map<String, List<EntidadeDTO>> getEntidadesPorSemestre(List<EntidadeDTO> entidades) {
        return entidades.stream().collect(Collectors.groupingBy(EntidadeDTO::getSemestre));
    }

    public static Map<String, List<Candidato<Vertice<Integer>, EntidadeDTO>>> getCandidatosPorSemestre(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        return candidatos.stream().collect(Collectors.groupingBy(CandidatoUtils::getSemestreCandidato));
    }

    public static String getSemestreCandidato(Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        return candidato.getEntidade().getSemestre();
    }

    public static List<EntidadeDTO> getEntidadesCanditatos(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        return candidatos.stream().map(Candidato::getEntidade).collect(Collectors.toList());
    }

    public static List<String> getProfessoresCandidatos(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        List<String> professores = new ArrayList<>();

        if (nonNull(candidatos)) {
            candidatos.forEach(candidato -> professores.addAll(getProfessores(candidato)));
        }

        return professores.stream().distinct().collect(Collectors.toList());
    }

    public static Integer getCorCandidato(Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        return candidato.getValor().getCor();
    }

    public static List<Integer> getDiasAulas(List<AulaDTO> aulas) {
        return aulas.stream().map(aula -> Integer.parseInt(aula.getDia())).sorted().distinct().collect(Collectors.toList());
    }

    public static List<Integer> getDiasExtrasCandidatos(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        return getDiasCandidatos(candidatos).stream().filter(dia -> dia > 5).collect(Collectors.toList());
    }

    public static List<Integer> getDiasCandidatos(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        return candidatos.stream().filter(Candidato::estaAtivo).map(CandidatoUtils::getCorCandidato).sorted().distinct().collect(Collectors.toList());
    }

    public static boolean ehCargaHorariaParcial(Candidato<Vertice<Integer>, EntidadeDTO> candidato, Integer quantidadeAulasPorTurno) {
        return candidato.getEntidade().getCargaHorariaTotal() < quantidadeAulasPorTurno;
    }

    public static boolean ehUnicaDisciplina(Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        return candidato.getEntidade().getAulas().stream().map(AulaDTO::getDisciplina).distinct().count() == 1 && getProfessores(candidato).size() == 1;
    }

    public static Map<String, List<AulaDTO>> getAulasCandidatosAgrupadasPorDisciplina(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        List<AulaDTO> aulasCandidatos = new ArrayList<>();

        candidatos.forEach(candidato -> aulasCandidatos.addAll(candidato.getEntidade().getAulas()));

        return aulasCandidatos.stream().collect(Collectors.groupingBy(AulaDTO::getDisciplina));
    }

    public static List<String> getSemestresCandidatos(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        return candidatos.stream().map(CandidatoUtils::getSemestreCandidato).distinct().collect(Collectors.toList());
    }

    public static List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosDaDisciplina(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, String disciplina) {
        return candidatos.stream().filter(candidato -> possuiDisciplina(candidato, disciplina)).collect(Collectors.toList());
    }

    public static List<EntidadeDTO> getEntidadessDasDisciplinas(List<EntidadeDTO> entidades, List<String> disciplinas) {
        return entidades.stream().filter(entidade -> possuiDisciplinas(entidade, disciplinas)).collect(Collectors.toList());
    }

    public static boolean ehDisciplinaMaisDeUmDiaNaTurma(Map<String, List<AulaDTO>> aulasCandidatosAgrupadosPorDisciplinas, Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        String turmasCandidato = getSemestreCandidato(candidato);

        for (AulaDTO aula : candidato.getEntidade().getAulas()) {
            List<AulaDTO> aulasDisciplina = aulasCandidatosAgrupadosPorDisciplinas.get(aula.getDisciplina());

            if (nonNull(aulasDisciplina) && CandidatoUtils.getDiasAulasNaTurma(aulasDisciplina, turmasCandidato).size() > 1) {
                return true;
            }
        }

        return false;
    }

    public static List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosNaTurma(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, String turma) {
        return candidatos.stream().filter(candidato -> candidato.getEntidade().getSemestre().equals(turma)).collect(Collectors.toList());
    }

    public static List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosNasTurmas(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, List<String> turmas) {
        return candidatos.stream().filter(candidato -> turmas.contains(CandidatoUtils.getSemestreCandidato(candidato))).collect(Collectors.toList());
    }

    private static List<Integer> getDiasAulasNaTurma(List<AulaDTO> aulas, String turma) {
        return getDiasAulas(aulas.stream().filter(aulaDTO -> aulaDTO.getTurma().equals(turma)).collect(Collectors.toList()));
    }

    private static List<String> getProfessores(Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        return candidato.getEntidade().getProfessores().stream().distinct().collect(Collectors.toList());
    }

    private static boolean estaNoSemestre(Candidato<Vertice<Integer>, EntidadeDTO> candidato, String semestre) {
        return candidato.getEntidade().getSemestre().equals(semestre);
    }

    private static boolean possuiDisciplina(Candidato<Vertice<Integer>, EntidadeDTO> candidato, String disciplina) {
        return !candidato.getEntidade().getAulasDisciplinas(Collections.singletonList(disciplina)).isEmpty();
    }

    private static boolean possuiDisciplinas(EntidadeDTO entidade, List<String> disciplinas) {
        return !entidade.getAulasDisciplinas(disciplinas).isEmpty();
    }

    public static List<EntidadeDTO> clonarEntidades(List<EntidadeDTO> entidades) {
        return entidades.stream().map(EntidadeDTO::clonar).collect(Collectors.toList());
    }

    public static void gerarNovosIdsEntidades(List<EntidadeDTO> entidades) {
        Map<String, String> relacaoIdsAntigosComNovos = new HashMap<>();
        for (int j = 0; j < entidades.size(); j++) {
            int idNovo = j + 1;
            relacaoIdsAntigosComNovos.put(String.valueOf(entidades.get(j).getId()), String.valueOf(idNovo));
            entidades.get(j).setId(idNovo);
        }

        for (EntidadeDTO entidade : entidades) {
            List<String> idsEntidadesIdenticasAntigos = entidade.getIdsEntidadesIdenticas();
            List<String> idsEntidadesIdenticasNovos = idsEntidadesIdenticasAntigos.stream()
                    .map(relacaoIdsAntigosComNovos::get).collect(Collectors.toList());
            entidade.setIdsEntidadesIdenticas(idsEntidadesIdenticasNovos);

            List<String> listaDeEntidadesComConflitosAntiga = entidade.getListaDeEntidadesComConflitos();
            List<String> listaDeEntidadesComConflitosNova = listaDeEntidadesComConflitosAntiga.stream()
                    .map(relacaoIdsAntigosComNovos::get).collect(Collectors.toList())
                    .stream().filter(Objects::nonNull).collect(Collectors.toList());
            entidade.setListaDeEntidadesComConflitos(listaDeEntidadesComConflitosNova);
        }
    }

    public static void juntarAulasProfessorNoMinimoDeDiasComEspaco(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, int quantidadeAulasPorTurno) {
        Map<Integer, Map<String, Integer>> cargaHorariaTurmasPorDia = CargaHorariaUtils.getCargaHorariaTurmasPorDia(candidatos);
        Map<Integer, Map<String, List<Candidato<Vertice<Integer>, EntidadeDTO>>>> candidatosTurmasPorDia = CandidatoUtils.getCandidatosTurmasPorDia(candidatos);

        List<Integer> dias = new ArrayList<>(cargaHorariaTurmasPorDia.keySet());

        for (Integer dia : dias) {
            Map<String, Integer> cargaHorariaTurmasDia = cargaHorariaTurmasPorDia.get(dia);
            List<String> turmas = new ArrayList<>(cargaHorariaTurmasDia.keySet());

            for (String turma : turmas) {
                Integer cargaHorariaTurmaDia = cargaHorariaTurmasDia.get(turma);
                List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosTurmasDia = candidatosTurmasPorDia.get(dia).get(turma);
                String professor = CandidatoUtils.getProfessoresCandidatos(candidatosTurmasDia).get(0);
                List<Integer> outrosDiasComProfessorNaTurma = getOutrosDiasComProfessorNaTurma(candidatosTurmasPorDia, cargaHorariaTurmasPorDia, dia, turma, professor, quantidadeAulasPorTurno);
                if (ehCandidatoParcial(cargaHorariaTurmaDia, quantidadeAulasPorTurno) && ehDeCandidatosApenasUmProfessor(candidatosTurmasPorDia, dia, turma)
                        && !outrosDiasComProfessorNaTurma.isEmpty()) {
                    LOGGER.info(dia + " " + candidatosTurmasDia.get(0).getEntidade().getProfessores().get(0) + " " + cargaHorariaTurmaDia + " ");
                    Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosDoProfessorPorDia = CandidatoUtils.getCandidatosDoProfessorPorDia(candidatos, professor);
                    Map<Integer, Integer> cargaHorariaProfessorPorDia = CargaHorariaUtils.getCargaHorariaProfessorPorDia(candidatosDoProfessorPorDia, professor);

                    for (int k = 0; k < outrosDiasComProfessorNaTurma.size(); k++) {
                        int outroDia = outrosDiasComProfessorNaTurma.size();
                        if (quantidadeAulasPorTurno - cargaHorariaTurmasDia.get(turma) >= cargaHorariaTurmasPorDia.get(outroDia).get(turma)
                                && cargaHorariaProfessorPorDia.get(dia) + cargaHorariaProfessorPorDia.get(outroDia) <= quantidadeAulasPorTurno) {
                            LOGGER.info("1");
                        } else {
                            int cargaHorariaDisponvivelOutroDia = quantidadeAulasPorTurno - cargaHorariaTurmasPorDia.get(outroDia).get(turma);
                            int cargaHorariaDisponvivelProfessorOutroDia = quantidadeAulasPorTurno - NumericUtils.getInteiroOuZero(cargaHorariaProfessorPorDia.get(outroDia));
                            if (cargaHorariaDisponvivelOutroDia >= cargaHorariaTurmasDia.get(turma)
                                    && cargaHorariaProfessorPorDia.get(dia) + cargaHorariaProfessorPorDia.get(outroDia) <= quantidadeAulasPorTurno) {
                                LOGGER.info("2");
                            } else if (cargaHorariaDisponvivelOutroDia > 0 && cargaHorariaDisponvivelProfessorOutroDia > 0) {
                                LOGGER.info("3");
                            }
                        }
                    }
                }
            }
        }
    }

    private static List<Integer> getOutrosDiasComProfessorNaTurma(Map<Integer, Map<String, List<Candidato<Vertice<Integer>, EntidadeDTO>>>> candidatosTurmasPorDia, Map<Integer, Map<String, Integer>> cargaHorariaTurmasPorDia,
                                                                  Integer diaSelecionado,
                                                                  String turma,
                                                                  String professor,
                                                                  int quantidadeAulasPorTurno) {
        List<Integer> dias = new ArrayList<>();
        Integer cargaHorariaTurmaDiaSelecionado = cargaHorariaTurmasPorDia.get(diaSelecionado).get(turma);
        candidatosTurmasPorDia.forEach((dia, candidatosTurmasDia) -> {
            List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos = candidatosTurmasDia.get(turma);
            List<String> professores = CandidatoUtils.getProfessoresCandidatos(candidatos);
            if (!dia.equals(diaSelecionado) && professores.contains(professor) && CargaHorariaUtils.getCargaHorariaCandidatos(candidatos) + cargaHorariaTurmaDiaSelecionado <= quantidadeAulasPorTurno) {
                dias.add(dia);
            }
        });

        return dias;
    }

    private static boolean ehCandidatoParcial(Integer cargaHorariaTurmaDia, Integer quantidadeAulasPorTurno) {
        return cargaHorariaTurmaDia > 0 && cargaHorariaTurmaDia < quantidadeAulasPorTurno;
    }

    private static boolean ehApenasUmProfessor(Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        return getProfessores(candidato).size() == 1;
    }

    private static boolean ehDeCandidatosApenasUmProfessor(Map<Integer, Map<String, List<Candidato<Vertice<Integer>, EntidadeDTO>>>> candidatosTurmasPorDia, Integer dia, String turma) {
        return CandidatoUtils.getProfessoresCandidatos(candidatosTurmasPorDia.get(dia).get(turma)).size() == 1;
    }

    public static void reduzirNumeroDeCores(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, Integer quantidadeAulasPorTurno) {
        GradeHorariaUtils.imprimirGradeHoraria(candidatos, quantidadeAulasPorTurno);

        Map<Integer, Map<String, Integer>> cargaHorariaTurmasPorDia = CargaHorariaUtils.getCargaHorariaTurmasPorDia(candidatos);
        List<Integer> dias = CandidatoUtils.getDiasCandidatos(candidatos);
        List<Integer> diasExtras = CandidatoUtils.getDiasExtrasCandidatos(candidatos);

        dias.removeAll(diasExtras);

        for (Integer dia : diasExtras) {
            List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDiaDeDisciplinaUnica = CandidatoUtils.getCandidatosNoDia(candidatos, dia)
                    .stream().filter(CandidatoUtils::ehUnicaDisciplina)
                    .filter(CandidatoUtils::ehApenasUmProfessor)
                    .collect(Collectors.toList());

            GradeHorariaUtils.imprimirGradeHoraria(candidatosNoDiaDeDisciplinaUnica, quantidadeAulasPorTurno);
            LOGGER.info(String.valueOf(candidatosNoDiaDeDisciplinaUnica.size()));

            for (Candidato<Vertice<Integer>, EntidadeDTO> candidato : candidatosNoDiaDeDisciplinaUnica) {
                String semestre = candidato.getEntidade().getSemestre();
                List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosTurma = CandidatoUtils.getCandidatosNaTurma(candidatos, semestre);
                String professor = candidato.getEntidade().getProfessores().get(0);
                Map<Integer, Integer> cargaHorariaProfessorPorDia = CargaHorariaUtils.getCargaHorariaProfessorPorDia(candidatos, professor);

                candidatosTurma.remove(candidato);

                GradeHorariaUtils.imprimirGradeHoraria(candidatosTurma, quantidadeAulasPorTurno);

//                GradeHorariaUtils.imprimirCargaHorariaPorDia(cargaHorariaProfessorPorDia);

                int cargaHorariaCandidato = candidato.getEntidade().getCargaHorariaTotal();
                List<Integer> diasPossiveisParaMover = new ArrayList<>(cargaHorariaProfessorPorDia.keySet());
                List<Integer> copiaDiasPossiveisParaMover = new ArrayList<>(diasPossiveisParaMover);
                System.out.println(professor);
                for (Integer diaProfessor : copiaDiasPossiveisParaMover) {
                    Integer cargaHorariaProfessorNoDia = cargaHorariaProfessorPorDia.get(diaProfessor);
                    if (diaProfessor.equals(dia) || quantidadeAulasPorTurno - cargaHorariaProfessorNoDia < cargaHorariaCandidato) {
                        diasPossiveisParaMover.remove(diaProfessor);
                        cargaHorariaProfessorPorDia.remove(diasPossiveisParaMover.indexOf(diaProfessor));
                    }
                }
                System.out.println(diasPossiveisParaMover);

                for (Integer diaPossivel : diasPossiveisParaMover) {
                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosTurmaNoDiaParaMover = getCandidatosTurmaNoDia(candidatos, semestre, diaPossivel);

                    if (!candidatosTurmaNoDiaParaMover.isEmpty()) {
                        Candidato<Vertice<Integer>, EntidadeDTO> candidatoPraMover = candidatosTurmaNoDiaParaMover.get(0);
                        String professorCandidatoPraMover = candidatoPraMover.getEntidade().getProfessores().get(0);
                        Map<Integer, Integer> cargaHorariaProfessorCandidatoPraMoverPorDia = CargaHorariaUtils.getCargaHorariaProfessorPorDia(candidatos, professorCandidatoPraMover);

                        System.out.println(professorCandidatoPraMover);
                        int cargaHorariaCandidatoPraMover = candidatoPraMover.getEntidade().getCargaHorariaTotal();
                        List<Integer> diasPossiveisParaMoverCandidatoPraMover = new ArrayList<>(cargaHorariaProfessorCandidatoPraMoverPorDia.keySet());
                        List<Integer> diasPossiveisParaMoverCandidatoPraMoverVazios = new ArrayList<>(dias);
                        List<Integer> copiaDiasPossiveisParaMoverCandidatoPraMover = new ArrayList<>(diasPossiveisParaMoverCandidatoPraMover);
                        System.out.println(diasPossiveisParaMoverCandidatoPraMover);
                        for (Integer diaProfessor : copiaDiasPossiveisParaMoverCandidatoPraMover) {
                            Integer cargaHorariaProfessorCandidatoPraMoverNoDia = cargaHorariaProfessorCandidatoPraMoverPorDia.get(diaProfessor);
                            if ((diaProfessor.equals(diaPossivel) || diaProfessor.equals(dia))
                                    || quantidadeAulasPorTurno - cargaHorariaProfessorCandidatoPraMoverNoDia < cargaHorariaCandidatoPraMover) {
                                diasPossiveisParaMoverCandidatoPraMover.remove(diaProfessor);
                                diasPossiveisParaMoverCandidatoPraMoverVazios.remove(diaProfessor);
                            }
                        }

                        for (Integer diaExistente : dias) {
                            Integer cargaHorariaNoDia = cargaHorariaTurmasPorDia.get(diaExistente).get(semestre);
                            if (nonNull(cargaHorariaNoDia) && cargaHorariaNoDia != 0) {
                                diasPossiveisParaMoverCandidatoPraMoverVazios.remove(diaExistente);
                            }
                        }

                        System.out.println(diasPossiveisParaMoverCandidatoPraMover);
                        System.out.println(diasPossiveisParaMoverCandidatoPraMoverVazios);

                        if (!diasPossiveisParaMoverCandidatoPraMoverVazios.isEmpty()) {
//                        MoverAulasDiaUtils.moverCandidatosParaDia(dia, candidatosTurmaNoDiaParaMover);
                            MoverAulasDiaUtils.setDiaCandidato(diasPossiveisParaMoverCandidatoPraMoverVazios.get(0), candidatoPraMover);
                            MoverAulasDiaUtils.setDiaCandidato(diaPossivel, candidato);
                            break;
                        }
                    }
                    break;
                }
                break;
            }
            break;
        }

        LOGGER.info(diasExtras.toString());
    }

    public static List<EntidadeDTO> getEntidadesComMaisDeUmDia(List<EntidadeDTO> entidades, Integer quantidadeAulasPorTurno) {
        Map<String, List<EntidadeDTO>> entidadesAgrupadaPorDisciplina = entidades.stream().collect(Collectors.groupingBy(EntidadeDTO::getNome));
        List<EntidadeDTO> entidadesDisciplinasMaisDeUmDia = new ArrayList<>();
        entidadesAgrupadaPorDisciplina.forEach((disciplina, entidadeDTOS) -> {
            if (entidadeDTOS.size() > quantidadeAulasPorTurno) {
                entidadesDisciplinasMaisDeUmDia.addAll(entidadeDTOS);
            }
        });
        return entidadesDisciplinasMaisDeUmDia;
    }
}



