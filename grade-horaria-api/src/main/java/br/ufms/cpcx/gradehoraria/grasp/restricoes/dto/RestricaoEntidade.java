package br.ufms.cpcx.gradehoraria.grasp.restricoes.dto;

import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;

import java.util.List;

public class RestricaoEntidade extends Restricao {
    private List<String> entidades;

    public RestricaoEntidade(ETipoRestricao tipo) {
        super(tipo);
    }

    public List<String> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<String> entidades) {
        this.entidades = entidades;
    }
}
