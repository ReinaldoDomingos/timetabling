package br.ufms.cpcx.gradehoraria.grasp.restricoes.business;

import br.ufms.cpcx.gradehoraria.generico.enumaration.EStatus;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.exception.NenhumaRestricaoFracaAtivaException;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.dto.Restricao;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.dto.RestricaoEntidade;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.dto.RestricaoProfessor;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ESituacaoRestricao;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.utils.RestricaoUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class RestricoesDTO<E> {
    private List<RestricaoProfessor> restricoesProfessores;
    private final List<RestricaoEntidade> restricoesFracas;
    private List<String> colunasRestricoesFortes;

    public RestricoesDTO() {
        this.restricoesProfessores = new ArrayList<>();
        this.restricoesFracas = RestricaoUtils.getListaRestricoes();
    }

    public void ativarRestricao(ETipoRestricao tipoRestricao, List<String> entidades) {
        this.restricoesFracas.stream()
                .filter(restricao -> restricao.getTipo().equals(tipoRestricao))
                .forEach(restricao -> ativarRestricao(restricao, entidades));
    }

    private void ativarRestricao(RestricaoEntidade restricao, List<String> entidades) {
        restricao.setStatus(EStatus.ATIVO);
        restricao.setEntidades(entidades);
    }

    public void ativarRestricao(List<RestricaoGradeHoraria> restricoesProfessores) {
        List<RestricaoGradeHoraria> restricoesProfessoresFiltradas = restricoesProfessores.stream()
                .filter(restricao -> ETipoRestricao.DIAS_PROFESSOR_INDISPONIVEL.equals(restricao.getTipo()))
                .collect(Collectors.toList());


        if (!restricoesProfessoresFiltradas.isEmpty()) {
            RestricaoProfessor restricaoProfessor = new RestricaoProfessor(ETipoRestricao.DIAS_PROFESSOR_INDISPONIVEL);
            restricaoProfessor.setRestricaoProfessores(restricoesProfessoresFiltradas);

            restricaoProfessor.setStatus(EStatus.ATIVO);
            this.restricoesProfessores.add(restricaoProfessor);
        }
    }

    public List<Candidato<E, EntidadeDTO>> getCandidatosFullRestricao(ETipoRestricao tipoRestricao, List<Candidato<E, EntidadeDTO>> candidatos) {
        RestricaoEntidade restricaoMesmoDia = getRestricaoFraca(tipoRestricao);

        if (restricaoMesmoDia.getStatus().equals(EStatus.ATIVO)) {
            return candidatos.stream()
                    .filter(candidato -> restricaoMesmoDia.getEntidades().contains(candidato.getEntidade().getNome()))
                    .sorted(Comparator.comparing(Candidato::getNumero)).collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    public RestricaoEntidade getRestricaoFraca(ETipoRestricao tipoRestricao) {
        return this.restricoesFracas.stream()
                .filter(restricao -> tipoRestricao.equals(restricao.getTipo()))
                .collect(Collectors.toList()).get(0);
    }

    public List<String> getDiasQuePodeMover(List<String> entidades) {
        List<String> entidadesMesmoDia = getRestricaoFraca(ETipoRestricao.MESMO_DIA).getEntidades();
        return entidades.stream().filter(entidade -> !entidadesMesmoDia.contains(entidade)).collect(Collectors.toList());
    }

    public void validarExistenciaRestricaoFraca() throws NenhumaRestricaoFracaAtivaException {
        if (getRestricoesAtivas().isEmpty()) {
            throw new NenhumaRestricaoFracaAtivaException();
        }
    }

    public boolean estaRestringindoPor(ETipoRestricao restricao) {
        return getRestricoesAtivas().contains(restricao) || getRestricoesProfessoresAtivas().contains(restricao);
    }

    public boolean estaAtivaEAplicadaRestricao(ETipoRestricao tipoRestricao) {
        RestricaoEntidade restricaoFraca = getRestricaoFraca(tipoRestricao);
        return estaAtiva(restricaoFraca) && estaAplicada(restricaoFraca);
    }

    private boolean estaAtiva(RestricaoEntidade restricaoFraca) {
        return EStatus.ATIVO.equals(restricaoFraca.getStatus());
    }

    private boolean estaAplicada(RestricaoEntidade restricaoFraca) {
        return ESituacaoRestricao.APLICADO.equals(restricaoFraca.getSituacao());
    }

    public List<String> getColunasRestricoesFortes() {
        return colunasRestricoesFortes;
    }

    public void setColunasRestricoesFortes(List<String> colunasRestricoesFortes) {
        this.colunasRestricoesFortes = colunasRestricoesFortes;
    }

    public List<ETipoRestricao> getRestricoesAtivas() {
        return this.restricoesFracas.stream()
                .filter(restricao -> restricao.getStatus().equals(EStatus.ATIVO))
                .map(Restricao::getTipo).collect(Collectors.toList());
    }

    public List<ETipoRestricao> getRestricoesProfessoresAtivas() {
        return this.restricoesProfessores.stream()
                .filter(restricao -> restricao.getStatus().equals(EStatus.ATIVO))
                .map(Restricao::getTipo).collect(Collectors.toList());
    }

    public void setSituacaoRestricao(ETipoRestricao tipoRestricao, ESituacaoRestricao situacaoRestricao) {
        this.getRestricaoFraca(tipoRestricao).setSituacao(situacaoRestricao);
    }

    public List<RestricaoProfessor> getRestricoesProfessores() {
        return restricoesProfessores;
    }
}
