package br.ufms.cpcx.gradehoraria.gradehoraria.service;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.filter.GenericFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.ProfessorDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfessorService {

    @Autowired
    private ProfessorRepository professorRepository;

    public ProfessorDTO salvar(ProfessorDTO professorDTO) {
        if (professorRepository.existsProfessorByNome(professorDTO.getNome()))
            throw new GenericException("Professor já existe.");

        return salvarProfessor(professorDTO);
    }

    public ProfessorDTO salvarOuGetPorNome(ProfessorDTO professorDTO) {
        if (professorRepository.existsProfessorByNome(professorDTO.getNome()))
            return buscarPorNome(professorDTO.getNome());

        return salvarProfessor(professorDTO);
    }

    public List<ProfessorDTO> buscarTodos() {
        return professorRepository.findAll().stream().map(ProfessorDTO::new)
                .sorted(Comparator.comparing(ProfessorDTO::getNome))
                .collect(Collectors.toList());
    }

    public Page<ProfessorDTO> buscarTodos(GenericFilter filter) {
        return professorRepository.findAll(filter.getPageRequest()).map(ProfessorDTO::new);
    }

    public ProfessorDTO buscarPorId(Long id) {
        return professorRepository.findById(id).map(ProfessorDTO::new).orElse(null);
    }

    public ProfessorDTO buscarPorNome(String nome) {
        return new ProfessorDTO(professorRepository.findByNome(nome));
    }

    public void deletar(Long id) {
        professorRepository.deleteById(id);
    }

    public void deletarTodosPorGradeHorariaId(Long id) {
        professorRepository.deleteAllByGradeHorariaTesteId(id);
    }

    public ProfessorDTO alterar(Long id, ProfessorDTO professorDTO) {
        if (!id.equals(professorDTO.getId()))
            throw new GenericException("Erro ao atualizar o registro.");

        return salvarProfessor(professorDTO);
    }

    private ProfessorDTO salvarProfessor(ProfessorDTO professorDTO) {
        Professor professorSalvo = professorRepository.save(ProfessorDTO.toMapProfessor(professorDTO));

        return new ProfessorDTO(professorSalvo);
    }
}
