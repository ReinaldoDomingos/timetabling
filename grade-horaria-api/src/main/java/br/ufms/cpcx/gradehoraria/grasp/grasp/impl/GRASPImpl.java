package br.ufms.cpcx.gradehoraria.grasp.grasp.impl;

import br.ufms.cpcx.gradehoraria.generico.utils.NumericUtils;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.GradeHorariaNovaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.EDiaSemana;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CandidatoUtils;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CargaHorariaUtils;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.GradeHoraria;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.HorarioAulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.enumaration.EPeriodo;
import br.ufms.cpcx.gradehoraria.grasp.grasp.GRASP;
import br.ufms.cpcx.gradehoraria.grasp.grasp.SolucaoGRASP;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.MelhorSolucaoDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.exception.NenhumaRestricaoFracaAtivaException;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Grafo;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;
import br.ufms.cpcx.gradehoraria.grasp.grasp.utils.AvaliadorSolucaoUtils;
import br.ufms.cpcx.gradehoraria.grasp.grasp.utils.GradeHorariaUtils;
import br.ufms.cpcx.gradehoraria.grasp.grasp.utils.MoverAulasDiaUtils;
import br.ufms.cpcx.gradehoraria.grasp.grasp.utils.RestricaoUtils;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.business.AvaliadorRestricoesBO;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.business.RestricoesDTO;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.dto.GradeHorariaRestricaoDTO;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ESituacaoRestricao;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static br.ufms.cpcx.gradehoraria.grasp.gradehoraria.utils.PeriodoUltils.ehPeriodoUnico;
import static br.ufms.cpcx.gradehoraria.grasp.grasp.utils.AvaliadorSolucaoUtils.possuiIgualOuMenosOuCores;
import static br.ufms.cpcx.gradehoraria.grasp.grasp.utils.AvaliadorSolucaoUtils.possuiMelhorAvalicaoQueAtual;
import static br.ufms.cpcx.gradehoraria.grasp.grasp.utils.AvaliadorSolucaoUtils.possuiMenosCores;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class GRASPImpl extends GRASP<Grafo<Integer, Integer>, Vertice<Integer>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(GRASPImpl.class);
    private static final String STR_CORES = " cores";

    private String colunaLocal;
    private List<EntidadeDTO> entidades;
    private final String colunaHorario;
    private Integer quantidadeAulasPorTurno;
    private final RestricoesDTO<Vertice<Integer>> restricoesDTO;
    private GradeHorariaNovaDTO gradeHorariaNovaDTO;

    private final AvaliadorRestricoesBO avaliadorRestricoesBO;

    public GRASPImpl(EPeriodo periodo, int tamanhoListaRestritaDeCandidatos, String colunaHorario) {
        super(periodo, tamanhoListaRestritaDeCandidatos);
        this.colunaHorario = colunaHorario;
        this.restricoesDTO = new RestricoesDTO<>();
        this.avaliadorRestricoesBO = new AvaliadorRestricoesBO();
    }

    public void setGradeHorariaNovaDTO(GradeHorariaNovaDTO gradeHorariaNovaDTO) {
        this.gradeHorariaNovaDTO = gradeHorariaNovaDTO;
    }

    public Integer getQuantidadeAulasPorTurno() {
        return quantidadeAulasPorTurno;
    }

    public void setQuantidadeAulasPorTurno(Integer quantidadeAulasPorTurno) {
        this.quantidadeAulasPorTurno = quantidadeAulasPorTurno;
    }

    public void ativarRestricao(ETipoRestricao tipoRestricao) {
        ativarRestricao(tipoRestricao, null);
    }

    public void ativarRestricao(ETipoRestricao tipoRestricao, List<String> entidades) {
        restricoesDTO.ativarRestricao(tipoRestricao, entidades);
    }

    public void ativarRestricao(List<RestricaoGradeHoraria> restricoesGradeHorria) {
        restricoesDTO.ativarRestricao(restricoesGradeHorria);
    }

    @Override
    public Grafo<Integer, Integer> buscaLocal(Grafo<Integer, Integer> solucao) {
        return solucao;
    }

    @Override
    public void avaliarCandidatos() {
        this.solucaoGRASP.getCandidatos().forEach(candidato -> setAvaliacaoCandidato(getCandidatosAgrupadosPorGrau(), candidato));
    }


    @Override
    public Grafo<Integer, Integer> construirSolucaoComGradeHorariaJaColorida() {
        Grafo<Integer, Integer> grafo = new Grafo<>();

        this.solucaoGRASP.setSolucaoAtual(grafo);

        this.solucaoGRASP.getCandidatos().forEach(candidato -> {
            Integer diaInicial = candidato.getEntidade().getDiaInicial();

            MoverAulasDiaUtils.setDiaCandidato(diaInicial, candidato);

            grafo.adicionaVertice(candidato.getValor());

            if (!this.solucaoGRASP.getCores().contains(diaInicial)) {
                this.solucaoGRASP.getCores().add(candidato.getValor().getCor());
            }
        });

        String mensgemLog = this.solucaoGRASP.getCores().size() + STR_CORES;
        LOGGER.info(mensgemLog);

        executarRestricoesPosColoracao();

        return grafo;
    }

    @Override
    public Grafo<Integer, Integer> adicionarCandidatoNaSolucao(Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        if (isNull(this.solucaoGRASP.getSolucaoAtual())) {
            this.solucaoGRASP.setSolucaoAtual(new Grafo<>());
        }

        Vertice<Integer> vertice = candidato.getValor();

        if (isNull(vertice.getCor())) {
            adicionaVertice(this.solucaoGRASP.getSolucaoAtual(), vertice);
        }

        List<Integer> coresVizinhos = new ArrayList<>();

        Grafo<Integer, Integer> finalSolucao = this.solucaoGRASP.getSolucaoAtual();
        candidato.getConflitos().forEach(conflito -> {
            Vertice<Integer> vizinho = this.solucaoGRASP.getCandidatos().get(conflito - 1).getValor();

            if (nonNull(vizinho.getCor())) {
                coresVizinhos.add(vizinho.getCor());
            }

            adicionaVertice(finalSolucao, vizinho);

            finalSolucao.adicionarAresta(vertice, vizinho, null);
        });

        colorirVertice(vertice, coresVizinhos);

        this.atualizarListaCandidatos(candidato);

        return this.solucaoGRASP.getSolucaoAtual();
    }

    @Override
    protected void executarRestricoesPreColoracao() {
        if (restricoesDTO.estaRestringindoPor(ETipoRestricao.MESMO_DIA)) {
            executarRestricaoMesmoDia();
            restricoesDTO.setSituacaoRestricao(ETipoRestricao.MESMO_DIA, ESituacaoRestricao.APLICADO);
        }
    }

    @Override
    protected void executarRestricoesPosColoracao() {//NOSONAR
        this.solucaoGRASP.getCandidatos().forEach(candidato -> MoverAulasDiaUtils.setDiaCandidato(candidato.getValor().getCor(), candidato));
        List<String> profesores = CandidatoUtils.getProfessoresCandidatos(this.solucaoGRASP.getCandidatos());
        List<String> semestres = this.solucaoGRASP.getSemestresCandidatos();

        int qtdCoresAnterior = this.solucaoGRASP.getCores().size();
        double multiplicador = 1.5;
        int tentativas = (int) Math.max(semestres.size() * multiplicador, 5);

        while (tentativas-- > 0) {
            for (String professor : profesores) {
                Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosDoProfessorPorDia = CandidatoUtils.getCandidatosDoProfessorPorDia(this.solucaoGRASP.getCandidatos(), professor);

                Map<Integer, Integer> cargaHorariaProfessorPorDia = CargaHorariaUtils.getCargaHorariaProfessorPorDia(candidatosDoProfessorPorDia, professor);
                List<Integer> dias = candidatosDoProfessorPorDia.keySet().stream().distinct().collect(Collectors.toList());

                Map<Integer, Map<String, Integer>> cargaHorariaTurmasPorDia = CargaHorariaUtils.getCargaHorariaTurmasPorDia(this.solucaoGRASP.getCandidatos());

                if (dias.size() > 1) {
                    for (int j = 0; j < dias.size() - 1; j++) {
                        List<Integer> copiaDias = new ArrayList<>(dias);

                        Integer dia1 = copiaDias.get(NumericUtils.sort(copiaDias.size()));
                        copiaDias.remove(dia1);
                        Integer dia2 = copiaDias.get(NumericUtils.sort(copiaDias.size()));

                        Candidato<Vertice<Integer>, EntidadeDTO> candidatoDia2 = candidatosDoProfessorPorDia.get(dia2).get(0);
                        Candidato<Vertice<Integer>, EntidadeDTO> candidatoDia1 = candidatosDoProfessorPorDia.get(dia1).get(0);

//                        LOGGER.info(professor);//NOSONAR

                        String semestreCandidatoDia1 = candidatoDia1.getEntidade().getSemestre();
                        String semestreCandidatoDia2 = candidatoDia2.getEntidade().getSemestre();

//                        LOGGER.info(dia1 + " " + semestreCandidatoDia1);//NOSONAR
//                        LOGGER.info(dia2 + " " + semestreCandidatoDia2);//NOSONAR

                        Integer cargaHorariaProfessorDia1 = cargaHorariaProfessorPorDia.get(dia1);
                        Integer cargaHorariaProfessorDia2 = cargaHorariaProfessorPorDia.get(dia2);

                        int cargaHorariaCandidatoDia1 = candidatoDia1.getEntidade().getCargaHorariaTotal();
                        int cargaHorariaCandidatoDia2 = candidatoDia2.getEntidade().getCargaHorariaTotal();

                        Integer cargaHorariaDia1Semestre2 = nonNull(cargaHorariaTurmasPorDia.get(dia1).get(semestreCandidatoDia2)) ?
                                cargaHorariaTurmasPorDia.get(dia1).get(semestreCandidatoDia2) : 0;
                        Integer cargaHorariaDia2Semestre1 = nonNull(cargaHorariaTurmasPorDia.get(dia2).get(semestreCandidatoDia1)) ?
                                cargaHorariaTurmasPorDia.get(dia2).get(semestreCandidatoDia1) : 0;

                        int cargaHorariaRestanteDia1NaTurmaCandidatoDia2 = this.quantidadeAulasPorTurno - cargaHorariaDia1Semestre2;
                        int cargaHorariaRestanteDia2NaTurmaCandidatoDia1 = this.quantidadeAulasPorTurno - cargaHorariaDia2Semestre1;

                        if (this.quantidadeAulasPorTurno.equals(cargaHorariaRestanteDia1NaTurmaCandidatoDia2)
                                && cargaHorariaRestanteDia1NaTurmaCandidatoDia2 >= cargaHorariaCandidatoDia2
                                && cargaHorariaProfessorDia1 + cargaHorariaCandidatoDia2 <= this.quantidadeAulasPorTurno) {

                            MoverAulasDiaUtils.setDiaCandidato(dia1, candidatoDia2);
                        } else if (this.quantidadeAulasPorTurno.equals(cargaHorariaRestanteDia2NaTurmaCandidatoDia1)
                                && cargaHorariaRestanteDia2NaTurmaCandidatoDia1 >= cargaHorariaCandidatoDia1
                                && cargaHorariaProfessorDia2 + cargaHorariaCandidatoDia1 <= this.quantidadeAulasPorTurno) {

                            MoverAulasDiaUtils.setDiaCandidato(dia2, candidatoDia1);
                        } else if (cargaHorariaRestanteDia1NaTurmaCandidatoDia2 >= cargaHorariaProfessorDia2) {
//                        List<AulaDTO> aulasMovidas = candidatoDia2.getEntidade().getAulas().stream().filter(aula -> aula.getProfessor().contains(professor)).collect(Collectors.toList());
//                        int cargaHorariaAulasParaMoverDoCandidato2 = aulasMovidas.stream().mapToInt(AulaDTO::getCargaHoraria).sum();
//
//                        if (cargaHorariaAulasParaMoverDoCandidato2 + cargaHorariaCandidatoDia1 <= this.quantidadeAulasPorTurno
//                                && cargaHorariaProfessorDia1 + cargaHorariaCandidatoDia1 <= this.quantidadeAulasPorTurno) {
//
//                            candidatoDia1.getEntidade().getAulas().addAll(aulasMovidas);
//
//                            if (candidatoDia1.getEntidade().getCargaHorariaTotal() > this.quantidadeAulasPorTurno) {
//                                LOGGER.info("Troca falhou");
//                                break;
//                            }
//
//                            for (AulaDTO aulasMovida : aulasMovidas) {
//                                candidatoDia2.getEntidade().getAulas().remove(aulasMovida);
//                            }
//
//                            if (candidatoDia2.getEntidade().getAulas().isEmpty()) {
//                                this.solucaoGRASP.getSolucaoAtual().getListaDeVertices().remove(candidatoDia2.getValor());
//                            }
//
//                            atualizarCoresExistentes();
//                            setGradeHorariaAtual();
//                            break;
//                        }
                        }
                    }
                }
            }

            if (this.solucaoGRASP.getCores().size() < qtdCoresAnterior) {
                qtdCoresAnterior = this.solucaoGRASP.getCores().size();
                tentativas = (int) Math.max(semestres.size() * multiplicador, 5);
                String mensgemLog = this.solucaoGRASP.getCores().size() + " cores melhorado";
                LOGGER.info(mensgemLog);
            }
        }

        atualizarCoresExistentes();

        String mensgemLog = this.solucaoGRASP.getCores().size() + " cores melhorado";
        String mensgemLog2 = this.solucaoGRASP.getCandidatos().stream().map(candidato -> candidato.getValor().getCor()).distinct().count() + STR_CORES;
        LOGGER.info(mensgemLog);
        LOGGER.info(mensgemLog2);

    }

    public void executarRestricaoMesmoDia() {
        getCandidatosRestricaoMesmoDia().forEach(this::adicionarCandidatoNaSolucao);
    }

    private List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosRestricaoMesmoDia() {
        return restricoesDTO.getCandidatosFullRestricao(ETipoRestricao.MESMO_DIA, this.solucaoGRASP.getCandidatos());
    }

    @Override
    public Candidato<Vertice<Integer>, EntidadeDTO> selecionarCandidato() {
        double soma = this.solucaoGRASP.getCandidatosRestrito().stream().mapToDouble(Candidato::getAvaliacao).sum();

        double valorSorteado = Math.random() * soma;

        double somaAtual = 0.0;
        for (int i = 0; i < this.solucaoGRASP.getCandidatosRestrito().size(); i++) {
            Candidato<Vertice<Integer>, EntidadeDTO> verticeCandidato = this.solucaoGRASP.getCandidatosRestrito().get(i);

            if (i + 1 < this.solucaoGRASP.getCandidatosRestrito().size() && valorSorteado < somaAtual + this.solucaoGRASP.getCandidatosRestrito().get(i + 1).getAvaliacao()) {
                return verticeCandidato;
            }

            somaAtual += verticeCandidato.getAvaliacao();

            if (soma == somaAtual) {
                return verticeCandidato;
            }
        }

        return this.solucaoGRASP.getCandidatosRestrito().get(this.solucaoGRASP.getCandidatosRestrito().size() - 1);
    }

    @Override
    public void construirLRC() {
        this.solucaoGRASP.setCandidatosRestrito(getCandidatosOrdenadosPorAvaliacao(this.tamanhoListaRestritaDeCandidatos));
    }

    @Override
    public void atualizarMelhorSolucao(Grafo<Integer, Integer> solucao) {
//        List<Integer> avaliacoesDias = this.solucaoGRASP.getAvaliacoesDias(this.colunaLocal, 2);
//        List<Integer> avaliacoesTurmas = this.solucaoGRASP.getAvaliacoesTurmas();

//        if (isNull(this.melhorSolucao) || possuiMenosCores(solucao, this.melhorSolucao) || ehMelhorSolucao(solucao, avaliacoesDias, avaliacoesTurmas)) {
        if (isNull(this.melhorSolucao) || possuiMenosCores(solucao, this.melhorSolucao)) {
//            this.melhorSolucao = new MelhorSolucaoDTO<>(solucao, solucao.getCores(), avaliacoesDias, avaliacoesTurmas, this.solucaoGRASP.getCandidatos());
            this.melhorSolucao = new MelhorSolucaoDTO<>(solucao, solucao.getCores(), new ArrayList<>(), new ArrayList<>(), this.solucaoGRASP.getCandidatos());
            System.out.println("Melhor solucão atualizada");
            System.out.println(melhorSolucao);
        }

        this.solucaoGRASP = new SolucaoGRASP<>();

        this.lerDados();

        this.solucaoGRASP.inicializar();
    }

    private boolean ehMelhorSolucao(Grafo<Integer, Integer> solucao, List<Integer> avaliacoesDias, List<Integer> avaliacoesTurmas) {
        return possuiIgualOuMenosOuCores(solucao, this.melhorSolucao) && possuiMelhorAvalicaoQueAtual(this.melhorSolucao, avaliacoesDias, avaliacoesTurmas);
    }

    @Override
    public void lerDados() {
        List<EntidadeDTO> copiasEntidades;

        if (isNull(this.gradeHorariaNovaDTO)) {
            copiasEntidades = RestricaoUtils.juntarDisciplinaEmMenosCandidatos(this.entidades, this.quantidadeAulasPorTurno);
        } else {
            copiasEntidades = RestricaoUtils.juntarDisciplinaEmMenosCandidatos(this.gradeHorariaNovaDTO);
        }
        this.solucaoGRASP.setCandidatos(copiasEntidades.stream().map(CandidatoImpl::new).collect(Collectors.toList()));
    }

    @Override
    public void repararSolucao(Grafo<Integer, Integer> solucao) {
        if (ehPeriodoUnico(this.periodo)) {
//            diminuirNumeroDeCores(); // erro aumentando quantidades aulas professor dia mais do que que máximo
//            setGradeHorariaAtual();
            repararSolucaoPeriodoUnico();
//            repararRestricaoDiaProfessorDisponivel();
            GradeHorariaUtils.imprimirGradeHoraria(this.solucaoGRASP.getCandidatos(), this.quantidadeAulasPorTurno);
            setGradeHorariaAtual();
            GradeHorariaUtils.imprimirGradeHoraria(this.solucaoGRASP.getCandidatos(), this.quantidadeAulasPorTurno);
        } else {
            repararSolucaoPeriodoIntegral();
        }
    }

    private void repararSolucaoPeriodoUnico() {
        int tentativasRestantes = 5;
//        if (restricaoBO.estaRestringindoPor(ETipoRestricao.LOCAL_DISPONIVEL)) {
//            while (tentativasRestantes-- > 0 && AvaliadorSolucaoUtils.getQtdAvaliacoesNegativas(this.solucaoGRASP.getAvaliacoesDias(this.colunaLocal, 2)) > 0) {
//                List<Integer> avaliacoesDias = solucaoGRASP.getAvaliacoesDias(this.colunaLocal, 2);
//
//                for (int posDia1 = 0; posDia1 < avaliacoesDias.size(); posDia1++) {
//                    if (avaliacoesDias.get(posDia1) < 0) {
//                        List<EntidadeDTO> entidadesDia1 = this.solucaoGRASP.getCandidatosDia(posDia1 + 1)
//                                .stream().map(Candidato::getEntidade).distinct()
//                                .filter(entidade -> entidade.get(this.colunaLocal).equalsIgnoreCase("sim"))
//                                .collect(Collectors.toList());
//
//                        if (entidadesDia1.isEmpty()) continue;
//
//                        String semestreDia1 = entidadesDia1.get(0).get(this.colunaHorario);
//                        String responsavelDia1 = entidadesDia1.get(0).get("Professor");
//
//
//                        for (int posDia2 = 0; posDia2 < avaliacoesDias.size(); posDia2++) {
//                            if (posDia2 != posDia1 && posDia2 < 6 && avaliacoesDias.get(posDia2) > 0) {
//                                List<String> semestresComEspacoNoDia2 = this.solucaoGRASP.getGradeHorariaAtual().getSemetresComEspacoNoDia(posDia2 + 1)
//                                        .stream().filter(semestre -> semestre.equalsIgnoreCase(semestreDia1)).collect(Collectors.toList());
//                                List<String> responsaveisNoDia2 = this.solucaoGRASP.getGradeHorariaAtual().getResponsavelNoDia(posDia2 + 1);
//                                if (!semestresComEspacoNoDia2.contains(semestreDia1) || responsaveisNoDia2.contains(responsavelDia1))
//                                    continue;
//
//                                this.solucaoGRASP.getGradeHorariaAtual().deslocarAula(entidadesDia1.get(0).getId(), posDia2 + 1);
//                                break;
//                            }
//                            setGradeHorariaAtual();
//                            avaliacoesDias = solucaoGRASP.getAvaliacoesDias(this.colunaLocal, 2);
//                        }
//                        break;
//                    }
//                }
//            }
//        }

//        atualizarCoresExistentes();
//        setGradeHorariaAtual();

//        if (restricoesDTO.estaAtivaEAplicadaRestricao(ETipoRestricao.MESMO_DIA)) {
//            corrigirCandidatosRestricaoMesmoDia();
//        }

//        atualizarCoresExistentes();
//        setGradeHorariaAtual();

        if (restricoesDTO.estaRestringindoPor(ETipoRestricao.DIAS_PROFESSOR_INDISPONIVEL)) {
            RestricaoUtils.juntarCandidatosMesmaDisciplina(this.solucaoGRASP.getCandidatosAtivos(), this.quantidadeAulasPorTurno);
            CandidatoUtils.juntarAulasProfessorNoMinimoDeDiasComEspaco(this.solucaoGRASP.getCandidatosAtivos(), this.quantidadeAulasPorTurno);
            CandidatoUtils.reduzirNumeroDeCores(this.solucaoGRASP.getCandidatosAtivos(), this.quantidadeAulasPorTurno);
            repararRestricaoDiaProfessorDisponivel();
        }

        tentativasRestantes = 5;
        if (restricoesDTO.estaRestringindoPor(ETipoRestricao.DIAS_NAO_CONSECUTIVOS)) {
            List<String> nomesEntidadesRestritas = restricoesDTO.getRestricaoFraca(ETipoRestricao.DIAS_NAO_CONSECUTIVOS).getEntidades();
//            this.solucaoGRASP.getGradeHorariaAtual().trocarDias();
//            while (tentativasRestantes-- > 0 && this.solucaoGRASP.getAvaliacaoTurmas() < 0) {
            while (tentativasRestantes-- > 0) {
                RestricaoUtils.executarRestricaoDiasAlternados(this.solucaoGRASP.getCandidatosAtivos(), nomesEntidadesRestritas);
//                this.solucaoGRASP.getGradeHorariaAtual().trocarDias();
//
////                System.out.println();
//
////                System.out.println(this.solucaoGRASP.getGradeHorariaAtual().getAulaEmSequenciaTurmas());
//                this.solucaoGRASP.getGradeHorariaAtual().reinicializarListas();
////                List<Integer> avaliacoesDias = restricaoBO.estaRestringindoPor(ERestricao.LOCAL_DISPONIVEL) ? this.solucaoGRASP.getAvaliacoesDias(this.colunaLocal, 2) : new ArrayList<>();
////                this.solucaoGRASP.getGradeHorariaAtual().imprimirGradeHoraria(this.solucaoGRASP.getAvaliacoesTurmas(), avaliacoesDias);
////                System.out.println();
            }
//            restricaoBO.getRestricaoFraca(ETipoRestricao.DIAS_NAO_CONSECUTIVOS).setSituacao(ESituacaoRestricao.APLICADO);
        }
//        atualizarCoresExistentes();
//        setGradeHorariaAtual();

        if (this.solucaoGRASP.getCores().size() > 5) {
            LOGGER.info("Ainda está com " + this.solucaoGRASP.getCores().size() + STR_CORES);
//            diminuirNumeroDeCores();
        }

        if (restricoesDTO.estaRestringindoPor(ETipoRestricao.DIAS_PROFESSOR_INDISPONIVEL)) {
            RestricaoUtils.juntarCandidatosMesmaDisciplina(this.solucaoGRASP.getCandidatosAtivos(), this.quantidadeAulasPorTurno);
            CandidatoUtils.juntarAulasProfessorNoMinimoDeDiasComEspaco(this.solucaoGRASP.getCandidatosAtivos(), this.quantidadeAulasPorTurno);
            CandidatoUtils.reduzirNumeroDeCores(this.solucaoGRASP.getCandidatosAtivos(), this.quantidadeAulasPorTurno);
            repararRestricaoDiaProfessorDisponivel();
        }

        tentativasRestantes = 5;
        if (restricoesDTO.estaRestringindoPor(ETipoRestricao.DIAS_NAO_CONSECUTIVOS)) {
            List<String> nomesEntidadesRestritas = restricoesDTO.getRestricaoFraca(ETipoRestricao.DIAS_NAO_CONSECUTIVOS).getEntidades();
//            this.solucaoGRASP.getGradeHorariaAtual().trocarDias();
//            while (tentativasRestantes-- > 0 && this.solucaoGRASP.getAvaliacaoTurmas() < 0) {
            while (tentativasRestantes-- > 0) {
                RestricaoUtils.executarRestricaoDiasAlternados(this.solucaoGRASP.getCandidatosAtivos(), nomesEntidadesRestritas);
                RestricaoUtils.realizarTrocaEntreCandidatosDiasAlternados(this.solucaoGRASP.getCandidatosAtivos(), nomesEntidadesRestritas, quantidadeAulasPorTurno);
            }
        }
    }

    private void repararRestricaoDiaProfessorDisponivel() {
        List<RestricaoGradeHoraria> restricoesProfessores = restricoesDTO.getRestricoesProfessores().get(0).getRestricaoProfessores();

        Map<Professor, List<RestricaoGradeHoraria>> restricoesAgrupadasPorProfessor = restricoesProfessores.stream().collect(Collectors.groupingBy(RestricaoGradeHoraria::getProfessor));

        List<Professor> professores = new ArrayList<>(restricoesAgrupadasPorProfessor.keySet());

        List<String> diasDaSemana = Arrays.stream(EDiaSemana.values()).map(EDiaSemana::getValor).collect(Collectors.toList());

        // Avaliar dias professores//NOSONAR
        GradeHorariaRestricaoDTO avaliacaoDias = getAvaliacoesDias(restricoesAgrupadasPorProfessor);
        // Avaliar dias professores//NOSONAR

        int tentativas = 10;
        while (tentativas-- > 0) {
            for (int i = 0; i < professores.size(); i++) {
//        for (int i = 0; i < 1; i++) {
                Professor professor = professores.get(i);
                List<RestricaoGradeHoraria> restricoesProfessor = restricoesAgrupadasPorProfessor.get(professor);
                LOGGER.info(professor.getNome());
                List<String> dias = restricoesProfessor.stream().map(restricaoGradeHoraria -> restricaoGradeHoraria.getDiaDaSemana().getValor()).collect(Collectors.toList());
                LOGGER.info(dias.toString());


                for (String dia : dias) {
                    int numeroDia = diasDaSemana.indexOf(dia) + 1;
                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia = CandidatoUtils.getCandidatosNoDia(this.solucaoGRASP.getCandidatosAtivos(), numeroDia);
                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDoProfessorNoDia = CandidatoUtils.getCandidatosDoProfessor(candidatosNoDia, professor.getNome());

                    if (!candidatosDoProfessorNoDia.isEmpty()) {
                        int diaParaTrocar = numeroDia + 2 < 5 ? numeroDia + 2 : numeroDia - 2;
                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDiaParaMover = CandidatoUtils.getCandidatosNoDia(this.solucaoGRASP.getCandidatosAtivos(), diaParaTrocar);

                        int somaAvaliacoesAntes = avaliacaoDias.getAvaliacaoDias(Arrays.asList(numeroDia, diaParaTrocar));

                        setGradeHorariaAtual();

                        System.out.println("Movendo candidatos numeroDia " + numeroDia + " pra dia " + diaParaTrocar);
                        MoverAulasDiaUtils.moverCandidatosParaDia(diaParaTrocar, candidatosNoDia);
                        MoverAulasDiaUtils.moverCandidatosParaDia(numeroDia, candidatosNoDiaParaMover);

                        avaliacaoDias = getAvaliacoesDias(restricoesAgrupadasPorProfessor);
                        int somaAvaliacoesDepois = avaliacaoDias.getAvaliacaoDias(Arrays.asList(numeroDia, diaParaTrocar));
                        System.out.println(somaAvaliacoesDepois);

                        if (somaAvaliacoesDepois < somaAvaliacoesAntes) {
                            System.out.println("Revertendo candidatos numeroDia " + diaParaTrocar + " pra dia " + numeroDia);
                            MoverAulasDiaUtils.moverCandidatosParaDia(numeroDia, candidatosNoDia);
                            MoverAulasDiaUtils.moverCandidatosParaDia(diaParaTrocar, candidatosNoDiaParaMover);
                        }
                        break;
                    }
                }
            }
        }
    }

    private GradeHorariaRestricaoDTO getAvaliacoesDias(Map<Professor, List<RestricaoGradeHoraria>> restricoesAgrupadasPorProfessor) {
        return avaliadorRestricoesBO.avaliarSolucao(this, restricoesAgrupadasPorProfessor);
    }


    private void atualizarCoresExistentes() {
        this.solucaoGRASP.setCores(this.solucaoGRASP.getSolucaoAtual()
                .getListaDeVertices()
                .stream().map(Vertice::getCor).distinct().sorted().collect(Collectors.toList()));
    }

    private void corrigirCandidatosRestricaoMesmoDia() {
        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosRestricao = getCandidatosRestricaoMesmoDia();
        List<Integer> coresCandidatos = this.solucaoGRASP.getCoresCandidatos(candidatosRestricao);

        int tentativasRestantes = 5;
        while (tentativasRestantes-- > 0 && coresCandidatos.size() > this.solucaoGRASP.getQtdCoresNecessaria(candidatosRestricao)) {
            Integer cor = coresCandidatos.get(coresCandidatos.size() - 1);
            List<Integer> candidatosDia = this.solucaoGRASP.getGradeHorariaAtual().getCandidatosDia(cor);
            List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosRestricaoNoDia = candidatosRestricao.stream()
                    .filter(candidato -> candidatosDia.contains(candidato.getEntidade().getId())).collect(Collectors.toList());

            String semestre = candidatosRestricaoNoDia.get(0).getEntidade().get(this.colunaHorario);
            List<Integer> diasComEspacoVazioNoSemetre = this.solucaoGRASP.getGradeHorariaAtual().getDiasComEspacoNoSemetre(semestre);

            for (Integer dia : diasComEspacoVazioNoSemetre) {
                if (coresCandidatos.contains(dia)) {
                    this.solucaoGRASP.getGradeHorariaAtual().deslocarAula(candidatosRestricaoNoDia.get(0).getNumero(), dia);
                    break;
                }
            }

            candidatosRestricao = getCandidatosRestricaoMesmoDia();
            coresCandidatos = this.solucaoGRASP.getCoresCandidatos(candidatosRestricao);
        }
    }

    private void diminuirNumeroDeCores() {
        List<String> semestres = this.solucaoGRASP.getSemestresCandidatos();

        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos = this.solucaoGRASP.getCandidatos();

        double multiplicador = 1.5;

        for (String turma : semestres) {
            Map<Integer, Map<String, Integer>> cargaHorariaTurmasPorDia = CargaHorariaUtils.getCargaHorariaTurmasPorDia(candidatos);
            List<Integer> cores = this.solucaoGRASP.getCandidatos().stream()
                    .filter(Candidato::possuiAulas)
                    .map(candidato -> candidato.getValor().getCor())
                    .distinct().sorted(Comparator.comparing(Integer::intValue))
                    .collect(Collectors.toList());

            List<Integer> diasVaziosTurma = new ArrayList<>();

            for (Integer dia : cores) {
                Map<String, Integer> cargaHorariaDia = cargaHorariaTurmasPorDia.get(dia);
                if (isNull(cargaHorariaDia.get(turma))) {
                    diasVaziosTurma.add(dia);
                }
            }

            if (!diasVaziosTurma.isEmpty()) {
                diasVaziosTurma.sort(Comparator.comparing(Integer::intValue));

                List<Integer> diasNaoVaziosNaTurma = cores.stream().filter(cor -> !diasVaziosTurma.contains(cor))
                        .sorted(Comparator.comparing(Integer::intValue).reversed()).collect(Collectors.toList());


                if (diasNaoVaziosNaTurma.size() > 2) {
                    trocarCandidatosTurmaOcupados(turma, cargaHorariaTurmasPorDia, diasNaoVaziosNaTurma);
                }

                Integer dia = diasNaoVaziosNaTurma.get(0);
                List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia = CandidatoUtils.getCandidatosNoDia(candidatos, dia);
                List<String> professoresCandidatosDia = CandidatoUtils.getProfessoresCandidatos(candidatosDia);

                List<Integer> diasVaziosTurmaQuePodeMover = new ArrayList<>(diasVaziosTurma);

                for (Integer diaParaMover : diasVaziosTurma) {
                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDiaParaMover = CandidatoUtils.getCandidatosNoDia(candidatos, diaParaMover);
                    List<String> professoresEmComumCandidatosDiaParaMover = CandidatoUtils.getProfessoresCandidatos(candidatosDia).stream()
                            .filter(professoresCandidatosDia::contains).collect(Collectors.toList());

                    for (String professor : professoresEmComumCandidatosDiaParaMover) {
                        int cargaHorariaProfessorNoDia = CargaHorariaUtils.getCargaHorariaProfessorCandidatos(candidatosDia, professor);
                        int cargaHorariaProfessorNoDiaParaMover = CargaHorariaUtils.getCargaHorariaProfessorCandidatos(candidatosDiaParaMover, professor);

                        if (cargaHorariaProfessorNoDia + cargaHorariaProfessorNoDiaParaMover > this.quantidadeAulasPorTurno) {
                            diasVaziosTurmaQuePodeMover.remove(diaParaMover);
                            setGradeHorariaAtual();
                        }
                    }
                }

                if (!diasVaziosTurmaQuePodeMover.isEmpty()) {
                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosTurmaNoDiaParaMover = CandidatoUtils.getCandidatosTurmaNoDia(candidatos, turma, dia);

                    MoverAulasDiaUtils.moverCandidatosParaDia(diasVaziosTurmaQuePodeMover.get(0), candidatosTurmaNoDiaParaMover);

                    atualizarCoresExistentes();
                    setGradeHorariaAtual();
                }

            }
        }
    }

    private void trocarCandidatosTurmaOcupados(String turma, Map<Integer, Map<String, Integer>> cargaHorariaTurmasPorDia, List<Integer> diasNaoVaziosNaTurma) {
        List<Integer> copiaDiasNaoVaziosNaTurma = new ArrayList<>(diasNaoVaziosNaTurma);
        Integer dia1 = copiaDiasNaoVaziosNaTurma.get(copiaDiasNaoVaziosNaTurma.size() - 1);
        copiaDiasNaoVaziosNaTurma.remove(dia1);
        int dia2 = copiaDiasNaoVaziosNaTurma.get((int) (Math.random() * copiaDiasNaoVaziosNaTurma.size()));//NOSONAR

        Integer cargaHorariaDia1 = cargaHorariaTurmasPorDia.get(dia1).get(turma);
        Integer cargaHorariaDia2 = cargaHorariaTurmasPorDia.get(dia2).get(turma);

        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia1 = CandidatoUtils.getCandidatosTurmaNoDia(this.solucaoGRASP.getCandidatos(), turma, dia1);
        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia2 = CandidatoUtils.getCandidatosTurmaNoDia(this.solucaoGRASP.getCandidatos(), turma, dia2);

        List<String> professoresDia1 = CandidatoUtils.getProfessoresCandidatos(candidatosDia1);
        List<String> professoresDia2 = CandidatoUtils.getProfessoresCandidatos(candidatosDia1);

        boolean faltouHorarioPraAlgumProfessorDia1 = isFaltouHorarioPraAlgumProfessorParaTrocarCanditatosDeDia(candidatosDia2, candidatosDia1, professoresDia2);

        boolean faltouHorarioPraAlgumProfessorDia2 = isFaltouHorarioPraAlgumProfessorParaTrocarCanditatosDeDia(candidatosDia1, candidatosDia2, professoresDia1);


        if (cargaHorariaDia1 <= cargaHorariaDia2 && !faltouHorarioPraAlgumProfessorDia1 && !faltouHorarioPraAlgumProfessorDia2) {
            MoverAulasDiaUtils.moverCandidatosParaDia(dia2, candidatosDia1);
            MoverAulasDiaUtils.moverCandidatosParaDia(dia1, candidatosDia2);
        }
    }

    private boolean isFaltouHorarioPraAlgumProfessorParaTrocarCanditatosDeDia(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia1,
                                                                              List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia2,
                                                                              List<String> professores) {
        for (String professore : professores) {
            int cargaHorariaProfessorDia2 = CargaHorariaUtils.getCargaHorariaProfessorCandidatos(candidatosDia2, professore);
            int cargaHorariaProfessorDia1 = CargaHorariaUtils.getCargaHorariaProfessorCandidatos(candidatosDia1, professore);

            if (this.quantidadeAulasPorTurno - cargaHorariaProfessorDia2 + cargaHorariaProfessorDia1 < 0) {
                return true;
            }
        }

        return false;
    }

    private void repararSolucaoPeriodoIntegral() {
        List<String> aulaEmSequenciaTurmas = new ArrayList<>();
        this.solucaoGRASP.getGradeHorariaAtual().getAulaEmSequenciaTurmas().stream()
                .filter(aulaEmSequencia -> !aulaEmSequencia.isEmpty())
                .forEach(aulaEmSequenciaTurmas::addAll);

        aulaEmSequenciaTurmas = aulaEmSequenciaTurmas.stream().distinct().collect(Collectors.toList());

//            String aula = aulaEmSequenciaTurmas.get(aulaEmSequenciaTurmas.size()-1).get(0);
//            List<Candidato<Vertice<Integer>, Entidade>> candidatosDaAula = this.solucaoGRASP.getCandidatos().stream().filter(candidato -> candidato.getEntidade().get(this.colunaNomeEntidade).equals(aula) && candidato.getEntidade().getTipo().equals(ETipoEntidade.PRATICA)).collect(Collectors.toList());
//            List<Integer> dias = candidatosDaAula.stream().map(candidato -> candidato.getValor().getCor()).collect(Collectors.toList());
//            List<Integer> idsCandidatosDaAula = candidatosDaAula.stream().map(candidato -> candidato.getEntidade().getId()).collect(Collectors.toList());
//
//            candidatosDaAula.forEach(System.out::println);
//            String[] professoresPratica = candidatosDaAula.get(0).getEntidade().get("Professor Prática").split(",");
//            if (professoresPratica.length > 1) {
//                System.out.println(Arrays.toString(professoresPratica));
//                System.out.println(dias);
//
//                for (int i = dias.size() - 1; i >= 0; i--) {
//                    System.out.println();
//                    this.solucaoGRASP.getGradeHorariaAtual().getDia(dias.get(i)).stream()
//                            .filter(horario -> nonNull(horario.getEntidade()) && !idsCandidatosDaAula.contains(horario.getEntidade()))
//                            .forEach(System.out::println);
//                }
//
//            }
//        System.out.println();

        int posDia = this.solucaoGRASP.getCores().size() - 1;
        List<Integer> entidadesNoDia = this.solucaoGRASP.getGradeHorariaAtual().getDia(posDia).stream()
                .map(HorarioAulaDTO::getEntidade)
                .filter(Objects::nonNull).distinct().collect(Collectors.toList());

        while (posDia >= 0 && entidadesNoDia.size() != 1 || !aulaEmSequenciaTurmas.contains(this.solucaoGRASP.getCandidatos().get(entidadesNoDia.get(0) - 1).getEntidade().getNome())) {
            entidadesNoDia = this.solucaoGRASP.getGradeHorariaAtual().getDia(--posDia).stream()
                    .map(HorarioAulaDTO::getEntidade)
                    .filter(Objects::nonNull).distinct().collect(Collectors.toList());
            if (posDia == 0) break;
        }
    }

    @Override
    public boolean ehValida(Grafo<Integer, Integer> solucao) throws NenhumaRestricaoFracaAtivaException {
        restricoesDTO.validarExistenciaRestricaoFraca();

        int avaliacaoTurmas = 0;
        int avaliacaoDias = 0;

        this.solucaoGRASP.setGradeHorariaAtual(null);
        if (restricoesDTO.estaRestringindoPor(ETipoRestricao.DIAS_NAO_CONSECUTIVOS)) {
            setGradeHorariaAtual();
            avaliacaoTurmas = this.solucaoGRASP.getAvaliacaoTurmas();
        }

        int qtdLaboratorios = 2;
        if (restricoesDTO.estaRestringindoPor(ETipoRestricao.LOCAL_DISPONIVEL) && ehPeriodoUnico(this.periodo)) {
            if (isNull(this.solucaoGRASP.getGradeHorariaAtual())) setGradeHorariaAtual();
            avaliacaoDias = (int) AvaliadorSolucaoUtils.getQtdAvaliacoesNegativas(this.solucaoGRASP.getAvaliacoesDias(this.colunaLocal, qtdLaboratorios)) * -1;
        }

        return avaliacaoTurmas >= 0 && avaliacaoDias >= 0 && this.solucaoGRASP.getCores().size() <= 5;
    }

    private void setGradeHorariaAtual() {
        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos = this.solucaoGRASP.getCandidatos();
        List<Integer> coresExistentes = candidatos.stream()
                .filter(Candidato::possuiAulas)
                .filter(Candidato::estaAtivo)
                .map(candidato -> candidato.getValor().getCor())
                .distinct().sorted().collect(Collectors.toList());

        List<Integer> cores = new ArrayList<>();
        Map<Integer, Integer> relacaoNovasCores = new HashMap<>();
        for (int i = 0; i < coresExistentes.size(); i++) {
            Integer corAntiga = coresExistentes.get(i);
            Integer novaCor = i + 1;
            cores.add(novaCor);
            relacaoNovasCores.put(corAntiga, novaCor);
        }

        candidatos.stream().filter(Candidato::estaAtivo)
                .filter(Candidato::possuiAulas)
                .forEach(candidato -> {
                    Integer corAntiga = candidato.getValor().getCor();
                    Integer corNova = relacaoNovasCores.get(corAntiga);
                    MoverAulasDiaUtils.setDiaCandidato(corNova, candidato);
                });

        this.solucaoGRASP.setCores(cores);

        this.solucaoGRASP.setGradeHorariaAtual(new GradeHoraria(this.solucaoGRASP.getCores().size(), this.solucaoGRASP.getCandidatos(), this.colunaHorario, this.quantidadeAulasPorTurno));
    }

    public List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosOrdenadosPorAvaliacao(int qtdMaximaCandidatos) {
        HashMap<Integer, List<Integer>> candidatosAgrupadosPorGrau = this.getCandidatosAgrupadosPorGrau();

        TreeMap<Integer, Integer> qtdEmCadaGrau = new TreeMap<>();
        candidatosAgrupadosPorGrau.forEach((chave, valor) -> qtdEmCadaGrau.put(chave, valor.size()));

        List<Integer> grausOrdenadoPorQuantidadeEmCadaVertice = qtdEmCadaGrau.entrySet().stream()
                .sorted(Map.Entry.comparingByValue()).map(Map.Entry::getKey).collect(Collectors.toList());

        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosOrdenadosPorAvaliacao = new ArrayList<>();
        for (int i = grausOrdenadoPorQuantidadeEmCadaVertice.size() - 1; i >= 0; i--) {
            Integer grau = grausOrdenadoPorQuantidadeEmCadaVertice.get(i);

            List<Integer> numerosCandidatosNoGrau = candidatosAgrupadosPorGrau.get(grau);

            Collections.sort(numerosCandidatosNoGrau);

            for (Integer numeroCandidato : numerosCandidatosNoGrau) {
                Candidato<Vertice<Integer>, EntidadeDTO> candidato = this.solucaoGRASP.getCandidatos().get(numeroCandidato - 1);
                if (!this.solucaoGRASP.getCandidatosColoridos().contains(candidato.getValor())) {
                    candidatosOrdenadosPorAvaliacao.add(candidato);
                }

                if (candidatosOrdenadosPorAvaliacao.size() == qtdMaximaCandidatos) {
                    break;
                }
            }

            if (candidatosOrdenadosPorAvaliacao.size() == qtdMaximaCandidatos) {
                break;
            }
        }

        return candidatosOrdenadosPorAvaliacao;
    }

    public void setCandidatos(List<EntidadeDTO> entidades) {
        this.entidades = entidades;
    }

    @Override
    public void setColunasRestricoesFortes(List<String> colunasRestricoesFortes) {
        restricoesDTO.setColunasRestricoesFortes(colunasRestricoesFortes);
    }

    public void setColunaLocal(String colunaLocal) {
        this.colunaLocal = colunaLocal;
    }

    private void colorirVertice(Vertice<Integer> vertice, List<Integer> coresVizinhos) {
        if (this.solucaoGRASP.getCores().isEmpty()) {
            this.solucaoGRASP.getCores().add(1);
        }

        if (coresVizinhos.isEmpty()) {
            vertice.setCor(this.solucaoGRASP.getCores().get(0));
        } else if (coresVizinhos.size() < this.solucaoGRASP.getCores().size()) {
            vertice.setCor(this.solucaoGRASP.getProximaMenorCor(coresVizinhos));
        } else {
            int maiorCor = coresVizinhos.stream().mapToInt(cor -> cor).max().orElse(0);
            int cor = maiorCor + 1;

            vertice.setCor(cor);

            if (!this.solucaoGRASP.getCores().contains(cor)) {
                this.solucaoGRASP.getCores().add(cor);
            }
        }
    }

    private Grafo<Integer, Integer> gerarGrafo() {
        Grafo<Integer, Integer> grafo = new Grafo<>();

        this.solucaoGRASP.getCandidatos().forEach(verticeCandidato -> {
            Vertice<Integer> vertice = verticeCandidato.getValor();
            adicionaVertice(grafo, vertice);

            verticeCandidato.getConflitos().forEach(conflito -> grafo.adicionarAresta(verticeCandidato.getValor(), this.solucaoGRASP.getCandidatos().get(conflito - 1).getValor(), null));
        });

        return grafo;
    }

    private void adicionaVertice(Grafo<Integer, Integer> solucao, Vertice<Integer> vertice) {
        if (isNull(solucao.buscaVertice(vertice.getRotulo()))) {
            solucao.adicionaVertice(vertice);
        }
    }

    private void setAvaliacaoCandidato(HashMap<Integer, List<Integer>> verticesAgrupadosPorGrau, Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        double avaliacao = NumericUtils.getDouble(verticesAgrupadosPorGrau.get(candidato.getGrau()).size()) / NumericUtils.getDouble(this.solucaoGRASP.getCandidatos().size());
        BigDecimal av = BigDecimal.valueOf(avaliacao).setScale(2, RoundingMode.HALF_EVEN);
        candidato.setAvaliacao(av.doubleValue());
    }

    private HashMap<Integer, List<Integer>> getCandidatosAgrupadosPorGrau() {
        Grafo<Integer, Integer> grafo = this.gerarGrafo();

        HashMap<Integer, List<Integer>> graus = new HashMap<>();
        this.solucaoGRASP.getCandidatos().forEach(verticeCandidato -> {
            int grau = grafo.listaRotuloAdjacentesPorDestino(verticeCandidato.getValor()).size();

            verticeCandidato.setGrau(grau);

            graus.put(grau, adicionarNaListaDeVerticesNoGrau(graus.get(grau), verticeCandidato.getNumero()));
        });

        return graus;
    }

    private List<Integer> adicionarNaListaDeVerticesNoGrau(List<Integer> listaVerticesNoGrau, Integer
            numeroCandidato) {
        if (isNull(listaVerticesNoGrau)) {
            listaVerticesNoGrau = new ArrayList<>();
        }

        listaVerticesNoGrau.add(numeroCandidato);

        return listaVerticesNoGrau;
    }
}
