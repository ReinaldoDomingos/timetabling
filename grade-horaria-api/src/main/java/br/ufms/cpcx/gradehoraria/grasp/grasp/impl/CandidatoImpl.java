package br.ufms.cpcx.gradehoraria.grasp.grasp.impl;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;

import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public class CandidatoImpl extends Candidato<Vertice<Integer>, EntidadeDTO> {

    public CandidatoImpl(EntidadeDTO entidade) {
        Integer numero = entidade.getId();

        this.numero = numero;
        this.entidade = entidade;
        this.valor = new Vertice<>(numero);
        this.conflitos = entidade.getListaDeEntidadesComConflitos()
                .stream().map(Integer::parseInt).collect(Collectors.toList());
    }

    @Override
    public boolean possuiAulas() {
        return !this.entidade.getAulas().isEmpty();
    }

    @Override
    public boolean estaAtivo() {
        return nonNull(this.valor);
    }

}
