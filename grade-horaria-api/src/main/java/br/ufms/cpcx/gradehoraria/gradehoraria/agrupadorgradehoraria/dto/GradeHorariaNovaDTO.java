package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Setter
public class GradeHorariaNovaDTO {
    private Integer quantidadeAulasPorTurno;
    private List<DiaGradeHorariaDTO> dias;

    public GradeHorariaNovaDTO(Integer quantidadeAulasPorTurno) {
        this.dias = new ArrayList<>();
        this.quantidadeAulasPorTurno = quantidadeAulasPorTurno;
    }

    public List<DiaGradeHorariaDTO> getDias() {
        return dias;
    }

    public List<EntidadeDTO> getEntidades() {
        List<EntidadeDTO> entidades = new ArrayList<>();

        this.dias.forEach(diaGradeHorariaDTO -> entidades.addAll(diaGradeHorariaDTO.getEntidades()));

        return entidades;
    }

    public Integer getQuantidadeAulasPorTurno() {
        return this.quantidadeAulasPorTurno;
    }

    public DiaGradeHorariaDTO adicionarDia(Integer dia) {
        DiaGradeHorariaDTO diaGradeHorariaDTO = new DiaGradeHorariaDTO(dia);

        this.dias.add(diaGradeHorariaDTO);

        return diaGradeHorariaDTO;
    }

    public DiaGradeHorariaDTO getDia(Integer dia) {
        List<DiaGradeHorariaDTO> diasFiltrados = this.dias.stream().filter(diaGradeHorariaDTO -> diaGradeHorariaDTO.getDia().equals(dia)).collect(Collectors.toList());

        return diasFiltrados.isEmpty() ? null : diasFiltrados.get(0);
    }
}
