package br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration;

public enum ESituacaoRestricao {
    NAO_APLICADO,
    APLICADO
}
