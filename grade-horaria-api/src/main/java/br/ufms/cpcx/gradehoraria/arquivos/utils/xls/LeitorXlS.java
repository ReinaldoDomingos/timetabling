package br.ufms.cpcx.gradehoraria.arquivos.utils.xls;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.utils.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.openxml4j.exceptions.OLE2NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class LeitorXlS {
    private LeitorXlS() {
    }

    public static Workbook getPlanilha(InputStream fis) throws IOException {
        try {
            return new XSSFWorkbook(fis);
        } catch (OLE2NotOfficeXmlFileException exception) {
            return new HSSFWorkbook(fis);
        }catch (NotOfficeXmlFileException erro){
            throw new GenericException("O arquivo importado deve ser uma planinha xls ou xlsx.");
        }
    }

    public static XSSFWorkbook getPlanilha(String nomeArquivo) throws IOException {
        File arquivo = new File(nomeArquivo);

        FileInputStream fis = new FileInputStream(arquivo);

        return new XSSFWorkbook(fis);
    }

    public static List<String> getLinhas(String nomeArquivo) throws IOException {
        return getLinhas(nomeArquivo, 0);
    }

    public static List<String> getLinhas(String nomeArquivo, int indexPagina) throws IOException {
        XSSFWorkbook planilha = getPlanilha(nomeArquivo);

        return getLinhas(planilha, indexPagina);
    }

    public static List<String> getLinhas(XSSFWorkbook planilha, String nomePagina) {
        int indexPagina = getIndexPaginaPorNome(planilha, nomePagina);

        return getLinhas(planilha, indexPagina);
    }

    public static List<String> getLinhas(XSSFWorkbook planilha, int indexPagina) {
        XSSFSheet pagina = planilha.getSheetAt(indexPagina);

        List<String> linhas = new ArrayList<>();

        StringBuilder linha;
        for (Row row : pagina) {
            linha = getLinhaEmString(row);
            linhas.add(linha.toString());
        }

        return linhas;
    }

    public static List<String> getCabecalho(Workbook planilha, int indexPagina) {
        Sheet pagina = planilha.getSheetAt(indexPagina);
        return StringUtils.getListaDoTexto(getLinhaEmString(pagina.getRow(0)).toString().replaceAll("\t", ","));
    }

    private static StringBuilder getLinhaEmString(Row row) {
        StringBuilder linha;
        linha = new StringBuilder();
        for (Cell coluna : row) {
            try {
                linha.append(coluna.getStringCellValue()).append("\t");
            } catch (Exception e) {
                linha.append(Double.valueOf(coluna.getNumericCellValue()).intValue()).append("\t");
            }
        }
        return linha;
    }

    private static int getIndexPaginaPorNome(XSSFWorkbook planilha, String nomePagina) {
        int indexPagina = 0;
        for (int i = 0; i < planilha.getNumberOfSheets(); i++) {
            if (planilha.getSheetName(i).equals(nomePagina)) {
                indexPagina = i;
            }
        }
        return indexPagina;
    }
}