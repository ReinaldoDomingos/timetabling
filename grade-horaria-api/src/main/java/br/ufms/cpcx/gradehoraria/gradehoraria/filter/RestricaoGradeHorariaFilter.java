package br.ufms.cpcx.gradehoraria.gradehoraria.filter;

import br.ufms.cpcx.gradehoraria.generico.filter.GenericFilter;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import org.springframework.beans.BeanUtils;

import java.util.Map;

import static br.ufms.cpcx.gradehoraria.generico.utils.FilterUtils.getParametro;
import static java.util.Objects.nonNull;

public class RestricaoGradeHorariaFilter extends GenericFilter {

    private ETipoRestricao tipo;

    private Long idGradeHoraria;

    private Long idProfessor;

    public RestricaoGradeHorariaFilter() {
    }

    public RestricaoGradeHorariaFilter(Map<String, String> parametros) {
        super(parametros);
        setTipo(parametros);
    }

    public ETipoRestricao getTipo() {
        return tipo;
    }

    public void setTipo(ETipoRestricao tipo) {
        this.tipo = tipo;
    }

    private void setTipo(Map<String, String> parametros) {
        String tipoStr = getParametro(parametros, "tipo");

        if (nonNull(tipoStr)) {
            this.tipo = ETipoRestricao.valueOf(tipoStr);
        }
    }

    public Long getIdGradeHoraria() {
        return idGradeHoraria;
    }

    public void setIdGradeHoraria(Long idGradeHoraria) {
        this.idGradeHoraria = idGradeHoraria;
    }

    public Long getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(Long idProfessor) {
        this.idProfessor = idProfessor;
    }

    public static RestricaoGradeHorariaFilter of(Map<String, String> parametros) {
        return new RestricaoGradeHorariaFilter(parametros);
    }

    public RestricaoGradeHorariaFilter clonar() {
        RestricaoGradeHorariaFilter clone = new RestricaoGradeHorariaFilter();
        BeanUtils.copyProperties(this, clone);

        return clone;
    }
}
