package br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto;


import br.ufms.cpcx.gradehoraria.generico.utils.StringUtils;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class EntidadeDTO extends HashMap<String, String> {
    private Integer id;
    private String nome;
    private Integer numero;
    private String semestre;
    private String professor;
    private Integer cargaHoraria;
    private List<String> atributos;
    private List<AulaDTO> aulas;
    private List<String> idsEntidadesIdenticas;
    private List<String> listaDeEntidadesComConflitos;
    private Integer diaInicial;

    public EntidadeDTO(Integer numero) {
        this.numero = numero;
        this.cargaHoraria = 0;
        inicializarListas();
    }

    public EntidadeDTO() {
        this.cargaHoraria = 0;
        inicializarListas();
    }

    public List<String> getProfessores() {
        return this.aulas.stream().map(AulaDTO::getProfessor).collect(Collectors.toList());
    }

    public int getCargaHorariaTotalProfessor(String professor) {
        int cargaHorariaTotal = 0;

        List<AulaDTO> aulasProfessor = this.aulas.stream()
                .filter(horarioAula -> horarioAula.getProfessor().equals(professor))
                .collect(Collectors.toList());

        for (AulaDTO aula : aulasProfessor) {
            cargaHorariaTotal += aula.getCargaHoraria();
        }

        return cargaHorariaTotal;
    }

    public int getCargaHorariaTotal() {
        return this.aulas.stream().mapToInt(AulaDTO::getCargaHoraria).sum();
    }

    private void inicializarListas() {
        this.aulas = new ArrayList<>();
        this.atributos = new ArrayList<>();
        this.idsEntidadesIdenticas = new ArrayList<>();
        this.listaDeEntidadesComConflitos = new ArrayList<>();
    }

    public void adicionarEntidadesComConflitos(List<String> lista) {
        List<String> listaFiltrada = lista.stream()
//                .filter(item -> !this.idsEntidadesIdenticas.contains(item))
                .filter(item -> !jaExisteConflito(item))
                .collect(Collectors.toList());

        this.listaDeEntidadesComConflitos.addAll(listaFiltrada);
    }

    private boolean jaExisteConflito(String item) {
        return item.equals(this.getId().toString()) || this.listaDeEntidadesComConflitos.contains(item);
    }

    @Override
    public String put(String chave, String valor) {
        validarAtributo(chave);
        return super.put(chave, valor);
    }

    private String gerarTextoLinhaAtributo(Object atributo, Object valor) {
        return atributo + "=" + valor + "\n";
    }

    private void validarAtributo(Object chave) {
        if (!this.atributos.contains(chave)) {
            this.atributos.add(chave.toString());
        }
    }

    @Override
    public String toString() {
        Object[] chaves = keySet().toArray();
        Object[] valores = values().toArray();
        StringBuilder retorno = new StringBuilder();
        retorno.append(gerarTextoLinhaAtributo("Id", this.id));
        for (int i = 0; i < keySet().size(); i++) {
            String atributoComValor = gerarTextoLinhaAtributo(chaves[i], valores[i]);
            retorno.append(atributoComValor);
        }
        retorno.append(gerarTextoLinhaAtributo("conflitos", this.listaDeEntidadesComConflitos.toString()));

        return StringUtils.removerColchetes(retorno.toString());
    }

    public EntidadeDTO clonar() {
        EntidadeDTO clone = new EntidadeDTO();
        BeanUtils.copyProperties(this, clone);

        this.aulas = aulas.stream().map(AulaDTO::clonar).collect(Collectors.toList());

        return clone;
    }

    public List<AulaDTO> getAulasDisciplinas(List<String> disciplinas) {
        return this.aulas.stream().filter(aulaDTO -> disciplinas.contains(aulaDTO.getDisciplina())).collect(Collectors.toList());
    }
}
