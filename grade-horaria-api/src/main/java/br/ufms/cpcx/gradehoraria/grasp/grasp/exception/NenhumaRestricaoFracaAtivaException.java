package br.ufms.cpcx.gradehoraria.grasp.grasp.exception;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;

public class NenhumaRestricaoFracaAtivaException extends GenericException {
    public NenhumaRestricaoFracaAtivaException() {
        super("Adicione ao menos uma restrição fraca pra avaliação das soluções.");
    }
}
