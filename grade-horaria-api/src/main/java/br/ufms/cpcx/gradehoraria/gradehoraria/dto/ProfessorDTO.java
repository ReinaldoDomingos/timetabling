package br.ufms.cpcx.gradehoraria.gradehoraria.dto;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.modelmapper.ModelMapper;

@Data
@AllArgsConstructor
public class ProfessorDTO {
    private Long id;
    private String nome;
    private Long codigo;
    private Long gradeHorariaTesteId;

    public ProfessorDTO() {
    }

    public ProfessorDTO(Professor professor) {
        this.id = professor.getId();
        this.nome = professor.getNome();
        this.codigo = professor.getCodigo();
        this.gradeHorariaTesteId = professor.getGradeHorariaTesteId();
    }

    public static Professor toMapProfessor(ProfessorDTO professorDTO) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(professorDTO, Professor.class);
    }

    public static ProfessorDTO fromProfessor(Professor professor) {
        ModelMapper mapper = new ModelMapper();

        return mapper.map(professor, ProfessorDTO.class);
    }

}
