package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.business;

import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto.CargaHorariaProfessorDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto.GradeCargaHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto.TurmaCargaHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.DiaGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.GradeHorariaNovaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.ProfessorGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.TurmaGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CandidatoUtils;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CargaHorariaUtils;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
public class AgrupadorCargaHorariaBO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgrupadorCargaHorariaBO.class);//NOSONAR

    public GradeHorariaNovaDTO gerarGradeHoraria(Integer quantidadeAulasPorTurno, List<EntidadeDTO> entidades, List<String> disciplinasNoMesmoDia) {
        Map<String, List<EntidadeDTO>> entidadesPorTurma = entidades.stream().collect(Collectors.groupingBy(EntidadeDTO::getSemestre));

        GradeCargaHorariaDTO gradeCargaHorariaDTO = new GradeCargaHorariaDTO(quantidadeAulasPorTurno);

        List<String> turmas = new ArrayList<>(entidadesPorTurma.keySet());

        gradeCargaHorariaDTO.adicionarTurmas(turmas);

        int cargaHorariaMaiorTurma = getCargaHorariaMaiorTurma(entidadesPorTurma);

        int qtdDiasMinimo = cargaHorariaMaiorTurma / quantidadeAulasPorTurno;

        gradeCargaHorariaDTO.adicionarDias(qtdDiasMinimo, quantidadeAulasPorTurno);

        List<EntidadeDTO> entidadesAdicionadas = new ArrayList<>();

        List<EntidadeDTO> copiaEntidades = new ArrayList<>(entidades);

        copiaEntidades.sort(Comparator.comparing(EntidadeDTO::getNome));

//        alocarEntidadesDeProfessoresComMaiorCargaHoraria(gradeCargaHorariaDTO, copiaEntidades, entidadesAdicionadas);//NOSONAR

        alocarEntidadesDasDisciplinas(gradeCargaHorariaDTO, copiaEntidades, entidadesAdicionadas, disciplinasNoMesmoDia);

        alocarEntidadesComMaisDeUmDia(gradeCargaHorariaDTO, copiaEntidades, entidadesAdicionadas, quantidadeAulasPorTurno);

        alocarEntidades(gradeCargaHorariaDTO, copiaEntidades, entidadesAdicionadas);

        return converterEmGradeHorariaNovaDTO(quantidadeAulasPorTurno, entidadesAdicionadas, turmas);
    }

    private void alocarEntidades(GradeCargaHorariaDTO gradeCargaHorariaDTO,
                                 List<EntidadeDTO> entidades,
                                 List<EntidadeDTO> entidadesAdicionadas) {
        while (!entidades.isEmpty()) {
            adicionarEntidade(gradeCargaHorariaDTO, entidades, entidadesAdicionadas);
        }
    }

    private void alocarEntidadesDeProfessoresComMaiorCargaHoraria(GradeCargaHorariaDTO gradeCargaHorariaDTO,//NOSONAR
                                                                  List<EntidadeDTO> entidades,
                                                                  List<EntidadeDTO> entidadesAdicionadas) {

        Map<String, List<EntidadeDTO>> entidadesAgrupadasPorProfessor = entidades.stream().collect(Collectors.groupingBy(EntidadeDTO::getProfessor));

        List<CargaHorariaProfessorDTO> cargaHorariaProfessor = new ArrayList<>();

        List<String> professores = new ArrayList<>(entidadesAgrupadasPorProfessor.keySet());
        for (String professor : professores) {
            cargaHorariaProfessor.add(new CargaHorariaProfessorDTO(professor, CargaHorariaUtils.getCargaHorariaEntidades(entidadesAgrupadasPorProfessor.get(professor))));
        }

        cargaHorariaProfessor.sort(Comparator.comparing(CargaHorariaProfessorDTO::getCargaHoraria).reversed());

        for (int i = 0; i < professores.size() * 0.5; i++) {
            List<EntidadeDTO> entidadesProfessor = entidadesAgrupadasPorProfessor.get(cargaHorariaProfessor.get(i).getProfessor());
            entidadesProfessor.sort(Comparator.comparing(EntidadeDTO::getNome));

            for (int j = 0; j < entidadesProfessor.size(); j++) {
                entidades.remove(entidadesProfessor.get(0));
                adicionarEntidade(gradeCargaHorariaDTO, entidadesProfessor, entidadesAdicionadas);
                imprimirCargaHorariasTurmas(gradeCargaHorariaDTO, entidadesAdicionadas);
            }
        }
    }

    private void alocarEntidadesDasDisciplinas(GradeCargaHorariaDTO gradeCargaHorariaDTO,
                                               List<EntidadeDTO> entidades,
                                               List<EntidadeDTO> entidadesAdicionadas,
                                               List<String> disciplinasNoMesmoDia) {

        for (String disciplina : disciplinasNoMesmoDia) {
            List<EntidadeDTO> entidadesDaDisciplina = CandidatoUtils.getEntidadessDasDisciplinas(entidades, Collections.singletonList(disciplina));

            entidadesDaDisciplina.sort(Comparator.comparing(EntidadeDTO::getNome));

            Integer diaAnterior = null;
            while (!entidadesDaDisciplina.isEmpty()) {
                EntidadeDTO entidade = entidadesDaDisciplina.get(0);
                entidades.remove(entidade);
                adicionarEntidadeApartirDoDia(gradeCargaHorariaDTO, entidadesDaDisciplina, entidadesAdicionadas, diaAnterior);
                diaAnterior = entidade.getDiaInicial();
            }
        }
    }

    private void alocarEntidadesComMaisDeUmDia(GradeCargaHorariaDTO gradeCargaHorariaDTO,
                                               List<EntidadeDTO> entidades,
                                               List<EntidadeDTO> entidadesAdicionadas,
                                               Integer quantidadeAulasPorTurno) {

        List<EntidadeDTO> entidadesDisciplinasMaisDeUmDia = CandidatoUtils.getEntidadesComMaisDeUmDia(entidades, quantidadeAulasPorTurno);

        entidadesDisciplinasMaisDeUmDia.sort(Comparator.comparing(EntidadeDTO::getNome));

        while (!entidadesDisciplinasMaisDeUmDia.isEmpty()) {
            entidades.remove(entidadesDisciplinasMaisDeUmDia.get(0));
            adicionarEntidade(gradeCargaHorariaDTO, entidadesDisciplinasMaisDeUmDia, entidadesAdicionadas);
        }
    }

    private int getCargaHorariaMaiorTurma(Map<String, List<EntidadeDTO>> entidadesPorTurma) {
        List<String> turmas = new ArrayList<>(entidadesPorTurma.keySet());

        int cargaHorariaMaiorTurma = 0;
        for (String turma : turmas) {
            int cargaHorariaTurma = CargaHorariaUtils.getCargaHorariaEntidades(entidadesPorTurma.get(turma));
            if (cargaHorariaTurma > cargaHorariaMaiorTurma) {
                cargaHorariaMaiorTurma = cargaHorariaTurma;
            }
        }
        return cargaHorariaMaiorTurma;
    }

    private GradeHorariaNovaDTO converterEmGradeHorariaNovaDTO(Integer quantidadeAulasPorTurno, List<EntidadeDTO> entidadesAdicionadas, List<String> turmas) {
        GradeHorariaNovaDTO gradeHorariaNovaDTO = new GradeHorariaNovaDTO(quantidadeAulasPorTurno);

        while (!entidadesAdicionadas.isEmpty()) {
            EntidadeDTO entidade = entidadesAdicionadas.remove(0);

            DiaGradeHorariaDTO diaGradeHorariaDTO = gradeHorariaNovaDTO.getDia(entidade.getDiaInicial());

            if (isNull(diaGradeHorariaDTO)) {
                diaGradeHorariaDTO = gradeHorariaNovaDTO.adicionarDia(entidade.getDiaInicial());
            }

            TurmaGradeHorariaDTO turmaGradeHorariaDTO = diaGradeHorariaDTO.getTurma(entidade.getSemestre());

            if (isNull(turmaGradeHorariaDTO)) {
                turmaGradeHorariaDTO = diaGradeHorariaDTO.adicionarTurma(entidade.getSemestre());
            }

            ProfessorGradeHorariaDTO professorGradeHorariaDTO = turmaGradeHorariaDTO.getProfessor(entidade.getProfessor());

            if (isNull(professorGradeHorariaDTO)) {
                professorGradeHorariaDTO = turmaGradeHorariaDTO.adicionarProfessor(entidade.getProfessor());
            }

            professorGradeHorariaDTO.adicionarEntidade(entidade);
        }

        gradeHorariaNovaDTO.getDias().forEach(diaGradeHorariaDTO ->
                turmas.forEach(turma -> {
                    if (isNull(diaGradeHorariaDTO.getTurma(turma))) {
                        diaGradeHorariaDTO.adicionarTurma(turma);
                    }
                })
        );

        return gradeHorariaNovaDTO;
    }

    private void adicionarEntidade(GradeCargaHorariaDTO gradeCargaHorariaDTO,
                                   List<EntidadeDTO> entidades,
                                   List<EntidadeDTO> entidadesAdicionadas) {

        EntidadeDTO entidade = entidades.get(0);

        gradeCargaHorariaDTO.adicionarEntidade(entidade);

        entidades.remove(entidade);
        entidadesAdicionadas.add(entidade);
    }

    private void adicionarEntidadeApartirDoDia(GradeCargaHorariaDTO gradeCargaHorariaDTO,
                                               List<EntidadeDTO> entidades,
                                               List<EntidadeDTO> entidadesAdicionadas,
                                               Integer diaInicial) {
        EntidadeDTO entidade = entidades.get(0);

        gradeCargaHorariaDTO.adicionarEntidade(entidade, diaInicial);

        entidades.remove(entidade);
        entidadesAdicionadas.add(entidade);
    }

    private void imprimirCargaHorariasTurmas(GradeCargaHorariaDTO gradeCargaHorariaDTO, List<EntidadeDTO> entidadesAdicionadas) {
        System.out.print(" \t");//NOSONAR
        gradeCargaHorariaDTO.getTurmas().get(0).getDias().forEach(diaCargaHorariaDTO -> System.out.print(diaCargaHorariaDTO.getDia() + " "));//NOSONAR
        System.out.println();//NOSONAR
        for (TurmaCargaHorariaDTO turmaCargaHorariaDTO : gradeCargaHorariaDTO.getTurmas()) {
            System.out.printf("%s", turmaCargaHorariaDTO.getTurma() + "\t");//NOSONAR
            turmaCargaHorariaDTO.getDias().forEach(diaCargaHorariaDTO -> System.out.print(diaCargaHorariaDTO.getCargaHorariaDisponivelTurma() + " "));//NOSONAR
            System.out.println();//NOSONAR
        }

        System.out.println("---------------------------------------------------");//NOSONAR

        Map<Integer, List<EntidadeDTO>> entidadesPorDia = entidadesAdicionadas.stream().collect(Collectors.groupingBy(EntidadeDTO::getDiaInicial));

        entidadesPorDia.forEach((dia, entidades) -> {
            System.out.println("Dia " + dia);//NOSONAR

            System.out.println("\nDisciplinas: ");//NOSONAR
            entidades.forEach(entidade -> System.out.println(entidade.getNome() + " CH: " + entidade.getCargaHorariaTotal()));//NOSONAR

            System.out.println("\nProfessores:");//NOSONAR
            Map<String, List<EntidadeDTO>> entidadesPorProfessor = entidades.stream().collect(Collectors.groupingBy(EntidadeDTO::getProfessor));

            entidadesPorProfessor.forEach((professor, entidadesProfessor) -> {
                int cargaHorariaProfessorNoDia = CargaHorariaUtils.getCargaHorariaEntidades(entidadesProfessor);
                System.out.println("Professor " + professor + " CH: " + cargaHorariaProfessorNoDia);//NOSONAR
                if (cargaHorariaProfessorNoDia > gradeCargaHorariaDTO.getQuantidadeAulasPorTurno()) {
                    System.out.println(cargaHorariaProfessorNoDia + " aulas no professor " + professor);//NOSONAR
                }
            });

            System.out.println();//NOSONAR
        });

        System.out.println("---------------------------------------------------");//NOSONAR
    }
}
