package br.ufms.cpcx.gradehoraria.grasp.grasp.utils;

import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.GradeHorariaNovaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CandidatoUtils;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CargaHorariaUtils;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class RestricaoUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestricaoUtils.class);

    private RestricaoUtils() {
    }

    public static List<EntidadeDTO> juntarDisciplinaEmMenosCandidatos(List<EntidadeDTO> entidades, Integer quantidadeAulasPorTurno) {
        List<EntidadeDTO> copiasEntidades = CandidatoUtils.clonarEntidades(entidades);

        copiasEntidades.sort(Comparator.comparing(EntidadeDTO::getCargaHoraria).reversed());

        Map<String, List<EntidadeDTO>> entidadesPorSemestre = CandidatoUtils.getEntidadesPorSemestre(copiasEntidades);

        for (Map.Entry<String, List<EntidadeDTO>> entry : entidadesPorSemestre.entrySet()) {
            List<EntidadeDTO> entidadesNoSemestre = entry.getValue();
            entidadesNoSemestre.sort(Comparator.comparing(EntidadeDTO::getNome));

            while (entidadesNoSemestre.size() > 1) {
                List<EntidadeDTO> entidadesParaJuntar = new ArrayList<>();

                EntidadeDTO entidade = entidadesNoSemestre.remove(0);
                Integer totalCargaHoraria = entidade.getCargaHoraria();
                for (EntidadeDTO value : entidadesNoSemestre) {
                    if (totalCargaHoraria + value.getCargaHoraria() <= quantidadeAulasPorTurno) {
                        entidadesParaJuntar.add(value);
                        totalCargaHoraria += value.getCargaHoraria();
                    }
                }

                if (!entidadesParaJuntar.isEmpty()) {
                    entidadesParaJuntar.stream().map(RestricaoUtils::converterEntidadeEmAula).forEach(aula -> entidade.getAulas().add(aula));

                    entidade.setCargaHoraria(totalCargaHoraria);
                    copiasEntidades.removeAll(entidadesParaJuntar);
                    entidadesNoSemestre.removeAll(entidadesParaJuntar);
                } else {
                    break;
                }
            }
        }

        CandidatoUtils.gerarNovosIdsEntidades(copiasEntidades);

        return copiasEntidades;
    }

    public static List<EntidadeDTO> juntarDisciplinaEmMenosCandidatos(GradeHorariaNovaDTO gradeHorariaNovaDTO) {
        AtomicInteger id = new AtomicInteger(1);

        List<EntidadeDTO> entidades = new ArrayList<>();

        gradeHorariaNovaDTO.getDias().forEach(diaGradeHorariaDTO ->
                diaGradeHorariaDTO.getTurmas().forEach(turmaGradeHorariaDTO -> {
                            List<EntidadeDTO> entidadesNaTurmaNoDia = CandidatoUtils.clonarEntidades(turmaGradeHorariaDTO.getEntidades());

                            if (!entidadesNaTurmaNoDia.isEmpty()) {
                                EntidadeDTO entidade = new EntidadeDTO();
                                entidade.setId(id.getAndIncrement());
                                entidade.setDiaInicial(diaGradeHorariaDTO.getDia());
                                entidade.setNome("");
                                entidade.setSemestre(turmaGradeHorariaDTO.getTurma());

                                for (EntidadeDTO entidadeDTO : entidadesNaTurmaNoDia) {
                                    entidade.setCargaHoraria(entidade.getCargaHoraria() + entidadeDTO.getCargaHorariaTotal());
                                    entidade.getAulas().addAll(entidadeDTO.clonar().getAulas());
                                }

                                entidades.add(entidade);
                            }
                        }
                )
        );

        return entidades;
    }

    public static void executarRestricaoDiasAlternados(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, List<String> nomesEntidadesRestritas) {//NOSONAR
        for (String disciplina : nomesEntidadesRestritas) {
            List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosRestricao = CandidatoUtils.getCandidatosDaDisciplina(candidatos, disciplina);

            List<Integer> dias = CandidatoUtils.getDiasCandidatos(candidatosRestricao);

            dias.sort(Comparator.comparing(Integer::intValue));

            int qtdDiasIdeal = 5;

            if (dias.size() > 1) {
                for (int j = 0; j < dias.size() - 1; j++) {//NOSONAR
                    Integer dia1 = dias.get(j);
                    Integer dia2 = dias.get(j + 1);

                    if (dia1 + 1 != dia2) continue;

                    if (dia2 + 1 <= qtdDiasIdeal) {
                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia2 = CandidatoUtils.getCandidatosNoDia(candidatos, dia2);
                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDepoisDia2 = CandidatoUtils.getCandidatosNoDia(candidatos, dia2 + 1);

                        MoverAulasDiaUtils.moverCandidatosParaDia(dia2 + 1, candidatosDia2);
                        MoverAulasDiaUtils.moverCandidatosParaDia(dia2, candidatosDepoisDia2);

                        candidatosRestricao = CandidatoUtils.getCandidatosDaDisciplina(candidatos, disciplina);

                        dias = CandidatoUtils.getDiasCandidatos(candidatosRestricao);

                        if (dias.size() > 2) {
                            GradeHorariaUtils.imprimirGradeHoraria(candidatosRestricao, 4);
                        }

                        break;
                    } else if (dia1 - 1 > 0) {
                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDia1 = CandidatoUtils.getCandidatosNoDia(candidatos, dia1);
                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosAntesDia1 = CandidatoUtils.getCandidatosNoDia(candidatos, dia1 - 1);

                        MoverAulasDiaUtils.moverCandidatosParaDia(dia1 - 1, candidatosDia1);
                        MoverAulasDiaUtils.moverCandidatosParaDia(dia1, candidatosAntesDia1);
                    }

                }
            }
        }
    }

    public static void juntarCandidatosMesmaDisciplina(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, Integer quantidadeAulasPorTurno) {
        GradeHorariaUtils.imprimirGradeHoraria(candidatos, quantidadeAulasPorTurno);

        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosParciaisDeUnicaDisciplinaComMaisDeUmDiaNaTurma = getCandidatosParciaisDeUnicaDisciplinaComMaisDeUmDiaNaTurma(candidatos, quantidadeAulasPorTurno);

        GradeHorariaUtils.imprimirGradeHoraria(candidatosParciaisDeUnicaDisciplinaComMaisDeUmDiaNaTurma, quantidadeAulasPorTurno);

        Map<String, List<AulaDTO>> aulasCandidatosAgrupadosPorDisciplinas = CandidatoUtils.getAulasCandidatosAgrupadasPorDisciplina(candidatosParciaisDeUnicaDisciplinaComMaisDeUmDiaNaTurma);

        List<String> disciplinas = new ArrayList<>(aulasCandidatosAgrupadosPorDisciplinas.keySet());
        List<String> turmas = CandidatoUtils.getSemestresCandidatos(candidatosParciaisDeUnicaDisciplinaComMaisDeUmDiaNaTurma);

        for (String turma : turmas) {
            List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNaTurma = CandidatoUtils.getCandidatosNaTurma(candidatosParciaisDeUnicaDisciplinaComMaisDeUmDiaNaTurma, turma);

            for (String disciplina : disciplinas) {
                List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDaDisciplinaNaTurma = candidatosNaTurma.stream()
                        .filter(Candidato::possuiAulas)
                        .filter(candidato -> candidato.getEntidade().getAulas().get(0).getDisciplina().equals(disciplina))
                        .collect(Collectors.toList());

                Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosDisciplinaPorDia = CandidatoUtils.getCandidatosPorDia(candidatosDaDisciplinaNaTurma);

                List<Integer> diasCandidatosDisciplina = new ArrayList<>(candidatosDisciplinaPorDia.keySet());

                boolean moverEntidades = false;
                for (int i = 0; i < diasCandidatosDisciplina.size() - 1 && !moverEntidades; i++) {
                    Integer dia1 = diasCandidatosDisciplina.get(i);
                    Integer dia2 = diasCandidatosDisciplina.get(i + 1);

                    Candidato<Vertice<Integer>, EntidadeDTO> candidatoDia1 = candidatosDisciplinaPorDia.get(dia1).get(0);
                    Candidato<Vertice<Integer>, EntidadeDTO> candidatoDia2 = candidatosDisciplinaPorDia.get(dia2).get(0);

                    int cargaHorariaCandidatoDia1 = CargaHorariaUtils.getCargaHorariaCandidato(candidatoDia1);
                    int cargaHorariaCandidatoDia2 = CargaHorariaUtils.getCargaHorariaCandidato(candidatoDia2);

                    Map<Integer, Integer> cargaHorariaProfessorPorDia = CargaHorariaUtils.getCargaHorariaProfessorPorDia(candidatos, candidatoDia1.getEntidade().getProfessores().get(0));

                    Integer cargaHorariaProfessorNoDia1 = cargaHorariaProfessorPorDia.get(dia1);
                    Integer cargaHorariaProfessorNoDia2 = cargaHorariaProfessorPorDia.get(dia2);

                    StringBuilder stringBuilder = new StringBuilder("Movendo candidato disciplina ");
                    if (MoverAulasDiaUtils.ehPossivelMoverParaDia2(cargaHorariaCandidatoDia2, cargaHorariaCandidatoDia1, cargaHorariaProfessorNoDia1, quantidadeAulasPorTurno)) {
                        LOGGER.info(stringBuilder.append(disciplina).append(" do dia ").append(dia2).append(" pra dia ").append(dia1).toString());//NOSONAR
                        MoverAulasDiaUtils.juntarCandidatosNoDia1(candidatoDia1, candidatoDia2);
                        moverEntidades = true;
                    } else if (MoverAulasDiaUtils.ehPossivelMoverParaDia2(cargaHorariaCandidatoDia1, cargaHorariaCandidatoDia2, cargaHorariaProfessorNoDia2, quantidadeAulasPorTurno)) {
                        LOGGER.info(stringBuilder.append(disciplina).append(" do dia ").append(dia1).append(" pra dia ").append(dia2).toString());//NOSONAR
                        MoverAulasDiaUtils.juntarCandidatosNoDia1(candidatoDia2, candidatoDia1);
                        moverEntidades = true;
                    }
                }
            }
        }

        GradeHorariaUtils.imprimirGradeHoraria(candidatos, quantidadeAulasPorTurno);
    }

    public static void realizarTrocaEntreCandidatosDiasAlternados(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, List<String> nomesEntidadesRestritas, Integer quantidadeAulasPorTurno) {
        if (nomesEntidadesRestritas.size() > 1) {
            for (int i = 0; i < nomesEntidadesRestritas.size() - 1; i++) {
                String disciplina1 = nomesEntidadesRestritas.get(i);
                String disciplina2 = nomesEntidadesRestritas.get(i + 1);

                List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDaDisciplina1 = CandidatoUtils.getCandidatosDaDisciplina(candidatos, disciplina1);
                List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosDaDisciplina2 = CandidatoUtils.getCandidatosDaDisciplina(candidatos, disciplina2);

                List<Integer> diasCandidatosDisciplina1 = CandidatoUtils.getDiasCandidatos(candidatosDaDisciplina1);
                List<Integer> diasCandidatosDisciplina2 = CandidatoUtils.getDiasCandidatos(candidatosDaDisciplina2);

                List<Integer> diasCandidatosDisciplina1NaoPossuiNaDisciplina2 = diasCandidatosDisciplina1.stream()
                        .filter(dia -> !diasCandidatosDisciplina2.contains(dia)).sorted().collect(Collectors.toList());
                List<Integer> diasDiferentes = new ArrayList<>(diasCandidatosDisciplina1NaoPossuiNaDisciplina2);

                List<Integer> diasCandidatosDisciplina2NaoPossuiNaDisciplina1 = diasCandidatosDisciplina2.stream()
                        .filter(dia -> !diasCandidatosDisciplina1.contains(dia)).sorted().collect(Collectors.toList());
                diasDiferentes.addAll(diasCandidatosDisciplina2NaoPossuiNaDisciplina1);


//                List<Integer> diasConsecutivosCandidatosDisciplina1 = new ArrayList<>();
//                for (int j = 0; j < diasCandidatosDisciplina1NaoPossuiNaDisciplina2.size() - 1; j++) {
//                    Integer dia1 = diasCandidatosDisciplina1NaoPossuiNaDisciplina2.get(j);
//                    Integer dia2 = diasCandidatosDisciplina1NaoPossuiNaDisciplina2.get(j + 1);
//
//                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia1 = CandidatoUtils.getCandidatosNoDia(candidatosDaDisciplina1, dia1);
//                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia2 = CandidatoUtils.getCandidatosNoDia(candidatosDaDisciplina1, dia2);
//
//                    List<String> semestresDia1 = candidatosNoDia1.stream().map(CandidatoUtils::getSemestreCandidato).collect(Collectors.toList());
//                    List<String> semestresDia2 = candidatosNoDia2.stream().map(CandidatoUtils::getSemestreCandidato).collect(Collectors.toList());
//
//                    List<String> semestresIguais = new ArrayList<>();
//                    semestresDia1.stream().filter(semestresDia2::contains).forEach(semestresIguais::add);
//                    semestresDia2.stream().filter(semestresDia1::contains).forEach(semestresIguais::add);
//
//                    semestresIguais = semestresIguais.stream().distinct().collect(Collectors.toList());
//
//                    for (int k = 0; k < semestresIguais.size(); k++) {
//                        String turma = semestresIguais.get(k);
//                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia1NoSemestre = CandidatoUtils.getCandidatosNaTurma(candidatosNoDia1, turma);
//                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia2NoSemestre = CandidatoUtils.getCandidatosNaTurma(candidatosNoDia2, turma);
//                        if (dia1 + 1 == dia2 && !candidatosNoDia1NoSemestre.isEmpty() && !candidatosNoDia2NoSemestre.isEmpty()) {
//                            diasConsecutivosCandidatosDisciplina1.add(dia1);
//                            diasConsecutivosCandidatosDisciplina1.add(dia2);
//                            diasConsecutivosCandidatosDisciplina1 = diasConsecutivosCandidatosDisciplina1.stream().distinct().collect(Collectors.toList());
//                        }
//
//                        candidatosDaDisciplina1 = CandidatoUtils.getCandidatosNasTurmas(candidatosDaDisciplina1, semestresIguais);
//                    }
//                }

//                List<Integer> diasConsecutivosCandidatosDisciplina2 = new ArrayList<>();
//                for (int j = 0; j < diasCandidatosDisciplina2NaoPossuiNaDisciplina1.size() - 1; j++) {
//                    Integer dia1 = diasCandidatosDisciplina2NaoPossuiNaDisciplina1.get(j);
//                    Integer dia2 = diasCandidatosDisciplina2NaoPossuiNaDisciplina1.get(j + 1);
//
//                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia1 = CandidatoUtils.getCandidatosNoDia(candidatosDaDisciplina2, dia1);
//                    List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia2 = CandidatoUtils.getCandidatosNoDia(candidatosDaDisciplina2, dia2);
//
//                    List<String> semestresDia1 = candidatosNoDia1.stream().map(CandidatoUtils::getSemestreCandidato).collect(Collectors.toList());
//                    List<String> semestresDia2 = candidatosNoDia2.stream().map(CandidatoUtils::getSemestreCandidato).collect(Collectors.toList());
//
//                    List<String> semestresIguais = new ArrayList<>();
//                    semestresDia1.stream().filter(semestresDia2::contains).forEach(semestresIguais::add);
//                    semestresDia2.stream().filter(semestresDia1::contains).forEach(semestresIguais::add);
//
//                    semestresIguais = semestresIguais.stream().distinct().collect(Collectors.toList());
//
//                    for (int k = 0; k < semestresIguais.size(); k++) {
//                        String turma = semestresIguais.get(k);
//                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia1NoSemestre = CandidatoUtils.getCandidatosNaTurma(candidatosNoDia1, turma);
//                        List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatosNoDia2NoSemestre = CandidatoUtils.getCandidatosNaTurma(candidatosNoDia2, turma);
//
//                        if (dia1 + 1 == dia2 && !candidatosNoDia1NoSemestre.isEmpty() && !candidatosNoDia2NoSemestre.isEmpty()) {
//                            diasConsecutivosCandidatosDisciplina2.add(dia1);
//                            diasConsecutivosCandidatosDisciplina2.add(dia2);
//                            diasConsecutivosCandidatosDisciplina2 = diasConsecutivosCandidatosDisciplina2.stream().distinct().collect(Collectors.toList());
//                        }
//                    }
//
//                    candidatosDaDisciplina1 = CandidatoUtils.getCandidatosNasTurmas(candidatosDaDisciplina1, semestresIguais);
//                }


//                if (!diasDiferentes.isEmpty() && (!diasConsecutivosCandidatosDisciplina1.isEmpty() || !diasConsecutivosCandidatosDisciplina2.isEmpty())) {
                if (!diasDiferentes.isEmpty()) {
                    System.out.println("Disciplina 1: " + disciplina1);
                    GradeHorariaUtils.imprimirGradeHoraria(CandidatoUtils.getCandidatosNosDias(candidatos, diasCandidatosDisciplina1), quantidadeAulasPorTurno);
//                    GradeHorariaUtils.imprimirGradeHoraria(CandidatoUtils.getCandidatosNosDias(candidatosDaDisciplina1, diasConsecutivosCandidatosDisciplina1), quantidadeAulasPorTurno);

                    System.out.println("Disciplina 2: " + disciplina2);
                    GradeHorariaUtils.imprimirGradeHoraria(CandidatoUtils.getCandidatosNosDias(candidatos, diasCandidatosDisciplina2), quantidadeAulasPorTurno);
//                    GradeHorariaUtils.imprimirGradeHoraria(CandidatoUtils.getCandidatosNosDias(candidatosDaDisciplina2, diasConsecutivosCandidatosDisciplina2), quantidadeAulasPorTurno);
                    System.out.println();
                }
            }
        }
    }

    private static List<Candidato<Vertice<Integer>, EntidadeDTO>> getCandidatosParciaisDeUnicaDisciplinaComMaisDeUmDiaNaTurma
            (List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, Integer quantidadeAulasPorTurno) {
        Map<String, List<AulaDTO>> aulasCandidatosAgrupadosPorDisciplinas = CandidatoUtils.getAulasCandidatosAgrupadasPorDisciplina(candidatos);

        return candidatos.stream()
                .filter(candidato -> CandidatoUtils.ehCargaHorariaParcial(candidato, quantidadeAulasPorTurno) && CandidatoUtils.ehUnicaDisciplina(candidato))
                .filter(candidato -> CandidatoUtils.ehDisciplinaMaisDeUmDiaNaTurma(aulasCandidatosAgrupadosPorDisciplinas, candidato))
                .collect(Collectors.toList());
    }

    private static AulaDTO converterEntidadeEmAula(EntidadeDTO entidade) {
        return AulaDTO.builder()
                .disciplina(entidade.getNome())
                .turma(entidade.getSemestre())
                .professor(entidade.getProfessor())
                .cargaHoraria(entidade.getCargaHoraria())
                .build();
    }
}
