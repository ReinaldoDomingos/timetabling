package br.ufms.cpcx.gradehoraria.gradehoraria.service;

import br.ufms.cpcx.gradehoraria.arquivos.dto.GradeHorariaXLS;
import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.utils.DataUtils;
import br.ufms.cpcx.gradehoraria.generico.utils.StringUtils;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.business.AgrupadorCargaHorariaBO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.business.AgrupadorGradeHorariaBO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.DiaGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.GradeHorariaNovaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.TurmaGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.CabecalhoGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.DisciplinaGradeHorariaEdicaoDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.GradeHorariaAulasDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.filter.GradeHorariaFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CandidatoUtils;
import br.ufms.cpcx.gradehoraria.gradehoraria.utils.CargaHorariaUtils;
import br.ufms.cpcx.gradehoraria.grasp.conflitos.GeradorListaDeConflitos;
import br.ufms.cpcx.gradehoraria.grasp.conflitos.exception.ColunaInexistenteException;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.enumaration.EPeriodo;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.MelhorSolucaoDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.exception.NenhumaRestricaoFracaAtivaException;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Grafo;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;
import br.ufms.cpcx.gradehoraria.grasp.grasp.impl.GRASPImpl;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class GradeHorariaXLSService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GradeHorariaXLSService.class);

    @Autowired
    private AgrupadorGradeHorariaBO agrupadorGradeHorariaBO;

    @Autowired
    private AgrupadorCargaHorariaBO agrupadorCargaHorariaBO;

    @Autowired
    private RestricaoGradeHorariaService restricaoGradeHorariaService;

    public Object gerarXLS(Long idGradeHoraria, List<DisciplinaGradeHorariaEdicaoDTO> disciplinaGradeHorariaEdicaoDTOS, Integer quantidadeAulasPorTurno, GradeHorariaFilter filter) {
        long tempoIncial = this.logIniciar();

//        int maximoIteracoes = 1;
        int maximoIteracoes = 50;
//        int maximoIteracoes = 20;
        CabecalhoGradeHorariaDTO cabecalhoGradeHorariaDTO = criarCabecalhoGradeHorariaDTO(EPeriodo.UNICO, maximoIteracoes, quantidadeAulasPorTurno);

        List<String> disciplinasTabuladas = gerarDisciplinasTabuladas(cabecalhoGradeHorariaDTO, disciplinaGradeHorariaEdicaoDTOS);
        GeradorListaDeConflitos geradorListaDeConflitos = getGeradorListaDeConflitos(disciplinasTabuladas, cabecalhoGradeHorariaDTO, quantidadeAulasPorTurno);

        List<EntidadeDTO> entidades = geradorListaDeConflitos.getEntidades();

        List<String> disciplinasNoMesmoDia = asList("Algoritmos e Programação I", "Algoritmos e Programação II", "Empreendedorismo", "Pré Cálculo");

        List<RestricaoGradeHoraria> restricoesGradeHorria = restricaoGradeHorariaService.buscarPorGradeHorariaId(idGradeHoraria);

        GradeHorariaNovaDTO gradeHorariaNovaDTO = null;
        if (filter.getRestricoes().contains(ETipoRestricao.DIAS_NAO_CONSECUTIVOS)) {
            gradeHorariaNovaDTO = agrupadorCargaHorariaBO.gerarGradeHoraria(geradorListaDeConflitos.getQuantidadeAulasPorTurno(), entidades, disciplinasNoMesmoDia);

//        GradeHorariaNovaDTO gradeHorariaNovaDTO = agrupadorGradeHorariaBO.gerarGradeHoraria(geradorListaDeConflitos.getQuantidadeAulasPorTurno(), entidades);

//        System.out.println("Gerado com " + gradeHorariaNovaDTO.getDias().size() + " dias");//NOSONAR
        }


        GRASPImpl grasp = configurarGRASP(cabecalhoGradeHorariaDTO, entidades, restricoesGradeHorria, disciplinasNoMesmoDia);
        if (filter.getRestricoes().contains(ETipoRestricao.DIAS_NAO_CONSECUTIVOS)) {
            grasp.setPossuiEntradaInicialJaColorida(true);
            grasp.setGradeHorariaNovaDTO(gradeHorariaNovaDTO);
        }

        MelhorSolucaoDTO<Grafo<Integer, Integer>> melhorSolucaoDTO = grasp.execute(cabecalhoGradeHorariaDTO.getMaximoIteracoes());

        this.logPosicaoAtual(tempoIncial, "Geração finalizada");

        GradeHorariaAulasDTO gradeHorariaAulasDTO = criarGradeHorariaAulasDTO(cabecalhoGradeHorariaDTO, melhorSolucaoDTO);

        GradeHorariaXLS gradeHorariaXLS = new GradeHorariaXLS();

        gerarGradeHorariaXLS(gradeHorariaXLS, gradeHorariaAulasDTO, melhorSolucaoDTO.getSolucao().getCores().size());

        verificarProfessoresQueExcederamCargaHorariaDiaria(melhorSolucaoDTO.getCandidatos(), quantidadeAulasPorTurno);

//        GradeHorariaXLS gradeHorariaXLS = getGradeHorariaXLSNovoFormato(quantidadeAulasPorTurno, geradorListaDeConflitos, entidades, gradeHorariaNovaDTO);


        LOGGER.info(String.valueOf(CargaHorariaUtils.getCargaHorariaEntidades(entidades)));

        return gradeHorariaXLS.exportarBase64();
    }

    private void verificarProfessoresQueExcederamCargaHorariaDiaria(List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos, Integer quantidadeAulasPorTurno) {
        Map<Integer, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosPorDias = candidatos.stream()
                .filter(Candidato::possuiAulas)
                .filter(Candidato::estaAtivo)
                .collect(Collectors.groupingBy(vertice -> vertice.getValor().getCor()));

        candidatosPorDias.forEach((dia, candidatosDia) -> {
            Map<String, List<Candidato<Vertice<Integer>, EntidadeDTO>>> candidatosPorProfessor = candidatosDia.stream().collect(Collectors.groupingBy(candidato -> candidato.getEntidade().getProfessores().get(0)));

            candidatosPorProfessor.forEach((professor, candidatosProfessor) -> {
                int cargaHorariaProfessorNodia = CargaHorariaUtils.getCargaHorariaProfessorCandidatos(candidatosProfessor, professor);
                if (cargaHorariaProfessorNodia > quantidadeAulasPorTurno) {
                    LOGGER.info("Professor " + professor + " com " + cargaHorariaProfessorNodia + " aulas dia " + dia);
                }
            });
        });
    }

    private GradeHorariaXLS getGradeHorariaXLSNovoFormato(Integer quantidadeAulasPorTurno, GeradorListaDeConflitos geradorListaDeConflitos, List<EntidadeDTO> entidades, GradeHorariaNovaDTO gradeHorariaNovaDTO) {
        List<Integer> dias = entidades.stream().map(EntidadeDTO::getDiaInicial).distinct().sorted().collect(Collectors.toList());

        List<List<String>> aulasTurmasPorDia = new ArrayList<>();
        List<List<String>> professoresTurmasPorDia = new ArrayList<>();
        List<String> turmas = getTurmasEntidades(entidades);


        Map<String, List<Integer>> posicoesTurmasGradeHoraria = new HashMap<>();

        if (!gradeHorariaNovaDTO.getDias().isEmpty()) {
            DiaGradeHorariaDTO diaGradeHorariaDTO = gradeHorariaNovaDTO.getDias().get(0);
            for (int turma = 0; turma < turmas.size(); turma++) {
                TurmaGradeHorariaDTO turmaGradeHorariaDTO = diaGradeHorariaDTO.getTurma(turmas.get(turma));

                List<Integer> posicoesTurmaGradeHoraria = new ArrayList<>();

                if (nonNull(posicoesTurmasGradeHoraria.get(turmaGradeHorariaDTO.getTurma()))) {
                    posicoesTurmaGradeHoraria = posicoesTurmasGradeHoraria.get(turmaGradeHorariaDTO.getTurma());
                }

                for (int aulaTurma = 0; aulaTurma < geradorListaDeConflitos.getQuantidadeAulasPorTurno(); aulaTurma++) {
                    posicoesTurmaGradeHoraria.add(turma * quantidadeAulasPorTurno + aulaTurma);

                    List<String> aulaTurmaNoDia = null;
                    List<String> professorTurmaNoDia = null;

                    aulaTurmaNoDia = new ArrayList<>(Collections.singletonList(turmaGradeHorariaDTO.getTurma()));
                    professorTurmaNoDia = new ArrayList<>(Collections.singletonList(turmaGradeHorariaDTO.getTurma()));

                    aulasTurmasPorDia.add(aulaTurmaNoDia);
                    professoresTurmasPorDia.add(professorTurmaNoDia);
                }

                posicoesTurmasGradeHoraria.put(turmaGradeHorariaDTO.getTurma(), posicoesTurmaGradeHoraria);
            }
        }

        for (int dia = 0; dia < gradeHorariaNovaDTO.getDias().size(); dia++) {
            DiaGradeHorariaDTO diaGradeHorariaDTO = gradeHorariaNovaDTO.getDias().get(dia);
            for (int turma = 0; turma < turmas.size(); turma++) {
                TurmaGradeHorariaDTO turmaGradeHorariaDTO = diaGradeHorariaDTO.getTurma(turmas.get(turma));

                List<AulaDTO> aulasTurmaNoDia = nonNull(turmaGradeHorariaDTO) ? turmaGradeHorariaDTO.getAulasProfessores() : null;
                List<Integer> posicoesTurma = posicoesTurmasGradeHoraria.get(turmas.get(turma));

                for (int aulaTurma = 0; aulaTurma < geradorListaDeConflitos.getQuantidadeAulasPorTurno(); aulaTurma++) {
                    List<String> aulaTurmaNoDia = null;
                    List<String> professorTurmaNoDia = null;

                    aulaTurmaNoDia = new ArrayList<>();
                    professorTurmaNoDia = new ArrayList<>();

                    int cargaHoraria = 1;
                    if (nonNull(aulasTurmaNoDia) && aulaTurma < aulasTurmaNoDia.size() && aulaTurma < posicoesTurma.size()) {
                        cargaHoraria = aulasTurmaNoDia.get(aulaTurma).getCargaHoraria();
                        aulaTurmaNoDia.add(getHorarioAulaParaImpressao(aulasTurmaNoDia.get(aulaTurma)));
                        professorTurmaNoDia.add(getAulaProfessorParaImpressao(aulasTurmaNoDia.get(aulaTurma)));
//                    }
                    } else {
                        aulaTurmaNoDia.add("");
                        professorTurmaNoDia.add("");
                    }

//                    for (int i = 0; i < cargaHoraria; i++) {
                    aulasTurmasPorDia.get(posicoesTurma.get(aulaTurma)).addAll(aulaTurmaNoDia);
                    professoresTurmasPorDia.get(posicoesTurma.get(aulaTurma)).addAll(professorTurmaNoDia);
//                        aulaTurma++;
//                    }

                }
            }
        }

        List<String> cabecalho = new ArrayList<>(Collections.singletonList("Semestre"));
        cabecalho.addAll(DataUtils.getDiasDaSemana(dias.size()));

        aulasTurmasPorDia.add(0, cabecalho);
        professoresTurmasPorDia.add(0, cabecalho);

        GradeHorariaXLS gradeHorariaXLS = new GradeHorariaXLS();

        gerarGradeHorariaXLS(gradeHorariaXLS, aulasTurmasPorDia, turmas, dias.size());

        gradeHorariaXLS.adicionarPagina();
        gradeHorariaXLS.setPaginaAtual(1);

        gerarGradeHorariaXLS(gradeHorariaXLS, professoresTurmasPorDia, turmas, dias.size());

        return gradeHorariaXLS;
    }

    private String getHorarioAulaParaImpressao(AulaDTO aula) {
        return aula.getDisciplina() + ", " + aula.getProfessor() + ", " + aula.getCargaHoraria();
    }

    private String getAulaProfessorParaImpressao(AulaDTO aula) {
        return aula.getProfessor() + ", " + aula.getCargaHoraria();
    }

    private GradeHorariaAulasDTO criarGradeHorariaAulasDTO(CabecalhoGradeHorariaDTO cabecalhoGradeHorariaDTO, MelhorSolucaoDTO<Grafo<Integer, Integer>> melhorSolucaoDTO) {
        Grafo<Integer, Integer> solucao = melhorSolucaoDTO.getSolucao();
        List<EntidadeDTO> entidades = CandidatoUtils.getEntidadesCanditatos(melhorSolucaoDTO.getCandidatos());

        List<List<AulaDTO>> aulasDiaPorEntidade = getAulasDiaPorEntidade(entidades, solucao);

        GradeHorariaAulasDTO gradeHorariaAulasDTO = new GradeHorariaAulasDTO();
        gradeHorariaAulasDTO.setDias(DataUtils.getDiasDaSemana(solucao.getCores().size()));
        gradeHorariaAulasDTO.setAulas(aulasDiaPorEntidade);
        gradeHorariaAulasDTO.setTurmas(getTurmasEntidades(entidades));
        gradeHorariaAulasDTO.setProfessores(getProfessoresEntidades(entidades));
        gradeHorariaAulasDTO.setQuantidadeAulasPorTurno(cabecalhoGradeHorariaDTO.getQuantidadeAulasPorTurno());

        return gradeHorariaAulasDTO;
    }

    private void gerarGradeHorariaXLS(GradeHorariaXLS gradeHorariaXLS, GradeHorariaAulasDTO gradeHorariaAulasDTO, int numeroCores) {
        List<List<String>> horario = gradeHorariaAulasDTO.getHorario();

        gerarGradeHorariaXLS(gradeHorariaXLS, horario, gradeHorariaAulasDTO.getTurmas(), numeroCores);

        gradeHorariaXLS.adicionarPagina();
        gradeHorariaXLS.setPaginaAtual(1);

        List<List<String>> horarioProfessores = gradeHorariaAulasDTO.getHorario(true);

        gerarGradeHorariaXLS(gradeHorariaXLS, horarioProfessores, gradeHorariaAulasDTO.getTurmas(), numeroCores);
    }

    private void gerarGradeHorariaXLS(GradeHorariaXLS gradeHorariaXLS, List<List<String>> horario, List<String> turmas, int numeroCores) {
        gradeHorariaXLS.adicionarLinhaCabecalho(0, horario.get(0));

        Map<String, Integer> coresTurmas = gerarMapCoresTurmas(turmas);

        String turmaAnterior = null;
        HSSFCellStyle estilo = null;
        for (int linha = 1; linha < horario.size(); linha++) {
            List<String> valoresColunas = horario.get(linha);

            if (isNull(turmaAnterior) || !valoresColunas.get(0).equals(turmaAnterior)) {
                turmaAnterior = valoresColunas.get(0);

                short corTurma = (short) ((int) coresTurmas.get(turmaAnterior));

                estilo = gradeHorariaXLS.gerarEstiloColorido(corTurma);
            }

            HSSFRow linhaPlanilha = gradeHorariaXLS.adicionarLinha(linha, valoresColunas, estilo);

            for (int i = valoresColunas.size(); i < numeroCores + 1; i++) {
                gradeHorariaXLS.adicionarColuna(i, linhaPlanilha, "", estilo);
            }
        }
    }

    private List<String> getTurmasEntidades(List<EntidadeDTO> entidades) {
        return entidades.stream().map(EntidadeDTO::getSemestre).distinct().sorted().collect(Collectors.toList());
    }

    private List<String> getProfessoresEntidades(List<EntidadeDTO> entidades) {
        List<String> professores = new ArrayList<>();

        entidades.forEach(entidade -> professores.addAll(getProfessoresAulas(entidade)));

        return professores.stream().distinct().collect(Collectors.toList());
    }

    private List<String> getProfessoresAulas(EntidadeDTO entidade) {
        return entidade.getAulas().stream().map(AulaDTO::getProfessor).collect(Collectors.toList());
    }

    private Map<String, Integer> gerarMapCoresTurmas(List<String> turmas) {
        List<Integer> cores = asList(1710, 2823, 1200, 1323, 1522);

        Map<String, Integer> coresTurmas = new HashMap<>();
        for (int i = 0; i < turmas.size(); i++) {
            String turma = turmas.get(i);

            coresTurmas.put(turma, cores.get(i % cores.size()));
        }

        return coresTurmas;
    }

    private List<List<AulaDTO>> getAulasDiaPorEntidade(List<EntidadeDTO> entidades, Grafo<Integer, Integer> solucao) {
        Map<Integer, List<Vertice<Integer>>> verticesAgrupadoPorCor = solucao.getListaDeVertices().stream()
                .collect(Collectors.groupingBy(Vertice::getCor));

        return getAulasDiaPorEntidade(entidades, verticesAgrupadoPorCor);
    }

    private List<List<AulaDTO>> getAulasDiaPorEntidade(List<EntidadeDTO> entidades, Map<Integer, List<Vertice<Integer>>> verticesAgrupadoPorCor) {
        AtomicInteger cargaHorariaTotal = new AtomicInteger();
        List<List<AulaDTO>> aulasDiaPorEntidade = new ArrayList<>();
        verticesAgrupadoPorCor.forEach((cor, verticesNaCor) ->
                        verticesNaCor.forEach(vertice -> {
                            EntidadeDTO entidade = entidades.get(vertice.getRotulo() - 1);
                            List<AulaDTO> copias = new ArrayList<>();

//                    if(!entidade.getAulas().isEmpty()) {
                            entidade.getAulas().forEach(aula -> {
                                aula.setDia(DataUtils.getDiaDaSemana(cor - 1));
                                for (int i = 0; i < aula.getCargaHoraria(); i++) {
                                    copias.add(aula);
                                }
                                cargaHorariaTotal.addAndGet(aula.getCargaHoraria());
                            });
                            entidade.getAulas().sort(Comparator.comparing(AulaDTO::getDisciplina));
                            aulasDiaPorEntidade.add(copias);
//                    }else {
//                        LOGGER.info("Entidade sem aula");
//                    }

                        })
        );

        int cargaHoraria = cargaHorariaTotal.get();
        LOGGER.info(String.valueOf(cargaHoraria));

        return aulasDiaPorEntidade;
    }

    private GRASPImpl configurarGRASP(CabecalhoGradeHorariaDTO cabecalhoGradeHorariaDTO,
                                      List<EntidadeDTO> entidades,
                                      List<RestricaoGradeHoraria> restricoesGradeHorria, List<String> disciplinasNoMesmoDia) throws NenhumaRestricaoFracaAtivaException {

        if (entidades.isEmpty()) {
            throw new GenericException("Adicione pelo menos uma disciplina.");
        }

        EPeriodo periodo = cabecalhoGradeHorariaDTO.getPeriodo();
        String colunaProfessor = cabecalhoGradeHorariaDTO.getColunaProfessor();
        int tamanhoListaRestritaDeCandidatos = (int) (entidades.size() * 0.4) + 2;

        GRASPImpl grasp = new GRASPImpl(periodo, tamanhoListaRestritaDeCandidatos, colunaProfessor);
        grasp.setQuantidadeAulasPorTurno(cabecalhoGradeHorariaDTO.getQuantidadeAulasPorTurno());

        grasp.setColunasRestricoesFortes(cabecalhoGradeHorariaDTO.getColunasRestricoesForte());
        grasp.setCandidatos(entidades);
        grasp.setColunaLocal("Laboratório");

        List<String> disciplinasAlternarDia = asList("Algoritmos e Programação I", "Algoritmos e Programação II", "Pré Cálculo");

        grasp.ativarRestricao(ETipoRestricao.DIAS_NAO_CONSECUTIVOS, disciplinasAlternarDia);
//        grasp.ativarRestricao(ETipoRestricao.LOCAL_DISPONIVEL);
        grasp.ativarRestricao(ETipoRestricao.MESMO_DIA, disciplinasNoMesmoDia);
        grasp.ativarRestricao(restricoesGradeHorria);

        return grasp;
    }

    private GeradorListaDeConflitos getGeradorListaDeConflitos(List<String> disciplinasTabuladas, CabecalhoGradeHorariaDTO cabecalhoGradeHorariaDTO, Integer quantidadeAulasPorTurno) throws ColunaInexistenteException {
        GeradorListaDeConflitos geradorListaDeConflitos = new GeradorListaDeConflitos(cabecalhoGradeHorariaDTO.getColunaDisciplina());

        geradorListaDeConflitos.setColunaSemestre(cabecalhoGradeHorariaDTO.getColunaSemestre());
        geradorListaDeConflitos.setColunaCargaHoraria(cabecalhoGradeHorariaDTO.getColunaCargaHoraria());
        geradorListaDeConflitos.setColunaIdentificador(cabecalhoGradeHorariaDTO.getColunaIdentificador());
        geradorListaDeConflitos.setColunaProfessor(cabecalhoGradeHorariaDTO.getColunaProfessor());
        geradorListaDeConflitos.setQuantidadeAulasPorTurno(quantidadeAulasPorTurno);

        geradorListaDeConflitos.lerRegistrosTabulados(disciplinasTabuladas, Collections.singletonList(cabecalhoGradeHorariaDTO.getColunaProfessor()));

        geradorListaDeConflitos.adicionarRestricaoColuna(cabecalhoGradeHorariaDTO.getColunaSemestre());
        geradorListaDeConflitos.adicionarRestricaoColuna(cabecalhoGradeHorariaDTO.getColunaProfessor());

        return geradorListaDeConflitos;
    }

    private CabecalhoGradeHorariaDTO criarCabecalhoGradeHorariaDTO(EPeriodo periodo, int maximoIteracoes, int quantidadeAulasPorTurno) {
        CabecalhoGradeHorariaDTO cabecalhoGradeHorariaDTO = new CabecalhoGradeHorariaDTO();

        cabecalhoGradeHorariaDTO.setPeriodo(periodo);
        cabecalhoGradeHorariaDTO.setMaximoIteracoes(maximoIteracoes);
        cabecalhoGradeHorariaDTO.setQuantidadeAulasPorTurno(quantidadeAulasPorTurno);

        cabecalhoGradeHorariaDTO.setColunaCargaHoraria("CHS");
        cabecalhoGradeHorariaDTO.setColunaSemestre("Semestre");
        cabecalhoGradeHorariaDTO.setColunaProfessor("Professor");
        cabecalhoGradeHorariaDTO.setColunaIdentificador("Numero");
        cabecalhoGradeHorariaDTO.setColunaDisciplina("Disciplina");
        cabecalhoGradeHorariaDTO.setColunaLaboratorio("Laboratório");

        return cabecalhoGradeHorariaDTO;
    }

    private List<String> gerarDisciplinasTabuladas(CabecalhoGradeHorariaDTO cabecalhoGradeHorariaDTO, List<DisciplinaGradeHorariaEdicaoDTO> disciplinaGradeHorariaEdicaoDTOS) {
        AtomicInteger contador = new AtomicInteger();
        List<String> disciplinasTabuladas = disciplinaGradeHorariaEdicaoDTOS.stream()
                .map(disciplinaDTO -> gerarAulaDisciplinaTabuladaString(contador, disciplinaDTO))
                .collect(Collectors.toList());

        String cabecalho = StringUtils.getTextoDaLista(cabecalhoGradeHorariaDTO.getColunas()).replace(",", "\t");
        disciplinasTabuladas.add(0, cabecalho);

        return disciplinasTabuladas;
    }

    private String gerarAulaDisciplinaTabuladaString(AtomicInteger numero, DisciplinaGradeHorariaEdicaoDTO disciplinaGradeHorariaEdicaoDTO) {
        validarProfessorSelecionado(disciplinaGradeHorariaEdicaoDTO);
        validarTurmaSelecionada(disciplinaGradeHorariaEdicaoDTO);

        boolean usaLaboratorio = Boolean.TRUE.equals(disciplinaGradeHorariaEdicaoDTO.getUsaLaboratorio());

        return numero.incrementAndGet() + "\t" + disciplinaGradeHorariaEdicaoDTO.getNome() + "\t"
                + disciplinaGradeHorariaEdicaoDTO.getProfessor().getNome() + "\t"
                + disciplinaGradeHorariaEdicaoDTO.getTurma().getNome() + "\t"
                + disciplinaGradeHorariaEdicaoDTO.getCargaHorariaSemanal() + "\t"
                + (usaLaboratorio ? "Sim" : "Não");
    }

    private void validarTurmaSelecionada(DisciplinaGradeHorariaEdicaoDTO disciplinaGradeHorariaEdicaoDTO) {
        if (isNull(disciplinaGradeHorariaEdicaoDTO.getTurma())) {
            throw new GenericException("Possui disciplina(s) em que não selecionou uma turma.");
        }
    }

    private void validarProfessorSelecionado(DisciplinaGradeHorariaEdicaoDTO disciplinaGradeHorariaEdicaoDTO) {
        if (isNull(disciplinaGradeHorariaEdicaoDTO.getProfessor())) {
            throw new GenericException("Possui disciplina(s) em que não selecionou um professor.");
        }
    }

    private long logIniciar() {
        long tempoInicial = System.currentTimeMillis();

        String mensagemLog = "Geração da Grade Horária iniciada: " +
                "Inicio -> '" + tempoInicial + "'. ";

        LOGGER.info(mensagemLog);

        return tempoInicial;
    }

    public void logPosicaoAtual(Long tempoInicial, String posicaoAtual) {
        long tempoFinal = System.currentTimeMillis();

        String mensagemLog = "Geração da Grade Horária finalizada: " +
                "Inicio -> '" + tempoInicial + "'. " +
                "Posição atual -> '" + posicaoAtual + "'. " +
                "Tempo Atual ->  " + ((tempoFinal - tempoInicial) / 1000) + "s.";

        LOGGER.info(mensagemLog);
    }
}
