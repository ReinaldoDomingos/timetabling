package br.ufms.cpcx.gradehoraria.generico.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

public class DataUtils {
    private DataUtils() {
    }

    public static List<String> getDiasDaSemanaIntegral() {
        return asList("Segunda de manhã", "Segunda á tarde", "Terça-feira de manhã", "Terça-feira á tarde",
                "Quarta-feira de manhã", "Quarta-feira á tarde", "Quinta-feira de manhã", "Quinta-feira á tarde",
                "Sexta-feira de manhã", "Sexta-feira á tarde", "Sábado  de manhã", "Sábado á tarde",
                "Domingo  de manhã", "Domingo á tarde"
        );
    }

    public static String getDiaDaSemana(int posDia) {
        return posDia < getDiasDaSemana().size() ? getDiasDaSemana().get(posDia) : String.valueOf((posDia + 1));
    }

    public static List<String> getDiasDaSemana(int quantidadeDias) {
        LinkedList<String> diasDaSemana = new LinkedList<>(getDiasDaSemana());

        while (diasDaSemana.size() > quantidadeDias) {
            diasDaSemana.pollLast();
        }

        for (int i = diasDaSemana.size() + 1; i <= quantidadeDias; i++) {
            diasDaSemana.add(String.valueOf(i));
        }

        return diasDaSemana;
    }

    public static List<String> getDiasDaSemana() {
        return asList("Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira",
                "Sexta-feira", "Sábado", "Domingo"
        );
    }

    public static List<String> getDiasDaSemana(List<Integer> dias) {
        List<String> diasSemanas = new ArrayList<>();

        List<String> diasDaSemanaPadrao = getDiasDaSemana();

        for (Integer dia : dias) {
            if (dia <= diasDaSemanaPadrao.size()) {
                diasSemanas.add(diasDaSemanaPadrao.get(dia - 1));
            } else {
                diasSemanas.add(String.valueOf(dia));
            }
        }

        return diasSemanas;
    }
}