package br.ufms.cpcx.gradehoraria.gradehoraria.service;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.filter.GenericFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.DisciplinaGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.DisciplinaGradeHorariaEdicaoDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.ProfessorDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Disciplina;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.DisciplinaGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.GradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Turma;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.DisciplinaGradeHorariaRepository;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.DisciplinaRepository;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.GradeHorariaRepository;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.ProfessorRepository;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.TurmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
public class DisciplinaGradeHorariaService {

    @Autowired
    private DisciplinaGradeHorariaRepository disciplinaGradeHorariaRepository;

    @Autowired
    private ProfessorRepository professorRepository;

    @Autowired
    private GradeHorariaRepository gradeHorariaRepository;

    @Autowired
    private DisciplinaRepository disciplinaRepository;

    @Autowired
    private TurmaRepository turmaRepository;

    public DisciplinaGradeHorariaEdicaoDTO salvar(Long idGradeHoraria, Long idDisciplina) {
        return salvarCompleta(idGradeHoraria, idDisciplina, null, null, null);
    }

    public DisciplinaGradeHorariaEdicaoDTO salvarCompleta(Long idGradeHoraria, Long idDisciplina, Long idProfessor, Long idTurma, Integer cargaHorariaSemanal) {
        Turma turma = nonNull(idTurma) ? turmaRepository.getOne(idTurma) : null;
        Professor professor = nonNull(idProfessor) ? professorRepository.getOne(idProfessor) : null;
        Disciplina disciplina = disciplinaRepository.getOne(idDisciplina);
        GradeHoraria gradeHoraria = gradeHorariaRepository.getOne(idGradeHoraria);

        DisciplinaGradeHoraria disciplinaGradeHoraria = new DisciplinaGradeHoraria();
        disciplinaGradeHoraria.setTurma(turma);
        disciplinaGradeHoraria.setProfessor(professor);
        disciplinaGradeHoraria.setDisciplina(disciplina);
        disciplinaGradeHoraria.setGradeHoraria(gradeHoraria);
        disciplinaGradeHoraria.setCargaHorariaSemanal(cargaHorariaSemanal);

        return new DisciplinaGradeHorariaEdicaoDTO(salvarDisciplinaGradeHoraria(disciplinaGradeHoraria));
    }

    private DisciplinaGradeHoraria salvarDisciplinaGradeHoraria(DisciplinaGradeHoraria disciplinaGradeHoraria) {
        return disciplinaGradeHorariaRepository.save(disciplinaGradeHoraria);
    }

    public List<DisciplinaGradeHorariaDTO> buscarTodas() {
        return disciplinaGradeHorariaRepository.buscarTodos()
                .stream().map(DisciplinaGradeHorariaDTO::new)
                .collect(Collectors.toList());
    }

    public Page<DisciplinaGradeHorariaEdicaoDTO> buscarPorGradeHorariaId(GenericFilter filter, Long idGradeHoraria) {
        PageRequest pageRequest = filter.getPageRequest();

        List<DisciplinaGradeHorariaEdicaoDTO> disciplinaGradeHorariaEdicaoDTOS = buscarTodasPorGradeHorariaId(idGradeHoraria).stream()
                .sorted(Comparator.comparing(DisciplinaGradeHorariaEdicaoDTO::getIsIncompleta).reversed()).collect(Collectors.toList());

        Sort sort = pageRequest.getSort().and(Sort.by(Sort.Direction.ASC, "isIncompleta"));

        PageRequest pageRequestComSort = PageRequest.of(pageRequest.getPageNumber(), pageRequest.getPageSize(), sort);

        PagedListHolder<DisciplinaGradeHorariaEdicaoDTO> page = new PagedListHolder<>(disciplinaGradeHorariaEdicaoDTOS);
        page.setPageSize(pageRequestComSort.getPageSize());
        page.setPage(pageRequestComSort.getPageNumber());

        return new PageImpl<>(page.getPageList(), pageRequestComSort, disciplinaGradeHorariaEdicaoDTOS.size());
    }

    public List<DisciplinaGradeHorariaEdicaoDTO> buscarTodasPorGradeHorariaId(Long idGradeHoraria) {
        return disciplinaGradeHorariaRepository.buscarPorGradeHorariaId(idGradeHoraria);
    }

    public List<ProfessorDTO> buscarProfessoresPorGradeHorariaId(Long idGradeHoraria) {
        List<DisciplinaGradeHorariaEdicaoDTO> disciplinaGradeHorariaEdicaoDTOS = disciplinaGradeHorariaRepository.buscarPorGradeHorariaId(idGradeHoraria)
                .stream().filter(disciplinaGradeHorariaEdicaoDTO -> nonNull(disciplinaGradeHorariaEdicaoDTO.getProfessor()))
                .collect(Collectors.toList());

        return new ArrayList<>(disciplinaGradeHorariaEdicaoDTOS.stream()
                .collect(Collectors.groupingBy(DisciplinaGradeHorariaEdicaoDTO::getProfessor))
                .keySet());
    }

    public DisciplinaGradeHorariaDTO buscarPorId(Long id) {
        return disciplinaGradeHorariaRepository.findById(id).map(DisciplinaGradeHorariaDTO::new).orElse(null);
    }

    public void deletar(Long id) {
        disciplinaGradeHorariaRepository.deleteById(id);
    }

    public DisciplinaGradeHorariaEdicaoDTO alterar(Long id, DisciplinaGradeHorariaEdicaoDTO disciplinaGradeHorariaEdicaoDTO) {
        validarDisciplinaGradeHoraria(id, disciplinaGradeHorariaEdicaoDTO);

        DisciplinaGradeHoraria disciplinaGradeHoraria = disciplinaGradeHorariaRepository.getOne(id);
        disciplinaGradeHoraria.setCargaHorariaSemanal(disciplinaGradeHorariaEdicaoDTO.getCargaHorariaSemanal());

        setProfessorDisciplinaGradeHoraria(disciplinaGradeHorariaEdicaoDTO, disciplinaGradeHoraria);
        setTurmaDisciplinaGradeHoraria(disciplinaGradeHorariaEdicaoDTO, disciplinaGradeHoraria);

        return new DisciplinaGradeHorariaEdicaoDTO(salvarDisciplinaGradeHoraria(disciplinaGradeHoraria));
    }

    public int buscarTotalCargaHorariasDisciplinasPorGradeHorariaId(Long idGradeHoraria) {
        Integer totalCargaHoraria = disciplinaGradeHorariaRepository.buscarTotalCargaHorariasDisciplinasPorGradeHorariaId(idGradeHoraria);
        return nonNull(totalCargaHoraria) ? totalCargaHoraria : 0;
    }

    private void setTurmaDisciplinaGradeHoraria(DisciplinaGradeHorariaEdicaoDTO disciplinaGradeHorariaEdicaoDTO, DisciplinaGradeHoraria disciplinaGradeHoraria) {
        Long idTurma = nonNull(disciplinaGradeHorariaEdicaoDTO.getTurma()) ? disciplinaGradeHorariaEdicaoDTO.getTurma().getId() : null;
        if (nonNull(idTurma)) {
            Turma turma = turmaRepository.getOne(idTurma);
            disciplinaGradeHoraria.setTurma(turma);
        }
    }

    private void setProfessorDisciplinaGradeHoraria(DisciplinaGradeHorariaEdicaoDTO disciplinaGradeHorariaEdicaoDTO, DisciplinaGradeHoraria disciplinaGradeHoraria) {
        Long idProfessor = nonNull(disciplinaGradeHorariaEdicaoDTO.getProfessor()) ? disciplinaGradeHorariaEdicaoDTO.getProfessor().getId() : null;
        if (nonNull(idProfessor)) {
            Professor professor = professorRepository.getOne(idProfessor);
            disciplinaGradeHoraria.setProfessor(professor);
        }
    }

    private void validarDisciplinaGradeHoraria(Long id, DisciplinaGradeHorariaEdicaoDTO disciplinaGradeHorariaEdicaoDTO) {
        if (!id.equals(disciplinaGradeHorariaEdicaoDTO.getIdDisciplinaGradeHoraria())) {
            throw new GenericException("Erro ao atualizar o disciplina grade horária.");
        }
    }
}
