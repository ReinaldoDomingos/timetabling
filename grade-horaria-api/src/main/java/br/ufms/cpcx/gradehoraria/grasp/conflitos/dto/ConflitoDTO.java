package br.ufms.cpcx.gradehoraria.grasp.conflitos.dto;

import br.ufms.cpcx.gradehoraria.generico.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ConflitoDTO {
    private final String valor;
    private final List<String> listaDeEntidadesComConflitos;

    public ConflitoDTO(String valor) {
        this.valor = valor;
        this.listaDeEntidadesComConflitos = new ArrayList<>();
    }

    public String getValor() {
        return this.valor;
    }

    public List<String> getValores() {
        return StringUtils.getListaDoTexto(this.valor);
    }

    public List<String> getListaDeEntidadesComConflitos() {
        return this.listaDeEntidadesComConflitos;
    }

    public void adicionarEntidadeComConflito(String numero) {
        if (!this.listaDeEntidadesComConflitos.contains(numero)) {
            this.listaDeEntidadesComConflitos.add(numero);
        }
    }

    @Override
    public String toString() {
        return "Entidade " + this.valor + ": conflitos=" + this.listaDeEntidadesComConflitos;
    }
}
