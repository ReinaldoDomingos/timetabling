package br.ufms.cpcx.gradehoraria.gradehoraria.enumaration;

public enum ESemestre {
    PRIMEIRO_SEMESTRE,
    SEGUNDO_SEMESTRE
}
