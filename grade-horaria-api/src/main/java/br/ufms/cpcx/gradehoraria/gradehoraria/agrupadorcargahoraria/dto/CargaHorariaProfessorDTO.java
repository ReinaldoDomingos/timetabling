package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto;

import lombok.Data;

@Data
public class CargaHorariaProfessorDTO {
    private Integer cargaHoraria;
    private String professor;

    public CargaHorariaProfessorDTO(String professor, Integer cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
        this.professor = professor;
    }
}
