package br.ufms.cpcx.gradehoraria.gradehoraria.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "TB_DISCIPLINA")
@SequenceGenerator(name = "SEQ_DISCIPLINA", sequenceName = "SEQ_DESTINACAO")
public class Disciplina {
    @Id
    @Column(name = "DIS_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DIS_NOME", length = 100)
    private String nome;

    @Column(name = "DIS_CODIGO", length = 14)
    private String codigo;

    @Column(name = "DIS_CARGA_HORARIA", length = 14)
    private Long cargaHoraria;

    @Column(name = "GRA_TESTE_ID")
    private Long gradeHorariaTesteId;
}
