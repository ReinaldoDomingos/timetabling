package br.ufms.cpcx.gradehoraria.grasp.gradehoraria.utils;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.enumaration.EPeriodo;

public class PeriodoUltils {
    private PeriodoUltils() {
    }

    public static boolean ehPeriodoUnico(EPeriodo periodo) {
        return EPeriodo.UNICO.equals(periodo);
    }
}
