package br.ufms.cpcx.gradehoraria.grasp.grasp.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class MelhorSolucaoDTO<E> {
    private E solucao;

    private List<Integer> cores;

    private List<Integer> avaliacoesDias;

    private List<Integer> avaliacoesTurmas;

    private List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos;

    @Override
    public String toString() {
        return "numeroCores=" + this.cores.size() +
                ", avaliacoesNegativasDias=" + contarAvalicoesNegativas(avaliacoesDias) +
                ", avaliacoesNegativasTurmas=" + contarAvalicoesNegativas(avaliacoesTurmas);
    }

    private long contarAvalicoesNegativas(List<Integer> avalicaoes) {
        return avalicaoes.stream().filter(avaliacao -> avaliacao < 0).count();
    }
}
