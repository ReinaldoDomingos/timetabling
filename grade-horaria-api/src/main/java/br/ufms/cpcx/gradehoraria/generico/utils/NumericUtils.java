package br.ufms.cpcx.gradehoraria.generico.utils;

import java.util.Random;

import static java.util.Objects.nonNull;

public class NumericUtils {
    private NumericUtils() {
    }

    public static double getDouble(int valor) {
        return Double.parseDouble(String.valueOf(valor));
    }

    public static int getInteiroOuZero(Integer valor) {
        return nonNull(valor) ? valor : 0;
    }

    public static int sort(int maximo) {
        Random random = new Random();
        return random.nextInt(maximo);
    }
}
