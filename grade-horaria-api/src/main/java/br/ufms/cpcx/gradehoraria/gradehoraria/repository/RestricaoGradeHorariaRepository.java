package br.ufms.cpcx.gradehoraria.gradehoraria.repository;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.EDiaSemana;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RestricaoGradeHorariaRepository extends JpaRepository<RestricaoGradeHoraria, Long> {

    @Query("SELECT rgh FROM RestricaoGradeHoraria rgh " +
            "LEFT JOIN FETCH rgh.gradeHoraria JOIN FETCH rgh.professor dis")
    List<RestricaoGradeHoraria> buscarTodos();

    @Query("SELECT rgh FROM RestricaoGradeHoraria rgh " +
            "LEFT JOIN  rgh.gradeHoraria LEFT JOIN  rgh.professor pr " +
            "WHERE rgh.gradeHoraria.id = :idGradeHoraria and  rgh.tipo = :tipo")
    Page<RestricaoGradeHoraria> buscarFiltro(@Param("idGradeHoraria") Long idGradeHoraria, @Param("tipo") ETipoRestricao tipo, Pageable pageable);

    @Query("SELECT rgh FROM RestricaoGradeHoraria rgh " +
            "LEFT JOIN  rgh.gradeHoraria LEFT JOIN  rgh.professor pr " +
            "WHERE rgh.gradeHoraria.id = :idGradeHoraria and  rgh.tipo = :tipo " +
            " AND pr.id = :idProfessor")
    List<RestricaoGradeHoraria> buscarFiltroPorProfessor(@Param("idGradeHoraria") Long idGradeHoraria,
                                          @Param("tipo") ETipoRestricao tipo,
                                          @Param("idProfessor") Long idProfessor);

    @Query("SELECT rgh FROM RestricaoGradeHoraria rgh WHERE rgh.gradeHoraria.id = ?1 ORDER BY rgh.professor.nome")
    Page<RestricaoGradeHoraria> buscarPorGradeHorariaId(Long idGradeHoraria, Pageable pageable);

    @Query("SELECT rgh FROM RestricaoGradeHoraria rgh WHERE rgh.gradeHoraria.id = ?1")
    List<RestricaoGradeHoraria> buscarPorGradeHorariaId(Long idGradeHoraria);

    boolean existsByGradeHorariaIdAndProfessorIdAndDiaDaSemana(Long idGradeHoraria, Long idProfessor, EDiaSemana diaSemana);

    void deleteAllByGradeHorariaId(Long idGradeHoraria);
}
