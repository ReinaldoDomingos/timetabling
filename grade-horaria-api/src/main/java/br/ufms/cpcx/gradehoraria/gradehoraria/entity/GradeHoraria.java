package br.ufms.cpcx.gradehoraria.gradehoraria.entity;

import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.ESemestre;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Entity
@Table(name = "TB_GRADE_HORARIA")
@SequenceGenerator(name = "SEQ_GRADE_HORARIA", sequenceName = "SEQ_GRADE_HORARIA")
public class GradeHoraria {
    @Id
    @Column(name = "GRA_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "GRA_ANO", length = 100)
    private Integer ano;

    @Enumerated(EnumType.STRING)
    @Column(name = "GRA_SEMESTRE_ANO")
    private ESemestre semestreAno;

    @Column(name = "GRA_IS_TESTE")
    private Boolean isTeste;

    @Column(name = "GRA_QTD_AULAS_TURNO")
    private Integer quantidadeAulasPorTurno;

    @Transient
    private Integer cargaHorariaSemanalTotal;
}
