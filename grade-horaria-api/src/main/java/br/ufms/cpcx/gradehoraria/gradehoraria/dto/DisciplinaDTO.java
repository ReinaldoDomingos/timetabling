package br.ufms.cpcx.gradehoraria.gradehoraria.dto;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Disciplina;
import lombok.Data;
import org.modelmapper.ModelMapper;

@Data
public class DisciplinaDTO {
    private Long id;
    private String nome;
    private String codigo;
    private Long cargaHoraria;
    private Long gradeHorariaTesteId;

    public DisciplinaDTO() {
    }

    public DisciplinaDTO(Disciplina disciplina) {
        this.id = disciplina.getId();
        this.nome = disciplina.getNome();
        this.codigo = disciplina.getCodigo();
        this.cargaHoraria = disciplina.getCargaHoraria();
        this.gradeHorariaTesteId = disciplina.getGradeHorariaTesteId();
    }

    public static Disciplina toMapDisciplina(DisciplinaDTO disciplinaDTO) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(disciplinaDTO, Disciplina.class);
    }
}
