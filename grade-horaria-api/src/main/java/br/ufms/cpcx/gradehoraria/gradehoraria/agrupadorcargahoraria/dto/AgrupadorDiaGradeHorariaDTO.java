package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto;

import br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto.ProfessorGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Data;

import java.util.List;

@Data
public class AgrupadorDiaGradeHorariaDTO {
    private List<EntidadeDTO> entidades;
    private Integer quantidadeAulasPorTurno;
    private List<ProfessorGradeHorariaDTO> professores;
}
