package br.ufms.cpcx.gradehoraria.gradehoraria.controller;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class RestErrorHandler {

    @ExceptionHandler(GenericException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Map processValidationError(GenericException ex) {
        Map exception = new HashMap<>();
        exception.put("message", ex.getMessage());
        return exception;
    }
}
