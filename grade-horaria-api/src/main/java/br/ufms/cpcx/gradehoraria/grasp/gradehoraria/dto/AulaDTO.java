package br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
@Builder
public class AulaDTO {
    private String dia;
    private String turma;
    private String professor;
    private String disciplina;
    private Integer cargaHoraria;

    public AulaDTO clonar() {
        AulaDTO clone = AulaDTO.builder().build();
        BeanUtils.copyProperties(this, clone);

        return clone;
    }
}
