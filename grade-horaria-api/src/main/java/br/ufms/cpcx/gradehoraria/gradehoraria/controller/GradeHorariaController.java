package br.ufms.cpcx.gradehoraria.gradehoraria.controller;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.DisciplinaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.DisciplinaGradeHorariaEdicaoDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.GradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.GradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.filter.GradeHorariaFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.importacao.dto.ImportacaoGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.service.DisciplinaGradeHorariaService;
import br.ufms.cpcx.gradehoraria.gradehoraria.service.GradeHorariaService;
import br.ufms.cpcx.gradehoraria.gradehoraria.service.GradeHorariaXLSService;
import br.ufms.cpcx.gradehoraria.gradehoraria.service.ImportacaoGradeHorariaService;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RestController
@RequestMapping("/gradehoraria-api/gradeHoraria")
public class GradeHorariaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GradeHorariaController.class);

    @Autowired
    private GradeHorariaService gradeHorariaService;

    @Autowired
    private GradeHorariaXLSService gradeHorariaXLSService;

    @Autowired
    private ImportacaoGradeHorariaService importacaoGradeHorariaService;

    @Autowired
    private DisciplinaGradeHorariaService disciplinaGradeHorariaService;

    @GetMapping
    public Page<GradeHorariaDTO> buscarTodas(@RequestParam Map<String, String> filters) {
        return gradeHorariaService.buscarTodos(GradeHorariaFilter.of(filters));
    }

    @GetMapping("/{id}")
    public GradeHorariaDTO buscarPorId(@PathVariable("id") Long id) {
        return gradeHorariaService.buscarPorId(id);
    }

    @GetMapping("/gradeHorariaCompleta/{gradeHorariaId}")
    public Object gerarXLS(GradeHorariaFilter filter) {
        try {
            GradeHoraria gradeHoraria = getGradeHoraria(filter.getGradeHorariaId());

            Integer quantidadeAulasPorTurno = gradeHoraria.getQuantidadeAulasPorTurno();
            List<DisciplinaGradeHorariaEdicaoDTO> disciplinaGradeHorariaEdicaoDTOS = buscarTodasDisciplinasPorGradeHorariaId(gradeHoraria.getId());

            return gradeHorariaXLSService.gerarXLS(gradeHoraria.getId(), disciplinaGradeHorariaEdicaoDTOS, quantidadeAulasPorTurno, filter);

        } catch (GenericException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    private GradeHoraria getGradeHoraria(Long id) {
        GradeHorariaDTO gradeHorariaDTO = buscarPorId(id);

        return GradeHorariaDTO.toMapGradeHoraria(gradeHorariaDTO);
    }

    public List<DisciplinaGradeHorariaEdicaoDTO> buscarTodasDisciplinasPorGradeHorariaId(Long idGradeHoraria) {
        return disciplinaGradeHorariaService.buscarTodasPorGradeHorariaId(idGradeHoraria);
    }

    @GetMapping("/{id}/disciplinas")
    public Page<DisciplinaGradeHorariaEdicaoDTO> buscarDisciplinas(@PathVariable("id") Long id, @RequestParam Map<String, String> filters) {
        return disciplinaGradeHorariaService.buscarPorGradeHorariaId(GradeHorariaFilter.of(filters), id);
    }

    @PostMapping
    public GradeHorariaDTO salvar(@RequestBody GradeHorariaDTO gradeHorariaDTO) {
        return gradeHorariaService.salvar(gradeHorariaDTO);
    }

    @PostMapping("/uploadDisciplinasXLS")
    public Map<String, Object> uploadDisciplinasXLS(@RequestParam MultipartFile arquivo) {
        try {
            return importacaoGradeHorariaService.salvarArquivoDisciplinaXLS(arquivo);
        } catch (GenericException e) {
            LOGGER.error("Erro ao importar xls", e);
            throw new GenericException("Erro ao importar xls");
        }
    }

    @PostMapping("/importacao")
    public GradeHorariaDTO importar(@RequestBody ImportacaoGradeHorariaDTO importacao) throws IOException {
        return importacaoGradeHorariaService.importar(importacao);
    }

    @PutMapping("{id}")
    public GradeHorariaDTO alterar(@PathVariable("id") Long id, @RequestBody GradeHorariaDTO gradeHorariaDTO) {
        return gradeHorariaService.alterar(id, gradeHorariaDTO);
    }

    @PostMapping("/{id}/adicionarDisciplina")
    public DisciplinaGradeHorariaEdicaoDTO adicionarDisciplina(@PathVariable("id") Long id, @RequestBody DisciplinaDTO disciplinaDTO) {
        return disciplinaGradeHorariaService.salvar(id, disciplinaDTO.getId());
    }

    @DeleteMapping("{id}")
    public void deletar(@PathVariable("id") Long id) {
        try {
            gradeHorariaService.deletar(id);
        } catch (DataIntegrityViolationException e) {
            throw new GenericException("Possui disciplna(s), turma(s) ou professor(es) usado(s) em outra(s) grade horária(s). Desmarque a opção Dados de teste para conseguir apagar essa grade horária!");
        }
    }
}
