package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Data
public class DiaGradeHorariaDTO {
    Integer dia;
    List<TurmaGradeHorariaDTO> turmas;

    public DiaGradeHorariaDTO(Integer dia) {
        this.dia = dia;
        this.turmas = new ArrayList<>();
    }

    public TurmaGradeHorariaDTO adicionarTurma(String turma) {
        TurmaGradeHorariaDTO turmaGradeHorariaDTO = new TurmaGradeHorariaDTO(turma);
        this.turmas.add(turmaGradeHorariaDTO);
        return turmaGradeHorariaDTO;
    }

    public TurmaGradeHorariaDTO getTurma(String turma) {
        List<TurmaGradeHorariaDTO> turmasFiltradas = this.turmas.stream().filter(turmaGradeHorariaDTO -> turmaGradeHorariaDTO.getTurma().equals(turma)).collect(Collectors.toList());

        return !turmasFiltradas.isEmpty() ? turmasFiltradas.get(0) : null;
    }

    public Integer getCargaHorariaTotalProfesor(String professor) {
        AtomicInteger cargaHorariaTotalProfessor = new AtomicInteger();

        this.turmas.stream().forEach(turmaGradeHorariaDTO -> cargaHorariaTotalProfessor.addAndGet(turmaGradeHorariaDTO.getCargaHorariaTotalProfesor(professor)));

        return cargaHorariaTotalProfessor.get();
    }

    public Integer getCargaHorariaTotal() {
        return this.turmas.stream().mapToInt(TurmaGradeHorariaDTO::getCargaHorariaTotal).sum();
    }

    public List<EntidadeDTO> getEntidades() {
        List<EntidadeDTO> entidades = new ArrayList<>();

        this.getTurmas().forEach(turmaGradeHorariaDTO -> entidades.addAll(turmaGradeHorariaDTO.getEntidades()));

        return entidades;
    }
}
