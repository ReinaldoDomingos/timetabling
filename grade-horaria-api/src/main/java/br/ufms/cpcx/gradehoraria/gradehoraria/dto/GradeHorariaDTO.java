package br.ufms.cpcx.gradehoraria.gradehoraria.dto;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.GradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.ESemestre;
import lombok.Data;
import org.modelmapper.ModelMapper;

@Data
public class GradeHorariaDTO {
    private Long id;
    private Integer ano;
    private Boolean isTeste;
    private ESemestre semestreAno;
    private Integer quantidadeAulasPorTurno;
    private Integer cargaHorariaSemanalTotal;

    public GradeHorariaDTO() {
    }

    public static GradeHorariaDTO fromGradeHoraria(GradeHoraria gradeHorariaDTO) {
        ModelMapper mapper = new ModelMapper();

        return mapper.map(gradeHorariaDTO, GradeHorariaDTO.class);
    }

    public static GradeHoraria toMapGradeHoraria(GradeHorariaDTO gradeHorariaDTO) {
        ModelMapper mapper = new ModelMapper();

        return mapper.map(gradeHorariaDTO, GradeHoraria.class);
    }
}
