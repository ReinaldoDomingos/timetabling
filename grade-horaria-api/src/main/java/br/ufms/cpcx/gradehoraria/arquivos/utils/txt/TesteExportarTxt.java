package br.ufms.cpcx.gradehoraria.arquivos.utils.txt;

import java.io.IOException;

public class TesteExportarTxt {
    public static void main(String[] args) throws IOException {
        ExportarTxtUtils exportarTxtUtils = new ExportarTxtUtils("./teste.txt");

        exportarTxtUtils.escreverLinha("teste");

        exportarTxtUtils.fecharArquivo();
    }
}
